<?php
return [
    'language' => 'ru',
    'sourceLanguage' => 'en-US',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'name' => 'Медиа Лэнд',
    'components' => [
        'db' => require(__DIR__ . '/db.php'),
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => 'common\models\User',
            'loginUrl' => ['login'],
    ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
        ],
        'billingApi' => [
            'class' => 'common\components\BillingApi'
        ],
        'i18n' => [
            'translations' => [
                'app*' => [ // The pattern app* indicates that all message categories whose names start with app should be translated using this message source.
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages', // \messages\en\texts.php
                    'fileMap' => [
                        'app' => 'app.php',
                        'app/auth' => 'auth.php'
                    ]
                ],
            ],
        ],
    ],
    'modules' => [
        'admin' => [
            'class' => 'backend\modules\AdminModule',
            'as access' => [ // if you need to set access
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'], // all auth users

                    ],
                ]
            ],
        ],
    ],
];

