<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'host' => 'http://185.100.222.7',
    'billing.api.dci' => [
        'url' => 'https://185.100.222.4:1500/dcimgr?authinfo=%s:%s',
        'user' => 'root',
        'pass' => '3T4a5S9k'
    ],
    'billing.api.vmmgr' => [
        'url' => 'https://185.100.222.2:1500/vmmgr?authinfo=%s:%s',
        'user' => 'admin',
        'pass' => '1V5b7E0o'
    ]
];
