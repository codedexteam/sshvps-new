<?php
namespace common\models\forms;

use yii\base\Model;

class EditMenuForm extends Model {

    public $title_ru;
    public $title_en;
    public $title_zh;
    public $link;
    public $parent_id;
    public $active;
    public $icon;

    public function rules() {
        return [
            [['title_ru'], 'required'],
            [['title_ru', 'title_en', 'title_zh', 'link'], 'filter', 'filter' => 'trim'],
            [['title_ru', 'title_en', 'title_zh','link'], 'string', 'max' => 255],
            [['parent_id'], 'number'],
            [['icon'], 'string', 'max' => 32],
        ];
    }

    public function attributeLabels(){
        return [
            'title_ru' => 'Название',
            'title_en' => 'Название на английском',
            'title_zh' => 'Название на китайском',
            'link' => 'Ссылка',
            'parent_id' => 'Родительский пункт',
            'active' => 'Активность',
            'icon' => 'Иконка',
        ];
    }

}