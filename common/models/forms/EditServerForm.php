<?php

namespace common\models\forms;

use Yii;
use yii\base\Model;

class EditServerForm extends Model {

    public $name_ru;
    public $name_en;
    public $name_zh;
    public $cpuname_ru;
    public $cpuname_en;
    public $cpuname_zh;
    public $ram_ru;
    public $ram_en;
    public $ram_zh;
    public $hdd_ru;
    public $hdd_en;
    public $hdd_zh;
    public $raid;
    public $ipmi;
    public $price;
    public $rack;
    public $chassis_templ;
    public $hostname;
    public $owner;
    public $mac;
    public $ip;
    public $localspeed_ru;
    public $localspeed_en;
    public $localspeed_zh;
    public $poweron;
    public $os;
    public $location;
    public $active;

    public function rules() {
        return [
            [['name_ru', 'cpuname_ru', 'ram_ru', 'hdd_ru', 'price'], 'required'],
            [['name_ru', 'name_en', 'name_zh', 'cpuname_ru', 'cpuname_en', 'cpuname_zh', 'ram_ru', 'ram_en', 'ram_zh', 'hdd_ru', 'hdd_en', 'hdd_zh', 'price', 'rack', 'chassis_templ',
                'hostname', 'owner', 'mac', 'ip', 'localspeed_ru', 'localspeed_en', 'localspeed_zh', 'os', 'ram_ru', 'ram_en', 'ram_zh'], 'filter', 'filter' => 'trim'],
            [['name_ru', 'name_en', 'name_zh', 'cpuname_ru', 'cpuname_en', 'cpuname_zh', 'rack', 'os'], 'string', 'max' => 255],
            [['hdd_ru', 'hdd_en', 'hdd_zh', 'chassis_templ'], 'string', 'max' => 500],
            [['owner', 'mac', 'ip', 'location'], 'string', 'max' => 100],
            ['location', 'string', 'max' => 50],
            [['ram_ru', 'ram_en', 'ram_zh'], 'string', 'max' => 20],
            [['price'], 'number'],
        ];
    }

    public function attributeLabels(){
        return [
            'name_ru' => 'Название',
            'name_en' => 'Название на ангийском',
            'name_zh' => 'Название на китайском',
            'cpuname_ru' => 'Процессор',
            'cpuname_en' => 'Процессор на английском',
            'cpuname_zh' => 'Процессор на китайском',
            'ram_ru' => 'ОЗУ',
            'ram_en' => 'ОЗУ на английском',
            'ram_zh' => 'ОЗУ на китайском',
            'hdd_ru' => 'Жёсткий диск',
            'hdd_en' => 'Жёсткий диск на английском',
            'hdd_zh' => 'Жёсткий диск на китайском',
            'raid' => 'RAID',
            'ipmi' => 'IPMI',
            'price' => 'Цена в долларах',
            'rack' => 'Стойка',
            'chassis_templ' => 'Тип платформы',
            'hostname' => 'Имя хоста',
            'owner' => 'Владелец',
            'mac' => 'MAC-адрес',
            'ip' => 'IP-адрес',
            'location' => 'Местоположение',
            'localspeed_ru' => 'Скорость локального соединения',
            'localspeed_en' => 'Скорость локального соединения ENG',
            'localspeed_zh' => 'Скорость локального соединения CH',
            'poweron' => 'Состояние',
            'os' => 'ОС',
            'active' => 'Активность',
        ];
    }

}