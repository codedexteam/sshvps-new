<?php

namespace common\models\forms;

use yii\base\Model;

class EditVpsTariffsForm extends Model {

    public $billing_id;
    public $title_ru;
    public $title_en;
    public $title_zh;
    public $price;
    public $cpu_ru;
    public $cpu_en;
    public $cpu_zh;
    public $ram_ru;
    public $ram_en;
    public $ram_zh;
    public $hdd_ru;
    public $hdd_en;
    public $hdd_zh;
    public $traff_ru;
    public $traff_en;
    public $traff_zh;
    public $virtual_ru;
    public $virtual_en;
    public $virtual_zh;
    public $img;
    public $spec;
    public $os;
    public $active;

    public function rules() {
        return [
            [['billing_id', 'title_ru', 'price', 'cpu_ru', 'ram_ru', 'hdd_ru', 'traff_ru', 'virtual_ru', 'img', 'os'], 'required'],
            [['title_ru', 'title_en', 'title_zh', 'price', 'ram_ru',
                'ram_en', 'ram_zh', 'hdd_ru', 'hdd_en', 'hdd_zh', 'traff_ru',
                'traff_en', 'traff_zh', 'virtual_ru', 'virtual_en', 'virtual_zh'], 'filter', 'filter' => 'trim'],
            [['title_ru', 'title_en', 'title_zh', 'traff_ru', 'traff_en', 'traff_zh', 'virtual_ru', 'virtual_en', 'virtual_zh'], 'string', 'max' => 100],
            [['price'], 'string', 'max' => 10],
            [['ram_ru', 'ram_en', 'ram_zh', 'hdd_ru', 'hdd_en', 'hdd_zh', 'cpu_ru', 'cpu_en', 'cpu_zh'], 'string', 'max' => 50],
            ['img', 'string', 'max' => 50],
            [['billing_id'], 'number'],
        ];
    }

    public function attributeLabels(){
        return [
            'billing_id' => 'Billing id',
            'title_ru' => 'Название',
            'title_en' => 'Название на английском',
            'title_zh' => 'Название на китайском',
            'price' => 'Цена',
            'cpu_ru' => 'Процессор',
            'cpu_en' => 'Процессор на английском',
            'cpu_zh' => 'Процессор на китайском',
            'ram_ru' => 'ОЗУ',
            'ram_en' => 'ОЗУ на английском',
            'ram_zh' => 'ОЗУ на китайском',
            'hdd_ru' => 'Жёсткий диск',
            'hdd_en' => 'Жёсткий диск на английском',
            'hdd_zh' => 'Жёсткий диск на китайском',
            'traff_ru' => 'Трафик',
            'traff_en' => 'Трафик на английском',
            'traff_zh' => 'Трафик на китайском',
            'virtual_ru' => 'Виртуализация',
            'virtual_en' => 'Виртуализация на русском',
            'virtual_zh' => 'Виртуализация на китайском',
            'os' => 'ОС',
            'spec' => 'Спец. предложение',
            'active' => 'Активность',
        ];
    }

}