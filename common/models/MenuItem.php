<?php

namespace common\models;

use Yii;


class MenuItem extends AbstractModel {

    public static function tableName() {
        return 'menu_items';
    }

    public function getMenuTypes() {
        return $this->hasMany(MenuType::className(), ['id' => 'type_id']);
    }

    public function getChildItems() {
        return $this->hasMany(MenuItem::className(), ['parent_id' => 'id'])
            ->orderBy([
                'childItems.sort' => SORT_ASC,
                'childItems.id' => SORT_DESC
            ])
            ->alias('childItems');
    }

    public function getRu() {
        return $this->hasMany(LangMenuItem::className(), ['item_id' => 'id'])
            ->where(['ru.lang' => 'ru'])
            ->alias('ru');
    }
    public function getEn() {
        return $this->hasMany(LangMenuItem::className(), ['item_id' => 'id'])
            ->where(['en.lang' => 'en'])
            ->alias('en');
    }
    public function getZh() {
        return $this->hasMany(LangMenuItem::className(), ['item_id' => 'id'])
            ->where(['zh.lang' => 'zh'])
            ->alias('zh');
    }
    
    public function getItemLang() {
        return $this->hasMany(LangMenuItem::className(), ['item_id' => 'id'])
            ->where(['itemLang.lang' => Yii::$app->language])
            ->alias('itemLang');
    }

    public function getAllItems($where = false, $request = true, $order = ['id' => SORT_ASC], $active = false, $lang = false) {
        $query = MenuItem::find()
            ->joinWith('menuTypes')
            ->orderBy($order);
        if ($active) {
            $query->joinWith(['childItems' => function ($query) {
                $query->andOnCondition(['childItems.active' => 1]);
            }]);
        } else {
            $query->joinWith('childItems');
        }

        if ($lang) {
            $query->joinWith('itemLang');
        } else {
            $query->joinWith('ru');

        }

        if ($where) {
            $query->where($where);
        }

        if ($request) {
            return $query->all();
        } else {
            return $query;
        }
    }

}