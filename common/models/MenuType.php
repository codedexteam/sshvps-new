<?php

namespace common\models;


class MenuType extends AbstractModel {
    
    const MAIN_MENU_ID = 1;
    const FOOTER_ID = 2;
    const ACCOUNT_MENU_ID = 3;

    public static function tableName() {
        return 'menu_types';
    }

}