<?php

namespace common\models;

use Yii;

class LangTexts extends AbstractModel {
    
    public static function tableName() {
        return 'lang_texts';
    }

    public function getTextsKey() {
        return $this->hasMany(Texts::className(), ['id' => 'text_id'])
            ->alias('key');
    }

    public static function getTexstByLang($search) { //'word' or ['search1', 'search2']
        $language = Yii::$app->language;
        $query = self::find()
            ->joinWith('textsKey');

        if (is_array($search)) {
            foreach ($search as $val) {
                $query->orWhere(['like', 'key.name', $val]);
            }
            $query->andWhere([LangTexts::tableName() . '.lang' => $language]);
        } else {
            $query->where(['like', 'key.name', $search]);
            $query->andWhere([LangTexts::tableName() . '.lang' => $language]);
        }
        $texts = $query->all();

        $keys = (new Texts())->getSpecificKeys($search);
        $arrayTexts = [];
        
        foreach ($keys as $key) {
            $arrayTexts[$key['name']]['content'] = '';
            $arrayTexts[$key['name']]['id'] = '';
        }

        if (!empty($texts)) {
            foreach ($texts as $text) {
                $arrayTexts[$text->textsKey[0]['name']]['content'] = $text['content'];
                $arrayTexts[$text->textsKey[0]['name']]['id'] = $text['id'];
            }
        }
        return $arrayTexts;
    }
    
}