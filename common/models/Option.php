<?php

namespace common\models;

use Yii;


class Option extends AbstractModel {

    const OPTION_TYPE_SLIDE = 1;
    const OPTION_TYPE_RADIO = 2;
    const OPTION_TYPE_LIST = 3;

    const TYPE_SERVER = 1;
    const TYPE_VPS = 2;

    public static function tableName() {
        return 'options';
    }

    public function getLangOption() {
        return $this->hasMany(LangOption::className(), ['option_id' => 'id'])
            ->where(['langOption.lang' => Yii::$app->language])
            ->alias('langOption');
    }

    public function getRu() {
        return $this->hasMany(LangOption::className(),  ['option_id' => 'id'])
            ->where(['ru.lang' => 'ru'])
            ->alias('ru');
    }
    public function getEn() {
        return $this->hasMany(LangOption::className(),  ['option_id' => 'id'])
            ->where(['en.lang' => 'en'])
            ->alias('en');
    }
    public function getZh() {
        return $this->hasMany(LangOption::className(),  ['option_id' => 'id'])
            ->where(['zh.lang' => 'zh'])
            ->alias('zh');
    }

    public function getChildItems() {
        return $this->hasMany(Option::className(), ['parent_id' => 'id'])
            ->orderBy([
                'childItems.sort' => SORT_ASC,
                'childItems.id' => SORT_DESC
            ])
            ->alias('childItems');
    }

    public static function getAllOptions($where = false, $request = true, $order = ['id' => SORT_ASC], $active = false, $lang = false) {
        $query = self::find()
            ->orderBy($order)
            ->alias('options');

        if ($active) {
            $query->joinWith(['childItems' => function ($query) {
                $query->andOnCondition(['childItems.active' => 1]);
            }]);
        } else {
            $query->joinWith('childItems');
        }

        if ($lang) {
            $query->joinWith('langOption');
        } else {
            $query->joinWith('ru');

        }

        if ($where) {
            $query->andWhere($where);
        }

        if ($request) {
            return $query->all();
        } else {
            return $query;
        }
    }

    public static function getOneOption($where = false, $request = true, $lang = false) {
        $query = self::find()
            ->alias('option');

        if ($lang) {
            $query->joinWith('langOption');
        } else {
            $query->joinWith('ru');

        }

        if ($where) {
            $query->andWhere($where);
        }

        if ($request) {
            return $query->one();
        } else {
            return $query;
        }
    }

}