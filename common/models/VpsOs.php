<?php

namespace common\models;


class VpsOs extends AbstractModel {

    public static function tableName() {
        return 'vps_os';
    }

    public function getOs() {
        return $this->hasMany(Os::className(), ['id' => 'os_id']);
    }
    
}