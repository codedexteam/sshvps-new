<?php

namespace common\models;

use Yii;

class VpsTariff extends AbstractModel {

    public static function tableName() {
        return 'vps_tariffs';
    }

    public function getVpsOs() {
        return $this->hasMany(VpsOs::className(), ['tariff_id' => 'id'])
            ->joinWith('os');
    }

    public function getLangText() {
        return $this->hasMany(LangVpsTariff::className(), ['tariff_id' => 'id'])
            ->where(['langText.lang' => Yii::$app->language])
            ->alias('langText');
    }

    public function getRu() {
        return $this->hasMany(LangVpsTariff::className(),  ['tariff_id' => 'id'])
            ->where(['ru.lang' => 'ru'])
            ->alias('ru');
    }
    public function getEn() {
        return $this->hasMany(LangVpsTariff::className(),  ['tariff_id' => 'id'])
            ->where(['en.lang' => 'en'])
            ->alias('en');
    }
    public function getZh() {
        return $this->hasMany(LangVpsTariff::className(),  ['tariff_id' => 'id'])
            ->where(['zh.lang' => 'zh'])
            ->alias('zh');
    }

    public function getAllTariffs($where = false, $request = true, $order = ['id' => SORT_ASC], $lang = false) {
        $query = VpsTariff::find()
            ->joinWith('vpsOs')
            ->orderBy($order);

        if ($lang) {
            $query->joinWith('langText');
        } else {
            $query->joinWith('ru');

        }

        if ($where) {
            $query->where($where);
        }

        if ($request) {
            return $query->all();
        } else {
            return $query;
        }
    }

    public function getOneVps($where = false, $request = true, $lang = false) {
        $query = VpsTariff::find()
            ->joinWith('vpsOs');
        if ($lang) {
            $query->joinWith('langText');
        } else {
            $query->joinWith('ru');

        }

        if ($where) {
            $query->andWhere($where);
        }

        if ($request) {
            return $query->one();
        } else {
            return $query;
        }
    }

}