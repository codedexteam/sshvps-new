<?php

namespace common\models;

class Texts extends AbstractModel {

    const HEADER_NAME = 'header';
    const FOOTER_NAME = 'footer';
    const MAIN_PAGE_NAME = 'main';
    const SIGNUP_NAME = 'signup';
    const ENTER_NAME = 'enter';
    const RESET_PASS_NAME = 'reset-pass';
    const ORDER_SERV_NAME = 'order-server';
    const ORDER_VPS_NAME = 'order-vps';
    const ACCOUNT_NAME = 'account';

    public static function tableName() {
        return 'texts';
    }

    public static function getSpecificKeys($search) {
        $query = self::find();
        if (is_array($search)) {
            foreach ($search as $val) {
                $query->orWhere(['like', 'name', $val]);
            }
        } else {
            $query->where(['like', 'name', $search]);
        }
        return $query->all();
    }
    
}