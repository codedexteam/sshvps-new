<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

abstract class AbstractModel extends ActiveRecord {

    public static function getAll($order = ['id' => SORT_ASC]) {
        return self::find()->orderBy($order)->all();
    }

    public static function getOne($order = ['id' => SORT_ASC]) {
        return self::find()->orderBy($order)->one();
    }

    public static function getByCondition($where, $order = ['id' => SORT_ASC]) {
        $query = self::find()->where($where);
        return $query->orderBy($order)->all();
    }

    public function updateData($table, $column, $data, $caseColumn = false, $andWhen = false) {
        $dataCases = [];
        $createCase = '';
        $whereCond = $andWhen ? ' AND ' . $andWhen : '';

        if (is_array($data)) {
            if ($caseColumn) {
                $ids = [];
                foreach ($data as $key => $items) {
                    if (is_array($items)) {
                        $dataCases[] = ' WHEN ' . $caseColumn . ' = "'. $items[$caseColumn] . '"' . $whereCond . ' THEN ' . (!empty($items[$column]) ? '\'' . $items[$column] . '\'' : 'NULL');
                        $ids[] = $items[$caseColumn];
                    } else {
                        $dataCases[] = ' WHEN ' . $caseColumn . ' = '. $key . ' THEN ' . (!is_null($items) ? '\'' . $items . '\'' : 'NULL');
                        $ids[] = $key;
                    }
                }
                $dataCases[] = 'ELSE ' . $caseColumn . ' END WHERE ' . $caseColumn . ' in ("' . implode('", "', $ids) . '")' . $whereCond;
                $createCase = ' CASE ' . implode(' ', $dataCases);
            }
        } else {
            return false;
        }

            return Yii::$app->db->createCommand(
                'UPDATE ' . $table . ' SET ' . $column . '=' . $createCase
            )->execute();
    }

    public function insertData($table, $data, $ignore = false, $replace = false) { // ['id' => 2]
        $value = [];
        $values = [];
        $keys = [];

        if (is_array($data)) {
            $keys = array_keys($data[key($data)]);
            if (count($data) > 1) {
                foreach ($data as $key => $items) {
                    $value = [];
                    foreach ($items as $k => $item) {
                        $value[$k] = $item;
                    }
                    $values[] = '(\'' . implode('\', \'', $value) . '\')';
                }
            } else {
                $values[] = '(\'' . implode('\', \'', $data[0]) . '\')';

            }
            $ignore = $ignore ? 'IGNORE' : '';
            $insert = $replace ? 'REPLACE ' : 'INSERT ';
            $sql = $insert . $ignore . ' INTO ' . $table . ' (' . implode(', ', $keys) . ')' .
                ' VALUES ' . implode(', ', $values);

            Yii::$app->db->createCommand($sql)->execute();
            return true;
        } else {
            return false;
        }
    }

    public function filter($object, $options = []) {
        if (!empty($options)) {
            if (count($options) > 1) {
                foreach ($options as $key => $value) {
                    $object = $object->andWhere($key . '=' . $value);
                }
            } else {
                $object =  $object->where($options);
            }
        }

        return $object;
    }

}