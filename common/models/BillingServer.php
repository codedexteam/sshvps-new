<?php

namespace common\models;

use common\models\Server;

class BillingServer extends AbstractModel {

    public static function tableName() {
        return 'billing_servers';
    }

    public function getServers() {
        return $this->hasOne(Server::className(), ['billing_id' => 'billing_id']);
    }

    public function countNewServers($where = false, $order = ['id' => SORT_ASC]) {
        $query = BillingServer::find()
            ->joinWith('servers')
            ->orderBy($order);

        if ($where) {
            $query = $this->filter($query, $where);
        }

        return $query->count();
    }

    public function getNewServers($where = false, $request = true, $order = ['id' => SORT_ASC]) {
        $query = BillingServer::find()
            ->joinWith('servers')
            ->orderBy($order);

        if ($where) {
            $query = $this->filter($query, $where);
        }

        if ($request) {
            return $query->all();
        } else {
            return $query;
        }
    }
    
}