<?php

namespace common\models;

use Yii;


class Server extends AbstractModel {
    
    public $locate = [
        'RU' => 'RU',
        'EN' => 'EN',
        'DH' => 'DH'
    ];

    public static function tableName() {
        return 'servers';
    }

    public function getApiServers() {
        return $this->hasOne(BillingServer::className(), ['billing_id' => 'billing_id'])->alias('apiServers');
    }

    public function getLangText() {
        return $this->hasMany(LangServer::className(), ['server_id' => 'id'])
            ->where(['langText.lang' => Yii::$app->language])
            ->alias('langText');
    }

    public function getRu() {
        return $this->hasMany(LangServer::className(),  ['server_id' => 'id'])
            ->where(['ru.lang' => 'ru'])
            ->alias('ru');
    }
    public function getEn() {
        return $this->hasMany(LangServer::className(),  ['server_id' => 'id'])
            ->where(['en.lang' => 'en'])
            ->alias('en');
    }
    public function getZh() {
        return $this->hasMany(LangServer::className(),  ['server_id' => 'id'])
            ->where(['zh.lang' => 'zh'])
            ->alias('zh');
    }

    public function getAllServers($where = false, $request = true, $order = ['id' => SORT_ASC],  $lang = false) {
        $query = Server::find()
            ->joinWith('apiServers')
            ->orderBy($order)
        ->alias('servers');

        if ($lang) {
            $query->joinWith('langText');
        } else {
            $query->joinWith('ru');

        }

        if ($where) {
            $query->andWhere($where);
        }

        if ($request) {
            return $query->all();
        } else {
            return $query;
        }
    }

    public function getOneServer($where = false, $request = true, $lang = false) {
        $query = Server::find()
            ->joinWith('apiServers')
            ->alias('servers');

        if ($lang) {
            $query->joinWith('langText');
        } else {
            $query->joinWith('ru');

        }

        if ($where) {
            $query->andWhere($where);
        }

        if ($request) {
            return $query->one();
        } else {
            return $query;
        }
    }

}