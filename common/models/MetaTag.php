<?php

namespace common\models;

use Yii;

class MetaTag extends AbstractModel {

    public static function tableName() {
        return 'meta_tags';
    }
    
    public static function getOnePage($page_key) {
        return self::findOne(['page_key' => $page_key]);
    }

    public function getLangTags() {
        return $this->hasMany(LangMetaTag::className(), ['mata_tag_id' => 'id'])
            ->where(['langTags.lang' => Yii::$app->language])
            ->alias('langTags');
    }

    public function getEn() {
        return $this->hasMany(LangMetaTag::className(),  ['mata_tag_id' => 'id'])
            ->where(['en.lang' => 'en'])
            ->alias('en');
    }
    public function getZh() {
        return $this->hasMany(LangMetaTag::className(),  ['mata_tag_id' => 'id'])
            ->where(['zh.lang' => 'zh'])
            ->alias('zh');
    }
    public function getRu() {
        return $this->hasMany(LangMetaTag::className(),  ['mata_tag_id' => 'id'])
            ->where(['ru.lang' => 'ru'])
            ->alias('ru');
    }

    public static function getAllTags($where = false, $request = true, $order = ['id' => SORT_ASC],  $lang = false) {
        $query = self::find()
            ->orderBy($order)
            ->alias('tags');

        if ($lang) {
            $query->joinWith('langTags');
        } else {
            $query->joinWith('ru');

        }

        if ($where) {
            $query->andWhere($where);
        }

        if ($request) {
            return $query->all();
        } else {
            return $query;
        }
    }

    public static function getOneTag($where = false, $request = true, $lang = false) {
        $query = self::find()
            ->alias('tag');

        if ($lang) {
            $query->joinWith('langTags');
        } else {
            $query->joinWith('ru');

        }

        if ($where) {
            $query->andWhere($where);
        }

        if ($request) {
            return $query->one();
        } else {
            return $query;
        }
    }
    
}