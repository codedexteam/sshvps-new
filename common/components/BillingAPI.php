<?php

namespace common\components;

use Yii;
use yii\base\Component;
use common\models\BillingServer;


class BillingAPI extends Component {

    protected $dciUrl;
    protected $dciUser;
    protected $dciPass;

    protected $vmmgrUrl;
    protected $vmmgrUser;
    protected $vmmgrPass;

    public function __construct() {
        parent::__construct();

        $this->dciUrl = Yii::$app->params['billing.api.dci']['url'];
        $this->dciUser = Yii::$app->params['billing.api.dci']['user'];
        $this->dciPass = Yii::$app->params['billing.api.dci']['pass'];

        $this->vmmgrUrl = Yii::$app->params['billing.api.vmmgr']['url'];
        $this->vmmgrUser = Yii::$app->params['billing.api.vmmgr']['user'];
        $this->vmmgrPass = Yii::$app->params['billing.api.vmmgr']['pass'];
    }

    public function getServerList() {
        $params = [
            'su' => 'admin',
            'func' => 'server',
            'out' => 'json'
        ];
        return $this->dciRequest($params);
    }

    public function getVMList() {
        $params = [
            'func' => 'vm',
            'out' => 'json'
        ];
        return $this->vmmgrRequest($params);
    }

    public function getServerOsList() {
        $params = [
            'func' => 'osmgr',
            'out' => 'json'
        ];
        return $this->dciRequest($params);
    }
    
    public function getOneServer($id) {
        $params = [
            'su' => 'admin',
            'func' => 'server.edit',
            'out' => 'json',
            'elid' => $id
        ];
        return $this->dciRequest($params);
    }

    public function dciRequest($params) {
        if (!empty($params) && is_array($params)) {
            $url = sprintf($this->dciUrl, $this->dciUser, $this->dciPass);

            foreach ($params as $key => $param) {
                $url .= '&' . $key . '=' . $param;
            }

            $context = stream_context_set_default([
                'ssl' => [
                    'peer_name' => 'generic-server',
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                ],
            ]);
            return json_decode(file_get_contents($url, false, $context));
        }
        return false;
    }

    public function vmmgrRequest($params) {
        if (!empty($params) && is_array($params)) {
            $url = sprintf($this->vmmgrUrl, $this->vmmgrUser, $this->vmmgrPass);

            foreach ($params as $key => $param) {
                $url .= '&' . $key . '=' . $param;
            }

            $context = stream_context_set_default([
                'ssl' => [
                    'peer_name' => 'generic-server',
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                ],
            ]);
            return json_decode(file_get_contents($url, false, $context));
        }
        return false;
    }

}