<?php
use yii\helpers\Html;

return [
    'repeat_send_mail' => 'The letter was sent. Please, check your inbox.',
    'confirm_reg' => 'english text',
    'link_activation' => 'english text',
    'account_exist' => 'english text',
    'link_activation_er' => 'english text',
    'reg_pass_compare_er' => 'english text',
    'email_exist' => 'english text',
    'phone_format_er' => 'english text',
    'name' => 'Name',
    'lastname' => 'Last name',
    'password' => 'Password',
    'email' => 'Email',
    'password_repeat' => 'Repeat password',
    'phone' => 'Phone',
    'country' => 'Country',
    'address' => 'Address',
    'pass_email_er' => 'Incorrect email or password.',
    'confirm_email' => 'Confirm email. ' . Html::a('Activate.', '/signup/confirm'),
    'reset_email_success' => 'Check your email for further instructions.',
    'reset_email_error' => 'Sorry, we are unable to reset password for the provided email address.',
    'reset_email_save' => 'New password saved.',
];