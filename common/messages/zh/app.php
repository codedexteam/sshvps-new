<?php
use yii\helpers\Html;

return [
    'repeat_send_mail' => 'китайский текст',
    'confirm_reg' => 'китайский текст',
    'link_activation' => 'китайский текст',
    'account_exist' => 'китайский текст',
    'link_activation_er' => 'китайский текст',
    'reg_pass_compare_er' => 'китайский текст',
    'email_exist' => 'китайский текст',
    'phone_format_er' => 'китайский текст',
    'name' => 'Name китай',
    'lastname' => 'Last name китай',
    'password' => 'Password китай',
    'email' => 'Email',
    'password_repeat' => 'Repeat pass китай',
    'phone' => 'Phone китай',
    'country' => 'Country китай',
    'address' => 'Address китай',
    'pass_email_er' => 'ошибка авторизации на китайском',
    'confirm_email' => 'Подтверждение на китайском. ' . Html::a('Активировать аккаунт.', '/signup/confirm'),
    'reset_email_success' => 'Проверьте свой почтовый ящик (китай).',
    'reset_email_error' => 'Извините, мы не можем сбросить пароль (китай).',
    'reset_email_save' => 'Пароль изменён (китай).',
];