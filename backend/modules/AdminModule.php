<?php

namespace backend\modules;

use Yii;
use yii\filters\AccessControl;
use yii\base\Module;

class AdminModule extends Module {

    public $controllerNamespace = 'backend\controllers\admin';

    public function beforeAction($action) {

        if (!parent::beforeAction($action)) {
            return false;
        }

        if (!Yii::$app->user->isGuest) {
            return true;
        }
        else {
            Yii::$app->getResponse()->redirect(Yii::$app->getHomeUrl());
            return false;
        }
    }

    public function init() {
        $this->setViewPath('@backend/views');
        $this->layout = "main.php";
        parent::init();
    }

}