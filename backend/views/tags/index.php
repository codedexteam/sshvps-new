<?php
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\helpers\Html;

$this->title = 'Мета-теги';
?>
<h1><?php echo 'Страницы > ' . $this->title; ?></h1>
<div class="row-fluid add">
    <a href="<?php echo Url::toRoute(Yii::$app->controller->id . '/edit'); ?>" class="btn btn-success">Добавить страницу</a>
</div>
<div class="row-fluid sections">
    <div class="progress progress-striped active sort-progress">
        <div class="progress-bar progress-bar-info" style="width: 100%"></div>
    </div>
    <table id="sort-table" class="table table-striped table-hover">
        <thead>
        <tr>
            <th>#</th>
            <th>Страница</th>
            <th>Title</th>
            <th>Keywords</th>
            <th>Description</th>
            <th></th>
        </tr>
        </thead>
        <?php if (!empty($tags)) { ?>
            <?php foreach ($tags as $tag) { ?>
                <tr>
                    <td><?php echo $tag->id; ?></td>
                    <td><?php echo $tag->page_description; ?></td>
                    <td><?php echo $tag->ru[0]->title; ?></td>
                    <td><?php echo $tag->ru[0]->keywords; ?></td>
                    <td><?php echo $tag->ru[0]->description; ?></td>
                    <td>
                        <div class="data col-md-12" data-id="<?php echo $tag->id; ?>">
                            <div class="row">
                                <a href="<?php echo Url::toRoute([Yii::$app->controller->id . '/edit', 'id' => $tag->id]); ?>" class="btn btn-default btn-xs" title="Редактировать">Edit</a>
<!--                                <a class="btn btn-danger btn-xs btn-delete" data-id="--><?//= $tariff->id; ?><!--" title="Удалить">-->
<!--                                    <span>Delete</span>-->
<!--                                    <div class="progress progress-striped active" style="border-radius: 0;margin: 0;height: 3px; display: none">-->
<!--                                        <div class="progress-bar progress-bar-danger" style="width: 100%"></div>-->
<!--                                    </div>-->
<!--                                </a>-->
                            </div>
                        </div>
                    </td>
                </tr>
            <?php } ?>
        <?php } ?>
    </table>
    <div class="row-fluid pager">
        <?php echo LinkPager::widget([
            'pagination' => $pager,
        ]); ?>
    </div>
</div>
