<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = Yii::$app->request->get('id') ? 'Редактировать' : 'Добавить';
?>
    <h1><?php echo 'Мета-теги > ' . $this->title; ?></h1>

<?php $form = ActiveForm::begin([
    'options' => ['class' => 'form-horizontal',],
    'fieldConfig' => [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10">{input}</div>',
        'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
<?php echo $form->field($edit, 'page_description')->input('text', ['value' => $model->page_description])->label(); ?>
<?php echo $form->field($edit, 'page_key')->input('text', ['value' => $model->page_key])->label(); ?>
<?php echo $form->field($edit, 'title_ru', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/ru.png" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->ru) ? $model->ru[0]['title'] : ''])->label(); ?>

<?php echo $form->field($edit, 'title_en', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/en.jpg" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->en) ? $model->en[0]['title'] : ''])->label(); ?>

<?php echo $form->field($edit, 'title_zh', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/ch.png" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->zh) ? $model->zh[0]['title'] : ''])->label(); ?>

<?php echo $form->field($edit, 'description_ru', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/ru.png" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->ru) ? $model->ru[0]['description'] : ''])->label(); ?>

<?php echo $form->field($edit, 'description_en', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/en.jpg" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->en) ? $model->en[0]['description'] : ''])->label(); ?>

<?php echo $form->field($edit, 'description_zh', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/ch.png" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->zh) ? $model->zh[0]['description'] : ''])->label(); ?>

<?php echo $form->field($edit, 'keywords_ru', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/ru.png" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->ru) ? $model->ru[0]['keywords'] : ''])->label(); ?>

<?php echo $form->field($edit, 'keywords_en', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/en.jpg" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->en) ? $model->en[0]['keywords'] : ''])->label(); ?>

<?php echo $form->field($edit, 'keywords_zh', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/ch.png" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->zh) ? $model->zh[0]['keywords'] : ''])->label(); ?>


    <div class="form-group">
        <div class="col-lg-10 col-lg-offset-2">
            <a href="<?php echo Url::toRoute([Yii::$app->controller->id . '/index']); ?>" class="btn btn-warning">Вернуться к списку</a>
            <?php echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>