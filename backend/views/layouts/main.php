<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use backend\assets\AppAsset;
use backend\components\AdminMenu;
use yii\helpers\Url;

$action = Yii::$app->controller->action->id;
AppAsset::register($this);
$session = Yii::$app->session;
$session->open();
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html class="fuelux">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php echo Html::csrfMetaTags() ?>
    <title><?php echo $this->title; ?></title>
    <?php $this->head() ?>
    <link rel="shortcut icon" href="/favicon-16x16.ico" type="image/x-icon" sizes="16x16">
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
</head>
<body>
<?php $this->beginBody() ?>
<nav class="navbar navbar-inverse" style="border-radius: 0;">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
<!--            <a class="navbar-brand" href="--><?php //echo Yii::$app->homeUrl; ?><!--" target="_blank">--><?php //echo Yii::$app->name; ?><!--</a>-->
            <a class="navbar-brand" href="<?php echo Url::toRoute(['/site/index']); ?>" target="_blank"><?php echo Yii::$app->name; ?></a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <?php echo AdminMenu::widget(); ?>
            </ul>
            <div class="pull-right exit">
                <span><?php echo Yii::$app->user->identity->username && !empty(Yii::$app->user->identity->username) ? 'Здравствуйте, ' . Yii::$app->user->identity->username . '!' : ''; ?></span><br>
                <a href="/logout">ВЫХОД</a>
            </div>
        </div>
    </div>
</nav>
<div class="container content">
    <div class="row-fluid">
        <div>
            <?php echo $content ?>
        </div>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
