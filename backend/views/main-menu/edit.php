<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = Yii::$app->request->get('id') ? 'Редактировать' : 'Добавить';
?>
<h1><?php echo $controllerTitle . ' > ' . $this->title; ?></h1>
<?php $form = ActiveForm::begin([
    'options' => ['class' => 'form-horizontal',],
    'fieldConfig' => [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10">{input}</div>',
        'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
<?php echo $form->field($edit, 'title_ru', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/ru.png" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->ru) ? $model->ru[0]['title'] : ''])->label(); ?>
<?php echo $form->field($edit, 'title_en', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/en.jpg" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->en) ? $model->en[0]['title'] : ''])->label(); ?>
<?php echo $form->field($edit, 'title_zh', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/ch.png" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->zh) ? $model->zh[0]['title'] : ''])->label(); ?>
<?php echo $form->field($edit, 'link')->input('text', ['value' => $model->link])->label(); ?>
<?php echo $form->field($edit, 'parent_id')->dropDownList($parenItems, ['options' => [ $model->parent_id => ['selected ' => true]]])->label('Родительский пункт'); ?>
<div class="form-group info-data">
    <div class="col-lg-10 col-lg-offset-2 bg-info">
        <span>Название иконки смотреть в списке иконок по следующим ссылкам:</span>
        <br>
        <a target="_blank" href="http://fontawesome.io/icons/">http://fontawesome.io/icons/</a>
        <br>
        <a target="_blank" href="http://zavoloklom.github.io/material-design-iconic-font/icons.html">http://zavoloklom.github.io/material-design-iconic-font/icons.html</a>
        <br>
        <span>Примеры названий иконки: </span><b>fa-plus-circle</b>, <b>zmdi zmdi-account</b>
    </div>
</div>
<?php echo $form->field($edit, 'icon', [
    'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10 option-icon">{input}<i class="' . $model->icon . '"></i></div>'
])->input('text', [
    'value' => $model->icon,
    'placeholder' => 'fa-plus-circle'
])->label(); ?>
<?php echo $form->field($edit, 'active')->input('checkbox', [
    'checked' => $model->active == 1 ? 'checked' : false,
    'class' => 'checkbox',
])->label('Активность'); ?>
<div class="form-group">
    <div class="col-lg-10 col-lg-offset-2">
        <a href="<?php echo Url::toRoute([Yii::$app->controller->id . '/index']); ?>" class="btn btn-warning">Вернуться к списку</a>
        <?php echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

