<?php
use mihaildev\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = Yii::$app->request->get('id') ? 'Редактировать' : 'Добавить';
?>
<h1><?php echo 'Серверы > ' . $this->title; ?></h1>

<?php $form = ActiveForm::begin([
    'options' => ['enctype'=>'multipart/form-data', 'class' => 'form-horizontal',],
    'fieldConfig' => [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10">{input}</div>',
        'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
<div class="form-group billing-data">
    <div class="col-lg-10 col-lg-offset-2">
        <p><?php echo $billingServer->name; ?></p>
    </div>
</div>
<?php echo $form->field($edit, 'name_ru', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/ru.png" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->ru) ? $model->ru[0]['name'] : ''])->label(); ?>
<?php echo $form->field($edit, 'name_en', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/en.jpg" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->en) ? $model->en[0]['name'] : ''])->label(); ?>
<?php echo $form->field($edit, 'name_zh', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/ch.png" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->zh) ? $model->zh[0]['name'] : ''])->label(); ?>
<div class="form-group billing-data">
    <div class="col-lg-10 col-lg-offset-2">
        <p><?php echo $billingServer->cpuname; ?></p>
    </div>
</div>
<?php echo $form->field($edit, 'cpuname_ru', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/ru.png" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->ru) ? $model->ru[0]['cpuname'] : ''])->label(); ?>
<?php echo $form->field($edit, 'cpuname_en', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/en.jpg" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->en) ? $model->en[0]['cpuname'] : ''])->label(); ?>
<?php echo $form->field($edit, 'cpuname_zh', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/ch.png" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->zh) ? $model->zh[0]['cpuname'] : ''])->label(); ?>
<div class="form-group billing-data">
    <div class="col-lg-10 col-lg-offset-2">
        <p><?php echo $billingServer->ram; ?></p>
    </div>
</div>
<?php echo $form->field($edit, 'ram_ru', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/ru.png" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->ru) ? $model->ru[0]['ram'] : ''])->label(); ?>
<?php echo $form->field($edit, 'ram_en', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/en.jpg" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->en) ? $model->en[0]['ram'] : ''])->label(); ?>
<?php echo $form->field($edit, 'ram_zh', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/ch.png" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->zh) ? $model->zh[0]['ram'] : ''])->label(); ?>
<div class="form-group billing-data">
    <div class="col-lg-10 col-lg-offset-2">
        <p><?php echo $billingServer->hdd; ?></p>
    </div>
</div>
<?php echo $form->field($edit, 'hdd_ru', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/ru.png" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->ru) ? $model->ru[0]['hdd'] : ''])->label(); ?>
<?php echo $form->field($edit, 'hdd_en', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/en.jpg" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->en) ? $model->en[0]['hdd'] : ''])->label(); ?>
<?php echo $form->field($edit, 'hdd_zh', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/ch.png" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->zh) ? $model->zh[0]['hdd'] : ''])->label(); ?>
<div class="form-group billing-data">
    <div class="col-lg-10 col-lg-offset-2">
        <p><?php echo $billingServer->price; ?></p>
    </div>
</div>
<?php echo $form->field($edit, 'price')->input('text', ['value' => $model->price])->label(); ?>
<div class="form-group billing-data">
    <div class="col-lg-10 col-lg-offset-2">
        <p><?php echo $billingServer->rack; ?></p>
    </div>
</div>
<?php echo $form->field($edit, 'rack')->input('text', ['value' => $model->rack])->label(); ?>
<div class="form-group billing-data">
    <div class="col-lg-10 col-lg-offset-2">
        <p><?php echo $billingServer->chassis_templ; ?></p>
    </div>
</div>
<?php echo $form->field($edit, 'chassis_templ')->input('text', ['value' => $model->chassis_templ])->label(); ?>
<div class="form-group billing-data">
    <div class="col-lg-10 col-lg-offset-2">
        <p><?php echo $billingServer->hostname; ?></p>
    </div>
</div>
<?php echo $form->field($edit, 'hostname')->input('text', ['value' => $model->hostname])->label(); ?>
<div class="form-group billing-data">
    <div class="col-lg-10 col-lg-offset-2">
        <p><?php echo $billingServer->owner; ?></p>
    </div>
</div>
<?php echo $form->field($edit, 'owner')->input('text', ['value' => $model->owner])->label(); ?>
<div class="form-group billing-data">
    <div class="col-lg-10 col-lg-offset-2">
        <p><?php echo $billingServer->mac; ?></p>
    </div>
</div>
<?php echo $form->field($edit, 'mac')->input('text', ['value' => $model->mac])->label(); ?>
<div class="form-group billing-data">
    <div class="col-lg-10 col-lg-offset-2">
        <p><?php echo $billingServer->ip; ?></p>
    </div>
</div>
<?php echo $form->field($edit, 'ip')->input('text', ['value' => $model->ip])->label(); ?>
<div class="form-group billing-data">
    <div class="col-lg-10 col-lg-offset-2">
        <p><?php echo $billingServer->localspeed; ?></p>
    </div>
</div>
<?php echo $form->field($edit, 'localspeed_ru', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/ru.png" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->ru) ? $model->ru[0]['localspeed'] : ''])->label(); ?>
<?php echo $form->field($edit, 'localspeed_en', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/en.jpg" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->en) ? $model->en[0]['localspeed'] : ''])->label(); ?>
<?php echo $form->field($edit, 'localspeed_zh', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/ch.png" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->zh) ? $model->zh[0]['localspeed'] : ''])->label(); ?>
<div class="form-group billing-data">
    <div class="col-lg-10 col-lg-offset-2">
        <p><?php echo $billingServer->os; ?></p>
    </div>
</div>
<?php echo $form->field($edit, 'os')->input('text', ['value' => $model->os])->label(); ?>
<?php echo $form->field($edit, 'location')->dropDownList($location, ['options' => [$model->location => ['selected ' => true]]])->label(); ?>
<div class="form-group billing-data">
    <div class="col-lg-10 col-lg-offset-2">
        <input type="checkbox" value="<?php echo $billingServer->raid; ?>" disabled> RAID
    </div>
</div>
<?php echo $form->field($edit, 'raid')->input('checkbox', [
    'checked' => $model->raid == 1 ? 'checked' : false,
    'class' => 'checkbox',
])->label(); ?>
<div class="form-group billing-data">
    <div class="col-lg-10 col-lg-offset-2">
        <input type="checkbox" value="<?php echo $billingServer->ipmi; ?>" disabled> IPMI
    </div>
</div>
<?php echo $form->field($edit, 'ipmi')->input('checkbox', [
    'checked' => $model->ipmi == 1 ? 'checked' : false,
    'class' => 'checkbox',
])->label(); ?>
<?php echo $form->field($edit, 'active')->input('checkbox', [
    'checked' => $model->active == 1 ? 'checked' : false,
    'class' => 'checkbox',
])->label(); ?>
<div class="form-group">
    <div class="col-lg-10 col-lg-offset-2">
        <a href="<?php echo Url::toRoute([Yii::$app->controller->id . '/index']); ?>" class="btn btn-warning">Вернуться к списку</a>
        <?php echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
