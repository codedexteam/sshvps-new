<?php
use yii\helpers\Url;
use yii\widgets\LinkPager;

$this->title = 'Выделенные серверы';
?>
<h1><?php echo 'Серверы > ' . $this->title; ?></h1>
<div class="row-fluid" id="billing-update">
<!--    <a href="servers/update-servers" class="btn btn-success">Обновить список выд. серверов сейчас</a>-->
    <a class="btn btn-success update-servers">Обновить список выд. серверов сейчас</a>
</div>
<div class="row-fluid sections">
    <div class="progress progress-striped active sort-progress">
        <div class="progress-bar progress-bar-info" style="width: 100%"></div>
    </div>
    <div id="tabs">
        <ul>
            <li><a href="#list">Список серверов</a></li>
            <li><a href="#new">Новые серверы (<span id="new-count"><?php echo $countNewServers; ?></span>)</a></li>
        </ul>
        <div id="list">
            <table  class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>id</th>
                    <th>billing_id</th>
                    <th>Название</th>
                    <th>Изменён</th>
                    <th>Удалён</th>
                    <th>Активность</th>
                    <th></th>
                </tr>
                </thead>
                <?php if (!empty($allServers)) {?>
                    <?php foreach ($allServers as $server) { ?>
                        <tr>
                            <td><?php echo $server->id; ?></td>
                            <td><?php echo $server->billing_id; ?></td>
                            <td><?php echo $server->ru[0]->name; ?></td>
                            <td><?php
                                $updateBilling = new DateTime($server->apiServers->updated_at);
                                $updateServ = new DateTime($server->updated_at);
                                echo $updateBilling > $updateServ ? 'Да' : 'Нет'; ?></td>
                            <td><?php echo $server->apiServers->is_deleted ? 'Да' : 'Нет'; ?></td>
                            <td class="status"><?php echo $server->active ? 'Да' : 'Нет'; ?></td>
                            <td>
                                <div class="data col-md-12" data-id="<?php echo $server->billing_id; ?>">
                                    <div class="row">
                                        <a href="<?php echo Url::toRoute([Yii::$app->controller->id . '/edit', 'id' => $server->billing_id]); ?>" class="btn btn-default btn-xs" title="Редактировать">Edit</a>
                                        <a class="btn btn-primary btn-xs btn-activate mini" data-value="<?php echo $server->active == 1 ? 0 : 1; ?>" data-id="<?= $server->id; ?>" title="<?= $server->active ? 'Деактивировать' : 'Активировать'; ?>">
                                            <span><?php echo $server->active == 1 ? 'Deactivate' : 'Activate'; ?></span>
                                            <div class="progress progress-striped active" style="border-radius: 0;margin: 0;height: 3px; display: none">
                                                <div class="progress-bar" style="width: 100%"></div>
                                            </div>
                                        </a>
                                        <a class="btn btn-danger btn-xs btn-delete" data-id="<?= $server->id; ?>" title="Удалить">
                                            <span>Delete</span>
                                            <div class="progress progress-striped active" style="border-radius: 0;margin: 0;height: 3px; display: none">
                                                <div class="progress-bar progress-bar-danger" style="width: 100%"></div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    <?php } ?>
                <?php } ?>
            </table>
            <?php if (!is_null($pagerServers)) { ?>
                <div class="row-fluid pager">
                    <?php echo LinkPager::widget([
                        'pagination' => $pagerServers,
                    ]); ?>
                </div>
            <?php } ?>
        </div>
        <div id="new">
            <table  class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Название</th>
                    <th>Владелец</th>
                    <th></th>
                </tr>
                </thead>
                <?php if (!empty($newServers)) {?>
                        <?php foreach ($newServers as $server) { ?>
                            <tr>
                                <td><?php echo $server->billing_id; ?></td>
                                <td><?php echo $server->name; ?></td>
                                <td><?php echo $server->owner; ?></td>
                                <td>
                                    <a class="btn btn-primary btn-xs btn-approve"  data-id="<?php echo $server->billing_id ?>">
                                        <span>Одобрить</span>
                                        <div class="progress progress-striped active" style="border-radius: 0;margin: 0;height: 3px; display: none">
                                            <div class="progress-bar" style="width: 100%"></div>
                                        </div>
                                    </a>
                                </td>
                            </tr>
                        <?php } ?>
                <?php } ?>
            </table>
            <?php if (!is_null($pagerNewServers)) { ?>
                <div class="row-fluid pager">
                    <?php echo LinkPager::widget([
                        'pagination' => $pagerNewServers,
                    ]); ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $(function() {
            $( "#tabs" ).tabs();
        });
        $('.btn-approve').click(function () {
            var $this = $(this);
            var id = $this.data().id;
            $this.find('.progress').show();
            $.ajax({
                url: '<?php echo Url::to([Yii::$app->controller->id . '/approve']); ?>',
                type: 'post',
                dataType: 'json',
                data: {
                    id: id,
                    _csrf: yii.getCsrfToken()
                },
                success: function (response) {
                    if (response.status == true) {
                        $this.closest('tr').remove();
                        var newCount = parseInt($('#new-count').text()) - 1;
                        $('#new-count').text(newCount);
                    }
                    $this.find('.progress').hide();
                },
                error: function () {
                    $this.find('.progress').hide();
                }
            });
        });
        $('.update-servers').click(function () {
            var $this = $(this);
            $('.row-fluid.sections').find('.progress').show();
            $.ajax({
                url: '<?php echo Url::to([Yii::$app->controller->id . '/update-servers']); ?>',
                type: 'post',
                dataType: 'json',
                data: {
                    _csrf: yii.getCsrfToken()
                },
                success: function (response) {
                    if (response.status == true) {
                        $('.row-fluid.sections').find('.progress').hide();
                    }
                },
                error: function () {
                    $('.row-fluid.sections').find('.progress').hide();
                }
            });
        });

        $('.btn-activate').click(function () {
            var $this = $(this);
            activeAjax($this, '<?php echo Url::toRoute(Yii::$app->controller->id . '/active'); ?>', 'Activate', 'Deactivate');
        });

        $('.btn-delete').click(function () {
            var agree = confirm('Вы действительно хотите удалить этот сервер?');
            if (agree) {
                var $this = $(this);
                deleteAjax($this, '<?php echo Url::toRoute(Yii::$app->controller->id . '/delete'); ?>');
            }
        });
    });
</script>
