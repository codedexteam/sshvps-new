<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Редактировать';
?>
<h1><?php echo 'Пользователи > ' . $this->title; ?></h1>

<?php $form = ActiveForm::begin([
    'options' => ['class' => 'form-horizontal',],
    'fieldConfig' => [
        'template' => '{label}<div class="col-lg-10">{input}</div>',
        'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
<div class="form-group">
    <label class="col-lg-2"></label>
    <div><?php echo $model->username; ?></div>
</div>
<div class="form-group">
    <label class="col-lg-2"></label>
    <div><?php echo $model->email; ?></div>
</div>
<?php echo $form->field($edit, 'role')->dropDownList($roles, ['options' => [$rollName => ['selected ' => true]]])->label('Роль'); ?>
<?php echo $form->field($edit, 'active')->input('checkbox', [
    'checked' => $model->status == 10 ? 'checked' : false,
    'class' => 'checkbox',
])->label('Активность'); ?>
<div class="form-group">
    <div class="col-lg-10 col-lg-offset-2">
        <a href="<?php echo Url::toRoute([Yii::$app->controller->id . '/index']); ?>" class="btn btn-warning">Вернуться к списку</a>
        <?php echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

