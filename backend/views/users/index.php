<?php
use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\models\Blog;

$this->title = 'Список';
//var_dump(Yii::$app->authManager->getRolesByUser(3));die;
?>
<h1><?php echo 'Пользователи > ' . $this->title; ?></h1>
<div class="row-fluid sections">
    <div class="progress progress-striped active sort-progress">
        <div class="progress-bar progress-bar-info" style="width: 100%"></div>
    </div>
    <table id="sort-table" class="table table-striped table-hover">
        <thead>
        <tr>
            <th>#</th>
            <th>Имя</th>
            <th>Роль</th>
            <th></th>
        </tr>
        </thead>
        <?php foreach ($users as $user) { ?>
            <tr>
                <td><?php echo $user->id; ?></td>
                <td><?php echo $user->username; ?></td>
                <td><?php foreach (Yii::$app->authManager->getRolesByUser($user->id) as $role) {echo $role->name;} ?></td>
                <td>
                    <div class="btn-group-vertical">
                        <a href="<?php echo Url::toRoute([Yii::$app->controller->id . '/edit', 'id' => $user->id]); ?>" class="btn btn-default btn-xs">Редактировать</a>
                    </div>
                </td>
            </tr>
        <?php } ?>
    </table>
</div>
