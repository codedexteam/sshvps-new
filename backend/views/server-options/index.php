<?php
use yii\helpers\Url;
use yii\helpers\Html;
use common\models\Option;

$this->title = $controllerTitle;
?>
<h1><?php echo 'Услуги > ' . $this->title; ?></h1>
<!--<div class="row-fluid">-->
<!--    <a href="--><?php //echo Url::toRoute(Yii::$app->controller->id . '/edit'); ?><!--" class="btn btn-success">Добавить пункт</a>-->
<!--</div>-->
<div class="row-fluid options">
    <ul class="nav navbar-nav">
        <li class="dropdown">
            <a class="dropdown-toggle btn btn-success" href="#" data-toggle="dropdown" role="button" aria-expanded="false" >Добавить услугу<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="<?php echo Url::toRoute(Yii::$app->controller->id . '/slide'); ?>"><span>Слайдер диапазона</span></a></li>
                <li><a href="<?php echo Url::toRoute(Yii::$app->controller->id . '/radio'); ?>"><span>Радио-кнопки</span></a></li>
                <li><a href="<?php echo Url::toRoute(Yii::$app->controller->id . '/list'); ?>"><span>Выпадающий список</span></a></li>
            </ul>
        </li>
    </ul>
</div>

<div class="sort">
    <button type="button" class="btn btn-info" id="sort">Сортировать</button>
    <button type="button" class="btn btn-primary" id="save-sort">Сохранить</button>
</div>

<div class="row-fluid sections">
    <div class="progress progress-striped active sort-progress">
        <div class="progress-bar progress-bar-info" style="width: 100%"></div>
    </div>
    <table class="table table-striped table-hover ">
        <thead>
        <tr>
            <th class="col-md-1">#</th>
            <th class="col-md-3">Название</th>
            <th class="col-md-1">Тип</th>
            <th class="col-md-1">Цена</th>
            <th class="col-md-2"></th>
            <th class="col-md-1">Активность</th>
            <th class="col-md-3"></th>
        </tr>
        </thead>
    </table>
    <ul id="accordion" class="sortable">
        <?php foreach ($options as $option) { ?>
            <?php if ($option->parent_id == null) { ?>
                <li class="clear" data-sort="<?php echo $option->sort; ?>" data-id="<?php echo $option->id; ?>">
                    <div class="accord" href="#id<?php echo $option->id; ?>">
                        <div class="col-md-1"><?php echo $option->id; ?></div>
                        <div class="col-md-3"><?php echo $option->ru[0]['title']; ?></div>
                        <div class="col-md-1"><?php echo $option->option_type != Option::OPTION_TYPE_SLIDE ? $option->option_type != Option::OPTION_TYPE_RADIO ? $option->option_type != Option::OPTION_TYPE_LIST ? '' : 'list' : 'radio' : 'slide'; ?></div>
                        <div class="col-md-1"><?php echo $option->price; ?></div>
                        <div class="col-md-2"><?php echo $option->option_type == Option::OPTION_TYPE_LIST || $option->option_type == Option::OPTION_TYPE_RADIO ? 'Пункт по умолчанию' : ''; ?></div>
                        <div class="status col-md-1" data-id="<?php echo $option->id; ?>"><?php echo $option->active ? 'Да' : 'Нет'; ?></div>
                        <div class="data col-md-3" data-id="<?php echo $option->id; ?>">
                            <div class="row">
                                <?php if ($option->option_type == Option::OPTION_TYPE_LIST || $option->option_type == Option::OPTION_TYPE_RADIO) {?>
                                <a href="<?php echo Url::toRoute([Yii::$app->controller->id . '/option', 'parent_id' => $option->id]); ?>" type="button" class="btn btn-xs btn-success">Add option</a>
                                <?php } ?>
                                <?php if ($option->option_type == Option::OPTION_TYPE_RADIO) {?>
                                <a href="<?php echo Url::toRoute([Yii::$app->controller->id . '/radio', 'id' => $option->id]); ?>" class="btn btn-default btn-xs" title="Редактировать">Edit</a>
                                <?php } ?>
                                <?php if ($option->option_type == Option::OPTION_TYPE_LIST) {?>
                                    <a href="<?php echo Url::toRoute([Yii::$app->controller->id . '/list', 'id' => $option->id]); ?>" class="btn btn-default btn-xs" title="Редактировать">Edit</a>
                                <?php } ?>
                                <?php if ($option->option_type == Option::OPTION_TYPE_SLIDE) {?>
                                    <a href="<?php echo Url::toRoute([Yii::$app->controller->id . '/slide', 'id' => $option->id]); ?>" class="btn btn-default btn-xs" title="Редактировать">Edit</a>
                                <?php } ?>
                                <a class="btn btn-primary btn-xs btn-activate mini" data-value="<?php echo $option->active == 1 ? 0 : 1; ?>" data-id="<?= $option->id; ?>" title="<?= $option->active ? 'Деактивировать' : 'Активировать'; ?>">
                                    <span><?php echo $option->active == 1 ? 'Deactivate' : 'Activate'; ?></span>
                                    <div class="progress progress-striped active" style="border-radius: 0;margin: 0;height: 3px; display: none">
                                        <div class="progress-bar" style="width: 100%"></div>
                                    </div>
                                </a>
                                <a class="btn btn-danger btn-xs btn-delete" data-id="<?= $option->id; ?>" title="Удалить">
                                    <span>Delete</span>
                                    <div class="progress progress-striped active" style="border-radius: 0;margin: 0;height: 3px; display: none">
                                        <div class="progress-bar progress-bar-danger" style="width: 100%"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php if (!empty($option->childItems)) { ?>
                        <ul class="sub-cat sortable">
                            <?php foreach ($option->childItems as $child) { ?>
                                <li class="clear" id="id<?php echo $child->id; ?>" data-sort="<?php echo $child->sort; ?>" data-id="<?php echo $child->id; ?>">
                                    <div class="col-md-1"><?php echo $child->id; ?></div>
                                    <div class="col-md-3"><?php echo $child->ru[0]['title']; ?></div>
                                    <span class="col-md-1"><?php echo $child->option_type != 1 ? $child->option_type != 2 ? $child->option_type != 3 ? '' : 'list' : 'radio' : 'slide'; ?></span>
                                    <span class="col-md-1"><?php echo $child->price; ?></span>
                                    <span class="col-md-2 option-checked"><?php echo $child->is_checked ? '<i class="fa fa-check"></i>' : ''; ?></span>
                                    <div class="status col-md-1" data-id="<?php echo $child->id; ?>"><?php echo $child->active ? 'Да' : 'Нет'; ?></div>
                                    <div class="data col-md-3" data-id="<?php echo $child->id; ?>">
                                        <div class="row">
                                            <a href="<?php echo Url::toRoute([Yii::$app->controller->id . '/option', 'id' => $child->id]); ?>" class="btn btn-default btn-xs" title="Редактировать">Edit</a>
                                            <a class="btn btn-primary btn-xs btn-activate mini" data-value="<?php echo $child->active == 1 ? 0 : 1; ?>" data-id="<?= $child->id; ?>" title="<?= $child->active ? 'Деактивировать' : 'Активировать'; ?>">
                                                <span><?php echo $child->active == 1 ? 'Deactivate' : 'Activate'; ?></span>
                                                <div class="progress progress-striped active" style="border-radius: 0;margin: 0;height: 3px; display: none">
                                                    <div class="progress-bar" style="width: 100%"></div>
                                                </div>
                                            </a>
                                            <a class="btn btn-danger btn-xs btn-delete" data-id="<?= $child->id; ?>" title="Удалить">
                                                <span>Delete</span>
                                                <div class="progress progress-striped active" style="border-radius: 0;margin: 0;height: 3px; display: none">
                                                    <div class="progress-bar progress-bar-danger" style="width: 100%"></div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                </li>
            <?php } ?>
        <?php } ?>
    </ul>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#accordion').accordion({
            heightStyle: "content", //Чтобы не менялась высота ul
            header: 'div.accord',
            navigation: true
        });

        $('#accordion a').click(function () {
            if ($(this).attr('href')) {
                location.href = $(this).attr('href');
                return false;
            }
        });

        $('.btn-activate').click(function () {
            var $this = $(this);
            var id = $this.data().id;
            var value = $this.attr('data-value');
            $this.find('.progress').show();
            $.ajax({
                url: '<?php echo Url::toRoute(Yii::$app->controller->id . '/active'); ?>',
                type: 'post',
                dataType: 'json',
                data: {
                    id: id,
                    value: value,
                    _csrf: yii.getCsrfToken()
                },
                success: function (response) {
                    if (response.status == true) {
                        if (value == 1) {
                            $this.find('span').text('Deactivate');
                            $('.btn-activate.mini[data-id=' + id + ']').attr('title', 'Деактивировать');
                            $this.attr('data-value', 0);
                            $('.status[data-id=' + id + ']').text('Да');
                        } else {
                            $this.find('span').text('Activate');
                            $('.btn-activate.mini[data-id=' + id + ']').attr('title', 'Активировать');
                            $this.attr('data-value', 1);
                            $('.status[data-id=' + id + ']').text('Нет');
                        }
                    }
                    $this.find('.progress').hide();
                },
                error: function () {
                    $this.find('.progress').hide();
                }
            });
            return false;
        });

        $('.btn-delete').click(function () {
            var agree = confirm('Вы действительно хотите удалить этот пункт?');
            if (agree) {
                var $this = $(this);
                $this.find('.progress').show();
                $.ajax({
                    url: '<?php echo Url::toRoute(Yii::$app->controller->id . '/delete'); ?>',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        id: $this.data().id,
                        _csrf: yii.getCsrfToken()
                    },
                    success: function (response) {
                        if (response.status == true) {
                            $('li[data-id=' + $this.data().id + ']').remove();
                        }
                        $this.find('.progress').hide();
                    },
                    error: function () {
                        $this.find('.progress').hide();
                    }
                });
            }
            return false;
        });

        $('#save-sort').click(function() {
            var data = {};
            var id, idSub;
            for (var i = 0; i < $('#accordion > li').length; i++) {
                id = $('#accordion > li').eq(i).attr('data-sort', i).data().id;
                data[id] = i;
            }
            for (var c = 0; c < $('.sub-cat > li').length; c++) {
                idSub = $('.sub-cat > li').eq(c).attr('data-sort', c).data().id;
                data[idSub] = c;
            }
            sortAjax(data, '<?php echo Url::toRoute(Yii::$app->controller->id . '/sort'); ?>');
        });
    });
</script>
