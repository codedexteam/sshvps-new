<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = Yii::$app->request->get('id') ? 'Редактировать' : 'Добавить';
?>
    <h1><?php echo 'Выпадающий список > ' . $this->title; ?></h1>

<?php $form = ActiveForm::begin([
    'options' => ['class' => 'form-horizontal',],
    'fieldConfig' => [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10">{input}</div>',
        'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
<?php echo $form->field($edit, 'title_ru', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/ru.png" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->ru) ? $model->ru[0]['title'] : ''])->label(); ?>

<?php echo $form->field($edit, 'title_en', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/en.jpg" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->en) ? $model->en[0]['title'] : ''])->label(); ?>

<?php echo $form->field($edit, 'title_zh', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/ch.png" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->zh) ? $model->zh[0]['title'] : ''])->label(); ?>

<?php $edit->description_ru = !empty($model->ru) ? $model->ru[0]['description'] : '';
echo $form->field($edit, 'description_ru', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/ru.png" alt="russian flag">{input}</div>']
)->textarea(['rows' => '6'])->label(); ?>

<?php $edit->description_en = !empty($model->ru) ? $model->en[0]['description'] : '';
echo $form->field($edit, 'description_en', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/en.png" alt="russian flag">{input}</div>']
)->textarea(['rows' => '6'])->label(); ?>

<?php $edit->description_zh = !empty($model->ru) ? $model->zh[0]['description'] : '';
echo $form->field($edit, 'description_zh', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/ch.png" alt="russian flag">{input}</div>']
)->textarea(['rows' => '6'])->label(); ?>

<?php echo $form->field($edit, 'price')->input('text', [
    'value' => $model->price,
    'placeholder' => '30.00'
])->label(); ?>

    <div class="form-group info-data">
        <div class="col-lg-10 col-lg-offset-2 bg-info">
            <span>Название иконки смотрите в списке иконок по следующей ссылке:</span>
            <a target="_blank" href="http://fontawesome.io/icons/">http://fontawesome.io/icons/</a>
            <span>Пример названия иконки: </span><b>fa-plus-circle</b>
        </div>
    </div>
<?php echo $form->field($edit, 'icon', [
    'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10 option-icon">{input}<i class="fa ' . $model->icon . '"></i></div>'
])->input('text', [
    'value' => $model->icon,
    'placeholder' => 'fa-plus-circle'
])->label(); ?>

<?php echo $form->field($edit, 'active')->input('checkbox', [
    'checked' => $model->active == 1 ? 'checked' : false,
    'class' => 'checkbox',
])->label('Активность'); ?>

    <div class="form-group">
        <div class="col-lg-10 col-lg-offset-2">
            <a href="<?php echo Url::toRoute([Yii::$app->controller->id . '/index']); ?>" class="btn btn-warning">Вернуться к списку</a>
            <?php echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>