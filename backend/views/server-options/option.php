<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = Yii::$app->request->get('id') ? 'Редактировать' : 'Добавить';
?>
<?php if (isset($error)) {?>
    <div class="form-group">
        <div class="col-lg-10 col-lg-offset-2">
            <p><?php echo $error; ?></p>
            <a href="<?php echo Url::toRoute([Yii::$app->controller->id . '/index']); ?>" class="btn btn-warning">Вернуться к списку</a>
        </div>
    </div>
    <?php } else { ?>
    <h1><?php echo 'Опция > ' . $this->title; ?></h1>

<?php $form = ActiveForm::begin([
    'options' => ['class' => 'form-horizontal',],
    'fieldConfig' => [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10">{input}</div>',
        'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
<?php echo $form->field($edit, 'title_ru', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/ru.png" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->ru) ? $model->ru[0]['title'] : ''])->label(); ?>

<?php echo $form->field($edit, 'title_en', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/en.jpg" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->en) ? $model->en[0]['title'] : ''])->label(); ?>

<?php echo $form->field($edit, 'title_zh', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/ch.png" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->zh) ? $model->zh[0]['title'] : ''])->label(); ?>

<?php echo $form->field($edit, 'price')->input('text', [
    'value' => $model->price,
    'placeholder' => '30.00'
])->label(); ?>

<?php echo $form->field($edit, 'is_checked')->input('checkbox', [
    'checked' => $model->is_checked == 1 ? 'checked' : false,
    'class' => 'checkbox',
])->label(); ?>

<?php echo $form->field($edit, 'active')->input('checkbox', [
    'checked' => $model->active == 1 ? 'checked' : false,
    'class' => 'checkbox',
])->label(); ?>

    <div class="form-group">
        <div class="col-lg-10 col-lg-offset-2">
            <a href="<?php echo Url::toRoute([Yii::$app->controller->id . '/index']); ?>" class="btn btn-warning">Вернуться к списку</a>
            <?php echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>
<?php } ?>
