<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = Yii::$app->request->get('id') ? 'Редактировать' : 'Добавить';
?>
<h1><?php echo 'VPS тарифы > ' . $this->title; ?></h1>
<?php $form = ActiveForm::begin([
    'options' => ['class' => 'form-horizontal',],
    'fieldConfig' => [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10">{input}</div>',
        'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
<?php echo $form->field($edit, 'billing_id')->input('text', ['value' => $model->billing_id])->label(); ?>
<?php echo $form->field($edit, 'title_ru', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/ru.png" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->ru) ? $model->ru[0]['title'] : ''])->label(); ?>
<?php echo $form->field($edit, 'title_en', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/en.jpg" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->en) ? $model->en[0]['title'] : ''])->label(); ?>
<?php echo $form->field($edit, 'title_zh', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/ch.png" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->zh) ? $model->zh[0]['title'] : ''])->label(); ?>
<?php echo $form->field($edit, 'price')->input('text', ['value' => $model->price])->label(); ?>

<?php echo $form->field($edit, 'cpu_ru', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/ru.png" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->ru) ? $model->ru[0]['cpu'] : ''])->label(); ?>
<?php echo $form->field($edit, 'cpu_en', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/en.jpg" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->en) ? $model->en[0]['cpu'] : ''])->label(); ?>
<?php echo $form->field($edit, 'cpu_zh', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/ch.png" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->zh) ? $model->zh[0]['cpu'] : ''])->label(); ?>

<?php echo $form->field($edit, 'ram_ru', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/ru.png" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->ru) ? $model->ru[0]['ram'] : ''])->label(); ?>
<?php echo $form->field($edit, 'ram_en', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/en.jpg" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->en) ? $model->en[0]['ram'] : ''])->label(); ?>
<?php echo $form->field($edit, 'ram_zh', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/ch.png" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->zh) ? $model->zh[0]['ram'] : ''])->label(); ?>

<?php echo $form->field($edit, 'hdd_ru', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/ru.png" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->ru) ? $model->ru[0]['hdd'] : ''])->label(); ?>
<?php echo $form->field($edit, 'hdd_en', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/en.jpg" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->en) ? $model->en[0]['hdd'] : ''])->label(); ?>
<?php echo $form->field($edit, 'hdd_zh', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/ch.png" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->zh) ? $model->zh[0]['hdd'] : ''])->label(); ?>

<?php echo $form->field($edit, 'traff_ru', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/ru.png" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->ru) ? $model->ru[0]['traff'] : ''])->label(); ?>
<?php echo $form->field($edit, 'traff_en', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/en.jpg" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->en) ? $model->en[0]['traff'] : ''])->label(); ?>
<?php echo $form->field($edit, 'traff_zh', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/ch.png" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->zh) ? $model->zh[0]['traff'] : ''])->label(); ?>

<?php echo $form->field($edit, 'virtual_ru', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/ru.png" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->ru) ? $model->ru[0]['virtual'] : ''])->label(); ?>
<?php echo $form->field($edit, 'virtual_en', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/en.jpg" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->en) ? $model->en[0]['virtual'] : ''])->label(); ?>
<?php echo $form->field($edit, 'virtual_zh', [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10"><img class="flag" src="/images/system/ch.png" alt="russian flag">{input}</div>']
)->input('text', ['value' => !empty($model->zh) ? $model->zh[0]['virtual'] : ''])->label(); ?>
<div class="form-group info-data">
    <div class="col-lg-10 col-lg-offset-2 bg-info">
        <span >Выделите один или несколько пунктов, зажав клавишу "Ctrl"</span>
    </div>
</div>
<?php echo $form->field($edit, 'os[]')->listBox($os, [
        'options' => $vpsOsAr,
        'size' => 5,
        'multiple' => true
    ])->label(); ?>
<?php echo $form->field($edit, 'img')
    ->radioList([
        'package-1.png' => '<img src="/images/system/package-1.png">',
        'package-3.png' => '<img src="/images/system/package-3.png">',
        'package-2.png' => '<img src="/images/system/package-2.png">',
        'package-4.png' => '<img src="/images/system/package-4.png">',
    ],
        [
            'value' => $model->img,
            'item' => function($index, $label, $name, $checked, $value) {
                $checked = $checked ? 'checked' : '';
                $return = '<label class="checkbox-inline">';
                $return .= '<input type="radio" name="' . $name . '" value="' . $value . '"' . $checked . '>';
                $return .= '<span>' . ucwords($label) . '</span>';
                $return .= '</label>';
                return $return;}
        ])->label(); ?>
<?php echo $form->field($edit, 'spec')->input('checkbox', [
    'checked' => $model->spec == 1 ? 'checked' : false,
    'class' => 'checkbox',
])->label(); ?>
<?php echo $form->field($edit, 'active')->input('checkbox', [
    'checked' => $model->active == 1 ? 'checked' : false,
    'class' => 'checkbox',
])->label(); ?>
<div class="form-group">
    <div class="col-lg-10 col-lg-offset-2">
        <a href="<?php echo Url::toRoute([Yii::$app->controller->id . '/index']); ?>" class="btn btn-warning">Вернуться к списку</a>
        <?php echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

