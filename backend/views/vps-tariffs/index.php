<?php
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\helpers\Html;

$this->title = 'VPS тарифы';
?>
<h1><?php echo 'Серверы > ' . $this->title; ?></h1>
<div class="row-fluid add">
    <a href="<?php echo Url::toRoute(Yii::$app->controller->id . '/edit'); ?>" class="btn btn-success">Добавить тариф</a>
</div>
<div class="row-fluid sections">
    <div class="progress progress-striped active sort-progress">
        <div class="progress-bar progress-bar-info" style="width: 100%"></div>
    </div>
    <table id="sort-table" class="table table-striped table-hover">
        <thead>
        <tr>
            <th>#</th>
            <th>billing_id</th>
            <th>Название</th>
            <th>Цена</th>
            <th>Спец. Предложение</th>
            <th>Активность</th>
            <th></th>
        </tr>
        </thead>
        <?php if (!empty($tariffs)) { ?>
        <?php foreach ($tariffs as $tariff) { ?>
            <tr>
                <td><?php echo $tariff->id; ?></td>
                <td><?php echo $tariff->billing_id; ?></td>
                <td><?php echo $tariff->ru[0]->title; ?></td>
                <td><?php echo $tariff->price; ?></td>
                <td><?php echo $tariff->spec ? 'Да' : 'Нет'; ?></td>
                <td class="status"><?php echo $tariff->active ? 'Да' : 'Нет'; ?></td>
                <td>
                    <div class="data col-md-12" data-id="<?php echo $tariff->id; ?>">
                        <div class="row">
                            <a href="<?php echo Url::toRoute([Yii::$app->controller->id . '/edit', 'id' => $tariff->id]); ?>" class="btn btn-default btn-xs" title="Редактировать">Edit</a>
                            <a class="btn btn-primary btn-xs btn-activate mini" data-value="<?php echo $tariff->active == 1 ? 0 : 1; ?>" data-id="<?= $tariff->id; ?>" title="<?= $tariff->active ? 'Деактивировать' : 'Активировать'; ?>">
                                <span><?php echo $tariff->active == 1 ? 'Deactivate' : 'Activate'; ?></span>
                                <div class="progress progress-striped active" style="border-radius: 0;margin: 0;height: 3px; display: none">
                                    <div class="progress-bar" style="width: 100%"></div>
                                </div>
                            </a>
                            <a class="btn btn-danger btn-xs btn-delete" data-id="<?= $tariff->id; ?>" title="Удалить">
                                <span>Delete</span>
                                <div class="progress progress-striped active" style="border-radius: 0;margin: 0;height: 3px; display: none">
                                    <div class="progress-bar progress-bar-danger" style="width: 100%"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                </td>
            </tr>
        <?php } ?>
       <?php } ?>
    </table>
    <div class="row-fluid pager">
        <?php echo LinkPager::widget([
            'pagination' => $pager,
        ]); ?>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.btn-activate').click(function () {
            var $this = $(this);
            activeAjax($this, '<?php echo Url::toRoute(Yii::$app->controller->id . '/active'); ?>');
        });

        $('.btn-delete').click(function () {
            var agree = confirm('Вы действительно хотите удалить этот пункт?');
            if (agree) {
                var $this = $(this);
                deleteAjax($this, '<?php echo Url::toRoute(Yii::$app->controller->id . '/delete'); ?>');
            }
        });
    });
</script>
