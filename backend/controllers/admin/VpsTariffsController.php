<?php

namespace backend\controllers\admin;

use common\models\VpsTariff;
use common\models\Os;
use common\models\VpsOs;
use common\models\LangVpsTariff;
use yii\data\Pagination;
use common\models\forms\EditVpsTariffsForm;
use yii\helpers\Url;
use yii\web\Response;
use yii\web\HttpException;
use Yii;


class VpsTariffsController extends AdminController {

    public function actionIndex() {
        $tariffs = (new VpsTariff())->getAllTariffs([VpsTariff::tableName() . '.is_deleted' => 0], false);
        $pagerTariffs = new Pagination(['totalCount' => $tariffs->count(), 'pageSize' => self::PAGE_SIZE]);
        $pagerTariffs->pageSizeParam = false;

        $allTariffs = $tariffs->offset($pagerTariffs->offset)
            ->limit($pagerTariffs->limit)
            ->all();
        
        return $this->render('index', [
            'tariffs' => $allTariffs,
            'pager' => $pagerTariffs
        ]);
    }

    public function actionEdit() {
        $id = Yii::$app->request->getQueryParam('id') ? Yii::$app->request->getQueryParam('id') : null;

        $vps = new VpsTariff;
        if ($id) {
            $isVps = (new VpsTariff())->getAllTariffs([VpsTariff::tableName() . '.id' => $id]);
            $vps = !empty($isVps) ? $isVps[0] : new VpsTariff();
        }
        $form = new EditVpsTariffsForm();
        $osAll = (new Os())->getAll();
        $vpsOs = new VpsOs();
        $vpsOsAr = [];
        $os = [];

        if (!empty($osAll)) {
            foreach ($osAll as $osItem) {
                $os[$osItem['id']] = $osItem['title'];
            }
        }

        if (!empty($vps)) {
            foreach ($vps->vpsOs as $item) {
                foreach ($item->os as $osItem) {
                    $vpsOsAr[$osItem['id']] = ['selected ' => true];
                }
            }

            if ($form->load(Yii::$app->request->post()) && $form->validate()) {
                $vps->billing_id = $form->billing_id;
                $vps->price = $form->price;
                $vps->img = $form->img;
                $vps->spec = isset(Yii::$app->request->post('EditVpsTariffsForm')['spec']) ? 1 : 0;
                $vps->active = isset(Yii::$app->request->post('EditVpsTariffsForm')['active']) ? 1 : 0;
                $vps->save();

                $idVps = $id ? $id : Yii::$app->db->lastInsertID;

                $langRu = LangVpsTariff::findOne(['tariff_id' => $idVps, 'lang' => 'ru']);
                if (!empty($langRu)) {
                    $langRu->title = $form->title_ru;
                    $langRu->cpu = $form->cpu_ru;
                    $langRu->ram = $form->ram_ru;
                    $langRu->hdd = $form->hdd_ru;
                    $langRu->traff = $form->traff_ru;
                    $langRu->virtual = $form->virtual_ru;
                    $langRu->update();
                } else {
                    $langRu = new LangVpsTariff();
                    $data = [[
                        'tariff_id' => $idVps,
                        'lang' => 'ru',
                        'title' => $form->title_ru,
                        'cpu' => $form->cpu_ru,
                        'ram' => $form->ram_ru,
                        'hdd' => $form->hdd_ru,
                        'traff' => $form->traff_ru,
                        'virtual' => $form->virtual_ru
                    ]];
                    $langRu->insertData(LangVpsTariff::tableName(), $data, true);
                }

                $langEn = LangVpsTariff::findOne(['tariff_id' => $idVps, 'lang' => 'en']);
                if (!empty($langEn)) {
                    $langEn->title = $form->title_en;
                    $langEn->cpu = $form->cpu_en;
                    $langEn->ram = $form->ram_en;
                    $langEn->hdd = $form->hdd_en;
                    $langEn->traff = $form->traff_en;
                    $langEn->virtual = $form->virtual_en;
                    $langEn->update();
                } else {
                    $langEn = new LangVpsTariff();
                    $data = [[
                        'tariff_id' => $idVps,
                        'lang' => 'en',
                        'title' => $form->title_en,
                        'cpu' => $form->cpu_en,
                        'ram' => $form->ram_en,
                        'hdd' => $form->hdd_en,
                        'traff' => $form->traff_en,
                        'virtual' => $form->virtual_en
                    ]];
                    $langEn->insertData(LangVpsTariff::tableName(), $data, true);
                }

                $langZh = LangVpsTariff::findOne(['tariff_id' => $idVps, 'lang' => 'zh']);
                if (!empty($langZh)) {
                    $langZh->title = $form->title_zh;
                    $langZh->cpu = $form->cpu_zh;
                    $langZh->ram = $form->ram_zh;
                    $langZh->hdd = $form->hdd_zh;
                    $langZh->traff = $form->traff_zh;
                    $langZh->virtual = $form->virtual_zh;
                    $langZh->update();
                } else {
                    $langZh = new LangVpsTariff();
                    $data = [[
                        'tariff_id' => $idVps,
                        'lang' => 'zh',
                        'title' => $form->title_zh,
                        'cpu' => $form->cpu_zh,
                        'ram' => $form->ram_zh,
                        'hdd' => $form->hdd_zh,
                        'traff' => $form->traff_zh,
                        'virtual' => $form->virtual_zh
                    ]];
                    $langZh->insertData(LangVpsTariff::tableName(), $data, true);
                }

                $osFromForm = Yii::$app->request->post('EditVpsTariffsForm')['os'];
                if (!empty($osFromForm)) {
                    $vpsOs->deleteAll(['tariff_id' => $id]);
                    $i = 0;
                    $data = [];
                    foreach ($osFromForm as $osItem) {
                        $data[$i]['tariff_id'] = $idVps;
                        $data[$i]['os_id'] = $osItem;
                        $i++;
                    }
                    $vpsOs->insertData(VpsOs::tableName(), $data, true);
                }

                Yii::$app->getResponse()->redirect(Url::toRoute([Yii::$app->controller->id . '/edit', 'id' => $idVps]));
            }

            return $this->render('edit', [
                'edit' => $form,
                'model' => $vps,
                'os' => $os,
                'vpsOsAr' => $vpsOsAr
            ]);
        } else {
            throw new HttpException(404 ,'Такой страницы нет!');
        }
    }

    public function actionActive() {
        if (Yii::$app->request->isAjax) {
            $response = false;

            $id = (int)Yii::$app->request->post()['id'];
            $value = (int)Yii::$app->request->post()['value'];

            $vpsTariff = VpsTariff::findOne($id);
            $vpsTariff->active = $value;
            if ($vpsTariff->update() !== false) {
                $response = true;
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'status' => $response,
            ];
        }
        Yii::$app->end();
    }

    public function actionDelete() {
        if (Yii::$app->request->isAjax) {
            $response = false;

            $id = (int)Yii::$app->request->post()['id'];

            $vpsTariff = VpsTariff::findOne($id);
            $vpsTariff->is_deleted = 1;
            $response = $vpsTariff->save();

            if ($response > 0) {
                $response = true;
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'status' => $response,
            ];
        }
        Yii::$app->end();
    }

}