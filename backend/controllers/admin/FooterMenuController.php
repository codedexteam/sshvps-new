<?php

namespace backend\controllers\admin;

use common\models\MenuType;

class FooterMenuController extends MainMenuController {

    public $menuId = MenuType::FOOTER_ID;
    public $controllerTitle = 'Футер';

}