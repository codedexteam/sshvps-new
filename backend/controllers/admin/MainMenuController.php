<?php

namespace backend\controllers\admin;

use common\models\LangMenuItem;
use Yii;
use common\models\MenuItem;
use common\models\MenuType;
use yii\data\Pagination;
use common\models\forms\EditMenuForm;
use yii\helpers\Url;
use yii\web\HttpException;
use yii\web\Response;


class MainMenuController extends AdminController {

    public $menuId = MenuType::MAIN_MENU_ID;
    public $controllerTitle = 'Главное меню';

    public function actionIndex() {
        $items = (new MenuItem())->getAllItems([MenuItem::tableName(). '.type_id' => $this->menuId], true, [
            MenuItem::tableName() . '.sort' => SORT_ASC,
            MenuItem::tableName() . '.id' => SORT_DESC,
        ]);

        return $this->render('/main-menu/index', [
            'items' => $items,
            'controllerTitle' => $this->controllerTitle
        ]);
    }

    public function actionEdit() {
        $id = Yii::$app->request->getQueryParam('id') ? Yii::$app->request->getQueryParam('id') : null;

        $item = new MenuItem();
        if ($id) {
            $isTtem = (new MenuItem())->getAllItems([MenuItem::tableName() . '.id' => $id]);
            $item = !empty($isTtem) ? $isTtem[0] : new MenuItem();
        }
        $form = new EditMenuForm();
        $parenItems = (new MenuItem())->getAllItems([
            MenuItem::tableName(). '.type_id' => $this->menuId,
            MenuItem::tableName(). '.parent_id' => null,
        ], true, [
            MenuItem::tableName() . '.sort' => SORT_ASC,
            MenuItem::tableName() . '.id' => SORT_DESC,
        ]);

        $parenItemsAr[0] = 'Нет';

        if (!empty($parenItems)) {
            foreach ($parenItems as $parentItem) {
                $parenItemsAr[$parentItem['id']] = $parentItem->ru[0]['title'];
            }
        }

        if (!empty($item)) {
            if ($form->load(Yii::$app->request->post()) && $form->validate()) {
                $item->link = $form->link ? $form->link : null;
                $item->parent_id = $form->parent_id ? $form->parent_id : null;
                $item->type_id = $this->menuId;
                $item->active = isset(Yii::$app->request->post('EditMenuForm')['active']) ? 1 : 0;
                $item->icon = $form->icon ? $form->icon : null;
                $item->save();

                $idLastItem = $id ? $id : Yii::$app->db->lastInsertID;

                if ($id) {
                    $langRu = LangMenuItem::findOne(['item_id' => $id, 'lang' => 'ru']);
                    if (!empty($langRu)) {
                        $langRu->title = $form->title_ru;
                        $langRu->update();
                    } else {
                        $langRu = new LangMenuItem();
                        $data = [[
                            'item_id' => $idLastItem,
                            'lang' => 'ru',
                            'title' => $form->title_ru
                        ]];
                        $langRu->insertData(LangMenuItem::tableName(), $data, true);
                    }

                    $langEn = LangMenuItem::findOne(['item_id' => $id, 'lang' => 'en']);
                    if (!empty($langEn)) {
                        $langEn->title = $form->title_en;
                        $langEn->update();
                    } else {
                        $langEn = new LangMenuItem();
                        $data = [[
                            'item_id' => $idLastItem,
                            'lang' => 'en',
                            'title' => $form->title_en
                        ]];
                        $langEn->insertData(LangMenuItem::tableName(), $data, true);
                    }

                    $langZh = LangMenuItem::findOne(['item_id' => $id, 'lang' => 'zh']);
                    if (!empty($langZh)) {
                        $langZh->title = $form->title_zh;
                        $langZh->update();
                    } else {
                        $langZh = new LangMenuItem();
                        $data = [[
                            'item_id' => $idLastItem,
                            'lang' => 'zh',
                            'title' => $form->title_zh
                        ]];
                        $langZh->insertData(LangMenuItem::tableName(), $data, true);
                    }
                } else {
                    $data = [
                        [
                            'item_id' => $idLastItem,
                            'lang' => 'ru',
                            'title' => $form->title_ru
                        ],
                        [
                            'item_id' => $idLastItem,
                            'lang' => 'en',
                            'title' => $form->title_en
                        ],
                        [
                            'item_id' => $idLastItem,
                            'lang' => 'zh',
                            'title' => $form->title_zh
                        ],
                    ];
                    $lang_item = new LangMenuItem();
                    $lang_item->insertData(LangMenuItem::tableName(), $data, true);
                }

                Yii::$app->getResponse()->redirect(Url::toRoute([Yii::$app->controller->id . '/edit', 'id' => $idLastItem]));
            }

            return $this->render('/main-menu/edit', [
                'edit' => $form,
                'model' => $item,
                'parenItems' => $parenItemsAr,
                'controllerTitle' => $this->controllerTitle
                
            ]);
        } else {
            throw new HttpException(404 ,'Такой страницы нет!');
        }
    }

    public function actionActive() {
        if (Yii::$app->request->isAjax) {
            $response = false;

            $id = (int)Yii::$app->request->post()['id'];
            $value = (int)Yii::$app->request->post()['value'];

            $item = MenuItem::findOne($id);
            $item->active = $value;
            if ($item->update() !== false) {
                $response = true;
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'status' => $response,
            ];
        }
        Yii::$app->end();
    }

    public function actionDelete() {
        if (Yii::$app->request->isAjax) {
            $response = false;

            $id = (int)Yii::$app->request->post()['id'];

            $item = MenuItem::findOne($id);
            if ($item->delete() !== false) {
                MenuItem::deleteAll(['parent_id' => $id]);
                $response = true;
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'status' => $response,
            ];
        }
        Yii::$app->end();
    }

    public function actionSort() {
        if (Yii::$app->request->isAjax) {
            $response = false;

            $data = Yii::$app->request->post()['data'];

            $item = new MenuItem();
            $item->updateData(MenuItem::tableName(), 'sort', $data, 'id');

            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'status' => true,
            ];
        }
        Yii::$app->end();
    }

}