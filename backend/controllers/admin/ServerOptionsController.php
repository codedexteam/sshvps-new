<?php

namespace backend\controllers\admin;

use Yii;
use common\models\Option;
use common\models\LangOption;
use backend\models\forms\Option as OptionForm;
use yii\helpers\Url;
use yii\web\HttpException;
use yii\web\Response;


class ServerOptionsController extends AdminController {

    public $controllerTitle = 'Выделенные серверы';
    public $serverType = Option::TYPE_SERVER;

    public function actionIndex() {

        $options = Option::getAllOptions(['options.server_type' => $this->serverType], true,
            ['options.sort' => SORT_ASC, 'options.id' => SORT_DESC]);
        
        return $this->render('/server-options/index', [
            'options' => $options,
            'controllerTitle' => $this->controllerTitle
        ]);
    }
    
    public function actionSlide() {
        $id = Yii::$app->request->getQueryParam('id') ? Yii::$app->request->getQueryParam('id') : null;
        $form = new OptionForm();
        $form->setScenario('slide');

        if ($id) {
            $option = Option::getOneOption(['option.id' => $id]);
            if (empty($option)) {
                throw new HttpException(404 ,'Такой страницы нет!');
            }
        } else {
            $option = new Option();
        }

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {

            $option->min = $form->min;
            $option->max = $form->max;
            $option->icon = $form->icon;
            $option->server_type = $this->serverType;
            $option->option_type = Option::OPTION_TYPE_SLIDE;
            $option->price = !empty($form->price) ? $form->price : 0;
            $option->active = isset(Yii::$app->request->post('Option')['active']) ? 1 : 0;
            $option->save();

            $lastId = $id ? $id : Yii::$app->db->lastInsertID;

            $langRu = $id ? LangOption::findOne(['option_id' => $id, 'lang' => 'ru']) : new LangOption;
            $langRu->title = $form->title_ru;
            $langRu->description = $form->description_ru;
            $langRu->option_id = $lastId;
            if (!$id) {
                $langRu->lang = 'ru';
            }
            $langRu->save();

            $langEn = $id ? LangOption::findOne(['option_id' => $id, 'lang' => 'en']) : new LangOption;
            $langEn->title = $form->title_en;
            $langEn->description = $form->description_en;
            $langEn->option_id = $lastId;
            if (!$id) {
                $langEn->lang = 'en';
            }
            $langEn->save();

            $langZh = $id ? LangOption::findOne(['option_id' => $id, 'lang' => 'zh']) : new LangOption;
            $langZh->title = $form->title_zh;
            $langZh->description = $form->description_zh;
            $langZh->option_id = $lastId;
            if (!$id) {
                $langZh->lang = 'zh';
            }
            $langZh->save();

            Yii::$app->getResponse()->redirect(Url::toRoute([Yii::$app->controller->id . '/slide', 'id' => $lastId]));
        }
        
        return $this->render('/server-options/slide', [
            'edit' => $form,
            'model' => $option,
        ]);
    }

    public function actionRadio() {
        $id = Yii::$app->request->getQueryParam('id') ? Yii::$app->request->getQueryParam('id') : null;
        $form = new OptionForm();
        $form->setScenario('list');

        if ($id) {
            $option = Option::getOneOption(['option.id' => $id]);
            if (empty($option)) {
                throw new HttpException(404 ,'Такой страницы нет!');
            }
        } else {
            $option = new Option();
        }

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {

            $option->icon = $form->icon;
            $option->server_type = $this->serverType;
            $option->option_type = Option::OPTION_TYPE_RADIO;
            $option->price = !empty($form->price) ? $form->price : 0;
            $option->active = isset(Yii::$app->request->post('Option')['active']) ? 1 : 0;
            $option->save();

            $lastId = $id ? $id : Yii::$app->db->lastInsertID;

            $langRu = $id ? LangOption::findOne(['option_id' => $id, 'lang' => 'ru']) : new LangOption;
            $langRu->title = $form->title_ru;
            $langRu->description = $form->description_ru;
            $langRu->option_id = $lastId;
            if (!$id) {
                $langRu->lang = 'ru';
            }
            $langRu->save();

            $langEn = $id ? LangOption::findOne(['option_id' => $id, 'lang' => 'en']) : new LangOption;
            $langEn->title = $form->title_en;
            $langEn->description = $form->description_en;
            $langEn->option_id = $lastId;
            if (!$id) {
                $langEn->lang = 'en';
            }
            $langEn->save();

            $langZh = $id ? LangOption::findOne(['option_id' => $id, 'lang' => 'zh']) : new LangOption;
            $langZh->title = $form->title_zh;
            $langZh->description = $form->description_zh;
            $langZh->option_id = $lastId;
            if (!$id) {
                $langZh->lang = 'zh';
            }
            $langZh->save();

            Yii::$app->getResponse()->redirect(Url::toRoute([Yii::$app->controller->id . '/radio', 'id' => $lastId]));
        }

        return $this->render('/server-options/radio', [
            'edit' => $form,
            'model' => $option,
        ]);
    }
    
    public function actionList() {
        $id = Yii::$app->request->getQueryParam('id') ? Yii::$app->request->getQueryParam('id') : null;
        $form = new OptionForm();
        $form->setScenario('list');

        if ($id) {
            $option = Option::getOneOption(['option.id' => $id]);
            if (empty($option)) {
                throw new HttpException(404 ,'Такой страницы нет!');
            }
        } else {
            $option = new Option();
        }

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {

            $option->icon = $form->icon;
            $option->server_type = $this->serverType;
            $option->option_type = Option::OPTION_TYPE_LIST;
            $option->price = !empty($form->price) ? $form->price : 0;
            $option->active = isset(Yii::$app->request->post('Option')['active']) ? 1 : 0;
            $option->save();

            $lastId = $id ? $id : Yii::$app->db->lastInsertID;

            $langRu = $id ? LangOption::findOne(['option_id' => $id, 'lang' => 'ru']) : new LangOption;
            $langRu->title = $form->title_ru;
            $langRu->description = $form->description_ru;
            $langRu->option_id = $lastId;
            if (!$id) {
                $langRu->lang = 'ru';
            }
            $langRu->save();

            $langEn = $id ? LangOption::findOne(['option_id' => $id, 'lang' => 'en']) : new LangOption;
            $langEn->title = $form->title_en;
            $langEn->description = $form->description_en;
            $langEn->option_id = $lastId;
            if (!$id) {
                $langEn->lang = 'en';
            }
            $langEn->save();

            $langZh = $id ? LangOption::findOne(['option_id' => $id, 'lang' => 'zh']) : new LangOption;
            $langZh->title = $form->title_zh;
            $langZh->description = $form->description_zh;
            $langZh->option_id = $lastId;
            if (!$id) {
                $langZh->lang = 'zh';
            }
            $langZh->save();

            Yii::$app->getResponse()->redirect(Url::toRoute([Yii::$app->controller->id . '/list', 'id' => $lastId]));
        }

        return $this->render('/server-options/list', [
            'edit' => $form,
            'model' => $option,
        ]);
    }

    public function actionOption() {
        $id = Yii::$app->request->getQueryParam('id') ? Yii::$app->request->getQueryParam('id') : null;
        $parent_id = Yii::$app->request->getQueryParam('parent_id') ? Yii::$app->request->getQueryParam('parent_id') : null;

        if ($id) {
            $option = Option::getOneOption(['option.id' => $id]);
            if (empty($option)) {
                throw new HttpException(404 ,'Такой страницы нет!');
            }
        } else {
            if (!$parent_id) {
                $error = 'Раздел для опции не выбран!';
                
                return $this->render('/server-options/option', [
                    'error' => $error
                ]);
            } else {
                $option = new Option();
            }
        }

        $form = new OptionForm();
        $form->setScenario('option');

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {

            if ($parent_id) {
                $option->parent_id = $parent_id;
            }
            $option->price = !empty($form->price) ? $form->price : 0;
            $option->is_checked = 0;
            if ($id) {
                if (isset(Yii::$app->request->post('Option')['is_checked'])) {
                    Option::updateAll(['is_checked' => 0], ['and', ['parent_id' => $option->parent_id], ['!=', 'id', $id]]);
                    $option->is_checked = 1;
                }
            } else {
                if (isset(Yii::$app->request->post('Option')['is_checked'])) {
                    Option::updateAll(['is_checked' => 0], ['parent_id' => $parent_id]);
                    $option->is_checked = 1;
                }
            }

            $option->active = isset(Yii::$app->request->post('Option')['active']) ? 1 : 0;
            $option->save();

            $lastId = $id ? $id : Yii::$app->db->lastInsertID;

            $langRu = $id ? LangOption::findOne(['option_id' => $id, 'lang' => 'ru']) : new LangOption;
            $langRu->title = $form->title_ru;
            $langRu->option_id = $lastId;
            if (!$id) {
                $langRu->lang = 'ru';
            }
            $langRu->save();

            $langEn = $id ? LangOption::findOne(['option_id' => $id, 'lang' => 'en']) : new LangOption;
            $langEn->title = $form->title_en;
            $langEn->option_id = $lastId;
            if (!$id) {
                $langEn->lang = 'en';
            }
            $langEn->save();

            $langZh = $id ? LangOption::findOne(['option_id' => $id, 'lang' => 'zh']) : new LangOption;
            $langZh->title = $form->title_zh;
            $langZh->option_id = $lastId;
            if (!$id) {
                $langZh->lang = 'zh';
            }
            $langZh->save();

            Yii::$app->getResponse()->redirect(Url::toRoute([Yii::$app->controller->id . '/option', 'id' => $lastId]));
        }

        return $this->render('/server-options/option', [
            'edit' => $form,
            'model' => $option,
        ]);
    }

    public function actionActive() {
        if (Yii::$app->request->isAjax) {
            $response = false;

            $id = (int)Yii::$app->request->post()['id'];
            $value = (int)Yii::$app->request->post()['value'];

            $option = Option::findOne($id);
            $option->active = $value;
            if ($option->update() !== false) {
                $response = true;
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'status' => $response,
            ];
        }
        Yii::$app->end();
    }

    public function actionDelete() {
        if (Yii::$app->request->isAjax) {
            $response = false;

            $id = (int)Yii::$app->request->post()['id'];

            $option = Option::findOne($id);
            if ($option->delete() !== false) {
                Option::deleteAll(['parent_id' => $id]);
                $response = true;
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'status' => $response,
            ];
        }
        Yii::$app->end();
    }

    public function actionSort() {
        if (Yii::$app->request->isAjax) {
            $response = false;

            $data = Yii::$app->request->post()['data'];

            $option = new Option();
            $option->updateData(Option::tableName(), 'sort', $data, 'id');

            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'status' => true,
            ];
        }
        Yii::$app->end();
    }

}