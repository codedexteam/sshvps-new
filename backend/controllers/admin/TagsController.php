<?php

namespace backend\controllers\admin;

use Yii;
use common\models\MetaTag;
use common\models\LangMetaTag;
use yii\data\Pagination;
use backend\models\forms\Tags;
use yii\helpers\Url;

class TagsController extends AdminController {
    
    public function actionIndex() {
        $tags = MetaTag::getAllTags(false, false, ['tags.id' => SORT_ASC]);
        $pager = new Pagination(['totalCount' => $tags->count(), 'pageSize' => self::PAGE_SIZE]);

        $allTags = $tags->offset($pager->offset)
            ->limit($pager->limit)
            ->all();

        return $this->render('index', [
            'tags' => $allTags,
            'pager' => $pager
        ]);
    }

    public function actionEdit() {
        $id = Yii::$app->request->getQueryParam('id') ? Yii::$app->request->getQueryParam('id') : null;
        $form = new Tags();

        $tag = $id ? MetaTag::getOneTag(['tag.id' => $id]) : new MetaTag();

        if ($id) {
            $form->setOldAttribute($tag->page_key);
        }

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {

            $tag->page_description = $form->page_description;
            $tag->page_key = $form->page_key;
            $tag->save();
            $lastId = $id ? $id : Yii::$app->db->lastInsertID;

            $langRu = $id ? LangMetaTag::findOne(['mata_tag_id' => $id, 'lang' => 'ru']) : new LangMetaTag;
            $langRu->title = $form->title_ru;
            $langRu->description = $form->description_ru;
            $langRu->keywords = $form->keywords_ru;
            $langRu->mata_tag_id = $lastId;
            if (!$id) {
                $langRu->lang = 'ru';
            }
            $langRu->save();

            $langEn = $id ? LangMetaTag::findOne(['mata_tag_id' => $id, 'lang' => 'en']) : new LangMetaTag;
            $langEn->title = $form->title_en;
            $langEn->description = $form->description_en;
            $langEn->keywords = $form->keywords_en;
            $langEn->mata_tag_id = $lastId;
            if (!$id) {
                $langEn->lang = 'en';
            }
            $langEn->save();

            $langZh = $id ? LangMetaTag::findOne(['mata_tag_id' => $id, 'lang' => 'zh']) : new LangMetaTag;
            $langZh->title = $form->title_zh;
            $langZh->description = $form->description_zh;
            $langZh->keywords = $form->keywords_zh;
            $langZh->mata_tag_id = $lastId;
            if (!$id) {
                $langZh->lang = 'zh';
            }
            $langZh->save();

            Yii::$app->getResponse()->redirect(Url::toRoute([Yii::$app->controller->id . '/edit', 'id' => $lastId]));
        }


        return $this->render('edit', [
            'edit' => $form,
            'model' => $tag,
        ]);
    }

}