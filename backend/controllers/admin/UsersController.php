<?php

namespace backend\controllers\admin;

use Yii;
use yii\web\ForbiddenHttpException;
use common\models\User;
use common\models\AuthAssignment;
use backend\models\forms\Role;
use yii\helpers\Html;
use yii\helpers\Url;

class UsersController extends AdminController {

    public function actionIndex() {
        $users = User::getAll();

        return $this->render('index', [
            'users' => $users
        ]);
    }

    public function actionEdit() {
        $id = Yii::$app->request->getQueryParam('id') ? Yii::$app->request->getQueryParam('id') : null;

        $model = new User();
        $model = $id ? $model->findOne($id) : $model;

        $form = new Role();
        $roles = Yii::$app->getAuthManager()->getRoles();
        $rollsArr = [];
        $rollName = '';
        
        foreach ($roles as $role) {
            $rollsArr[$role->name] = $role->name;
        }

        if (!empty($id)) {
            foreach (Yii::$app->authManager->getRolesByUser($id) as $role) {
                $rollName =  $role->name;
            }
        }

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {

            if (!empty($form->username)) {
                $model->username = trim(Html::encode($form->username));
            }
            if (!empty($form->email)) {
                $model->email = trim(Html::encode($form->email));
            }

            $model->status = isset(Yii::$app->request->post('Role')['active']) ? 10 : 0;
            $model->save();
            $id = $id ? $id : Yii::$app->db->lastInsertID;
            $user = AuthAssignment::findOne(['user_id' => $id]);
            if (!empty($user)) {
                AuthAssignment::deleteAll(['user_id' => $id]);
            }
            if ($userRole = Yii::$app->authManager->getRole($form->role)) {
                Yii::$app->authManager->assign($userRole, $id);
                if ($userRole->name == 'admin') {
                    $permit = Yii::$app->authManager->getPermission('AdminPanel');
                    Yii::$app->authManager->assign($permit, $id);
                }
            }

            Yii::$app->getResponse()->redirect(Url::toRoute([Yii::$app->controller->id . '/edit', 'id' => $id]));
        }


        return $this->render('edit', [
            'edit' => $form,
            'model' => $model,
            'roles' => $rollsArr,
            'rollName' => $rollName
        ]);
    }

}