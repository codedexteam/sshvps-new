<?php
namespace backend\controllers\admin;

use common\models\MenuType;

class AccountMenuController extends MainMenuController {

    public $menuId = MenuType::ACCOUNT_MENU_ID;
    public $controllerTitle = 'Личный кабинет';

}