<?php

namespace backend\controllers\admin;

use Yii;
use common\models\Server;
use common\models\BillingServer;
use yii\data\Pagination;
use yii\web\Response;
use common\models\forms\EditServerForm;
use yii\helpers\Url;
use console\controllers\ServerController;
use yii\web\HttpException;
use common\models\LangServer;

class ServersController extends AdminController {

    public function actionIndex() {
        $servers = (new Server)->getAllServers(['servers.is_deleted' => 0], false);
        $pagerServers = new Pagination(['totalCount' => $servers->count(), 'pageSize' => self::PAGE_SIZE]);
        $pagerServers->pageSizeParam = false;

        $allServers = $servers->offset($pagerServers->offset)
            ->limit($pagerServers->limit)
            ->all();

        $countNewServers = (new BillingServer)->countNewServers('billing_servers.is_deleted = 0 AND servers.billing_id is null');
        $newServ = (new BillingServer())->getNewServers('billing_servers.is_deleted = 0 AND servers.billing_id is null', false);

        $pagerNewServers = new Pagination(['totalCount' => $newServ->count(), 'pageSize' => self::PAGE_SIZE]);
        $pagerNewServers->pageSizeParam = false;

        $newServers = $newServ->offset($pagerNewServers->offset)
            ->limit($pagerNewServers->limit)
            ->all();

        return $this->render('index', [
            'allServers' => $allServers,
            'pagerServers' => $pagerServers,
            'pagerNewServers' => $pagerNewServers,
            'countNewServers' => $countNewServers,
            'newServers' => $newServers,
        ]);
    }

    public function actionEdit() {
        $id = Yii::$app->request->getQueryParam('id') ? Yii::$app->request->getQueryParam('id') : null;

        $form = new EditServerForm();

        $server = $id ? Server::findOne(['billing_id' => $id]) : [];
        $billingServer = $id ? BillingServer::findOne(['billing_id' => $id]) : [];
        $location = (new Server())->locate;
        array_unshift($location, 'Нет');

        if (!empty($server)) {
            if ($form->load(Yii::$app->request->post()) && $form->validate()) {
                $server->raid = isset(Yii::$app->request->post('EditServerForm')['raid']) ? 1 : 0;
                $server->ipmi = isset(Yii::$app->request->post('EditServerForm')['ipmi']) ? 1 : 0;
                $server->price = $form->price;
                $server->rack = $form->rack;
                $server->chassis_templ = $form->chassis_templ;
                $server->hostname = $form->hostname;
                $server->owner = $form->owner;
                $server->mac = $form->mac;
                $server->ip = $form->ip;
                $server->poweron = isset(Yii::$app->request->post('EditServerForm')['poweron']) ? 1 : 0;
                $server->os = $form->os;
                $server->location = $form->location ? $form->location : null;
                $server->active = isset(Yii::$app->request->post('EditServerForm')['active']) ? 1 : 0;
                $server->save();

                $langRu = LangServer::findOne(['server_id' => $server->id, 'lang' => 'ru']);
                if (!empty($langRu)) {
                    $langRu->name = $form->name_ru;
                    $langRu->cpuname = $form->cpuname_ru;
                    $langRu->ram = $form->ram_ru;
                    $langRu->hdd = $form->hdd_ru;
                    $langRu->localspeed = $form->localspeed_ru;
                    $langRu->update();
                } else {
                    $langRu = new LangServer();
                    $data = [[
                        'server_id' => $server->id,
                        'lang' => 'ru',
                        'name' => $form->name_ru,
                        'cpuname' => $form->cpuname_ru,
                        'ram' => $form->ram_ru,
                        'hdd' => $form->hdd_ru,
                        'localspeed' => $form->localspeed_ru
                    ]];
                    $langRu->insertData(LangServer::tableName(), $data, true);
                }

                $langEn = LangServer::findOne(['server_id' => $server->id, 'lang' => 'en']);
                if (!empty($langEn)) {
                    $langEn->name = $form->name_en;
                    $langEn->cpuname = $form->cpuname_en;
                    $langEn->ram = $form->ram_en;
                    $langEn->hdd = $form->hdd_en;
                    $langEn->localspeed = $form->localspeed_en;
                    $langEn->update();
                } else {
                    $langEn = new LangServer();
                    $data = [[
                        'server_id' => $server->id,
                        'lang' => 'en',
                        'name' => $form->name_en,
                        'cpuname' => $form->cpuname_en,
                        'ram' => $form->ram_en,
                        'hdd' => $form->hdd_en,
                        'localspeed' => $form->localspeed_en
                    ]];
                    $langEn->insertData(LangServer::tableName(), $data, true);
                }

                $langZh = LangServer::findOne(['server_id' => $server->id, 'lang' => 'zh']);
                if (!empty($langZh)) {
                    $langZh->name = $form->name_zh;
                    $langZh->cpuname = $form->cpuname_zh;
                    $langZh->ram = $form->ram_zh;
                    $langZh->hdd = $form->hdd_zh;
                    $langZh->localspeed = $form->localspeed_zh;
                    $langZh->update();
                } else {
                    $langZh = new LangServer();
                    $data = [[
                        'server_id' => $server->id,
                        'lang' => 'zh',
                        'name' => $form->name_zh,
                        'cpuname' => $form->cpuname_zh,
                        'ram' => $form->ram_zh,
                        'hdd' => $form->hdd_zh,
                        'localspeed' => $form->localspeed_zh
                    ]];
                    $langZh->insertData(LangServer::tableName(), $data, true);
                }

                Yii::$app->getResponse()->redirect(Url::toRoute([Yii::$app->controller->id . '/edit', 'id' => $id]));
            }

            return $this->render('edit', [
                'edit' => $form,
                'model' => $server,
                'billingServer' => $billingServer,
                'location' => $location
            ]);
        } else {
            throw new HttpException(404 ,'Такой страницы нет!');
        }
    }

    public function actionActive() {
        if (Yii::$app->request->isAjax) {
            $response = false;

            $id = (int)Yii::$app->request->post()['id'];
            $value = (int)Yii::$app->request->post()['value'];

            $server = Server::findOne($id);
            $server->active = $value;
            if ($server->update() !== false) {
                $response = true;
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'status' => $response,
            ];
        }
        Yii::$app->end();
    }

    public function actionApprove() {
        if (Yii::$app->request->isAjax) {
            $response = false;

            $id = (int)Yii::$app->request->post()['id'];
            $billingServer = BillingServer::findOne(['billing_id' => $id]);
            if (!empty($billingServer)) {
                $server = new Server();
                $langServer = new LangServer();
                $server->billing_id = $billingServer->billing_id;
                $server->name = $billingServer->name;
                $server->raid = $billingServer->raid;
                $server->ipmi = $billingServer->ipmi;
                $server->price = $billingServer->price;
                $server->rack = $billingServer->rack;
                $server->chassis_templ = $billingServer->chassis_templ;
                $server->hostname = $billingServer->hostname;
                $server->owner = $billingServer->owner;
                $server->mac = $billingServer->mac;
                $server->ip = $billingServer->ip;
                $server->poweron = $billingServer->poweron;
                $server->os = $billingServer->os;
                $server->save();

                $idLastItem = Yii::$app->db->lastInsertID;

                $langServer->server_id = $idLastItem;
                $langServer->lang = 'ru';
                $langServer->name = $billingServer->name;
                $langServer->cpuname = $billingServer->cpuname;
                $langServer->ram = $billingServer->ram;
                $langServer->hdd = $billingServer->hdd;
                $langServer->localspeed = $billingServer->localspeed;
                $langServer->save();
                $response = true;
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'status' => $response,
            ];
        }
        Yii::$app->end();
    }

    public function actionUpdateServers() {
        if (Yii::$app->request->isAjax) {
            $response = false;
            $response = (new ServerController($this->id, $this->module))->actionUpdate();
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'status' => $response,
            ];
        }
        Yii::$app->end();
    }

    public function actionDelete() {
        if (Yii::$app->request->isAjax) {
            $response = false;

            $id = (int)Yii::$app->request->post()['id'];

            $server = Server::findOne($id);
            $server->is_deleted = 1;
            $res = $server->save();

            if ($res > 0) {
                $response = true;
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'status' => $response,
            ];
        }
        Yii::$app->end();
    }
    
}