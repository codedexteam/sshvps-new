<?php

namespace backend\controllers\admin;

use common\models\Option;

class VpsOptionsController extends ServerOptionsController {

    public $controllerTitle = 'VPS серверы';
    public $serverType = Option::TYPE_VPS;

}