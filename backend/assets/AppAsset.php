<?php

namespace backend\assets;

use Yii;
use yii\web\AssetBundle;
use yii\web\View;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle {
    
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $jsOptions = ['position' => View::POS_HEAD];

    public $css = [
        'fonts/font-awesome/css/font-awesome.min.css',
        'fonts/material-design-iconic-font/css/material-design-iconic-font.min.css',
        'lib/jquery-ui-1.12.1.custom/jquery-ui.min.css',
        'css/admin.css?r3',
    ];
    public $js = [
        'lib/jquery-ui-1.12.1.custom/jquery-ui.min.js',
        'js/admin.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'yii\web\JqueryAsset'
    ];

    public function init() {
        Yii::setAlias('admin', $this->basePath . '/backend/web/');
        parent::init();
    }

}
