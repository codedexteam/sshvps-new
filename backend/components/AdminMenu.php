<?php
namespace backend\components;

use yii\base\Widget;
use yii\helpers\Html;

class AdminMenu extends Widget {

    public $menu = [
        [
            'name' => 'Страницы',
            'submenu' => [
                [
                    'name' => 'Мета-теги',
                    'controller' => 'tags'
                ],
            ]
        ],
        [
            'name' => 'Пользователи',
            'submenu' => [
                [
                    'name' => 'Список',
                    'controller' => 'users'
                ],
            ]
        ],
        [
            'name' => 'Серверы',
            'submenu' => [
                [
                    'name' => 'Выделенные серверы',
                    'controller' => 'servers'
                ],
                [
                    'name' => 'VPS тарифы',
                    'controller' => 'vps-tariffs'
                ],
            ]
        ],
        [
            'name' => 'Меню',
            'submenu' => [
                [
                    'name' => 'Главное меню',
                    'controller' => 'main-menu'
                ],
                [
                    'name' => 'Футер',
                    'controller' => 'footer-menu'
                ],
                [
                    'name' => 'Личный кабинет',
                    'controller' => 'account-menu'
                ],
            ]
        ],
        [
            'name' => 'Услуги',
            'submenu' => [
                [
                    'name' => 'Выделенные серверы',
                    'controller' => 'server-options'
                ],
                [
                    'name' => 'VPS',
                    'controller' => 'vps-options'
                ],
            ]
        ],
    ];

    public function run() {
        $mainLi = '';
        $subLi = '';
        foreach ($this->menu as $items) {
            $subLi = '';
            foreach ($items as $key => $item) {
                if (is_array($item)) {
                    foreach ($item as $val) {
                        $subSpan = Html::tag('span', $val['name']);
                        $a = Html::tag('a', $subSpan, ['href' => '/admin/' . $val['controller']]);
                        $subLi .= Html::tag('li', $a);
                    }
                }
            }
            $subUl = Html::tag('ul', $subLi, ['class' => 'dropdown-menu', 'role' => 'menu']);
            $mainSpan = Html::tag('span', '', ['class' => 'caret']);
            $mainA = Html::tag('a', $items['name'] . $mainSpan, [
                'href' => '#',
                'class' => 'dropdown-toggle',
                'data-toggle' => 'dropdown',
                'role' => 'button',
                'aria-expanded' => 'false'
            ]);
            $mainLi .= Html::tag('li', $mainA . $subUl, ['class' => 'dropdown']);
            }
        return $mainLi;
    }

}