<?php

namespace backend\models\forms;

use Yii;
use yii\base\Model;

class Role extends Model {

    public $username;
    public $password;
    public $role;
    public $email;
    public $active;

    public function rules() {
        return [
            ['role', 'required'],
            ['email', 'email'],
        ];
    }

    public function attributeLabels() {
        return [
            'username' => 'Логин',
            'password' => 'Пароль',
            'role' => 'Роль',
            'email' => 'Email',
            'active' => 'Активность'
        ];
    }

}
