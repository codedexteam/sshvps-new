<?php

namespace backend\models\forms;

use Yii;
use yii\base\Model;
use common\models\MetaTag;

class Tags extends Model {

    public $page_description;
    public $title_ru;
    public $title_en;
    public $title_zh;
    public $description_ru;
    public $description_en;
    public $description_zh;
    public $keywords_ru;
    public $keywords_en;
    public $keywords_zh;
    public $page_key;
    public $old_attribute_val = '';

    public function rules() {
        return [
            [['page_description', 'title_ru', 'title_en', 'title_zh', 'description_ru',
                'description_en', 'description_zh', 'keywords_ru', 'keywords_en', 'keywords_zh', 'page_key'], 'string', 'max' => 255],
            [['page_description', 'title_ru', 'title_en', 'title_zh', 'description_ru',
                'description_en', 'description_zh', 'keywords_ru', 'keywords_en', 'keywords_zh', 'page_key'], 'filter', 'filter' => 'trim'],
            [['title_ru', 'description_ru', 'keywords_ru'], 'required'],
            ['page_key', 'unique', 'targetClass' => MetaTag::className(), 'message' =>  'Такой ключ уже есть', 'when' => function ($form, $attribute) {
                return $this->$attribute !== $this->old_attribute_val;
            },],
        ];
    }

    public function setOldAttribute($value) {
        return $this->old_attribute_val = $value;
    }

    public function attributeLabels() {
        return [
            'page_description' => 'Описание',
            'title_ru' => 'Title',
            'title_en' => 'Title на английском',
            'title_zh' => 'Title на китайском',
            'description_ru' => 'Description',
            'description_en' => 'Description на английском',
            'description_zh' => 'Description на китайском',
            'keywords_ru' => 'Keywords',
            'keywords_en' => 'Keywords на английском',
            'keywords_zh' => 'Keywords на китайском',
            'page_key' => 'Ключ (только для разработчика)'
        ];
    }

}