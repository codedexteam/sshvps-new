<?php

namespace backend\models\forms;

use Yii;
use yii\base\Model;


class Option extends Model {

    public $title_ru;
    public $title_en;
    public $title_zh;
    public $description_ru;
    public $description_en;
    public $description_zh;
    public $min;
    public $max;
    public $is_checked;
    public $icon;
    public $price;
    public $active;

    public function rules() {
        return [
            [['title_ru'], 'required'],
            [['min', 'max'], 'required'],
            [['title_ru', 'title_en', 'title_zh'], 'string', 'max' => 255],
            [['description_ru', 'description_en', 'description_zh'], 'string'],
            [['description_ru', 'description_en', 'description_zh', 'title_ru', 'title_en', 'title_zh', 'icon'], 'filter', 'filter' => 'trim'],
            ['icon', 'string', 'max' => 100],
            [['min', 'max'], 'number'],
            ['max', 'compare', 'compareValue' => 0, 'operator' => '>'],
            ['price', 'double']
        ];
    }

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['slide'] = ['title_ru', 'title_en', 'title_zh', 'min', 'max', 'icon', 'price', 'description_ru', 'description_en', 'description_zh', 'active'];
        $scenarios['list'] = ['title_ru', 'title_en', 'title_zh', 'icon', 'price', 'description_ru', 'description_en', 'description_zh', 'active'];
        $scenarios['option'] = ['title_ru', 'title_en', 'title_zh', 'price', 'is_checked', 'active'];
        return $scenarios;
    }

    public function attributeLabels() {
        return [
            'title_ru' => 'Название',
            'title_en' => 'Название на английском',
            'title_zh' => 'Название на китайском',
            'description_ru' => 'Описание',
            'description_en' => 'Описание на английском',
            'description_zh' => 'Описание на китайском',
            'min' => 'Минимальное значение',
            'max' => 'Максимальное значение',
            'is_checked' => 'Пункт по умолчанию',
            'icon' => 'Иконка',
            'price' => 'Цена в долларах',
            'active' => 'Активность'
        ];
    }

}