var sumVal = $('*').is('#sum') ? parseInt($('#sum').text()) : 0;

function sumSlide() {
    var coreVal = parseInt($('#core').val()) > 1 ? parseInt($('#core').val()) * 10 : 0;
    var ramVal = parseInt($('#ram').val()) > 2 ? parseInt($('#ram').val()) * 2 : 0;
    var hardVal = parseInt($('#hard').val()) > 20 ? parseInt($('#hard').val()) * 5 : 0;
    var ddosVal = parseInt($('#ddos').val()) * 10;
    var sum = sumVal + coreVal + ramVal + hardVal + ddosVal;
    $( "#sum" ).text(sum);
}
// Edit texts
function editTexts(elem, url) {
    if (elem.hasClass('edit-text')) {
        elem.removeClass('edit-text').addClass('save-text').text('Сохранить').parents('.wrapper').addClass('edit');
        $('*[data-name]').each(function () {
            var inside = '<textarea>' + $(this).html() + '</textarea>';
            $(this).html(inside);
        });
    } else {
        //Сохранить тексы
        if (elem.hasClass('save-text')) {
            var $this = elem;
            var values = {};
            var i = 0;
            $('*[data-name]').each(function () {
                var inside = $(this).children('textarea').val();
                $(this).html(inside);
                values[i] = {
                    id: $(this).data('id'),
                    name: $(this).data('name'),
                    content: $(this).html()
                };
                i++;
            });
            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: {
                    values: values,
                    _csrf: yii.getCsrfToken()
                },
                success: function (response) {
                    if (response >= 0) {
                        $this.removeClass('save-text').addClass('edit-text').text('Редактировать').parents('.wrapper').removeClass('edit');
                    }
                },
                error: function () {
                }
            });
        }
    }
}


////account
//height of menu
function accountMenu() {
    var menu = $('.account .menu');
    var footer = $('#footer');
    var menuOffset = menu.offset().top;
    var footerHeight = footer.innerHeight();
    var footerBorder = footer.offset().top - footerHeight;
    var scroll = $(window).scrollTop();

    if ((scroll >= menuOffset) && (scroll < footerBorder)) {
        menu.addClass('fixed');
    } else {
        menu.removeClass('fixed');
    }

    if (scroll >= footerBorder) {
        menu.addClass('absolute');
    } else {
        menu.removeClass('absolute');
    }

    $(document).scroll(function () {
        scroll = $(window).scrollTop();

        if ((scroll >= menuOffset) && (scroll < footerBorder)) {
            menu.addClass('fixed');
        } else {
            menu.removeClass('fixed');
        }
        if (scroll >= footerBorder) {
            menu.addClass('absolute');
        } else {
            menu.removeClass('absolute');
        }
    });
}

$(document).ready(function () {
    //Конфиги выделенного сервера
    //Радио кнопки
    //Процессор
    $('.processor .name .mybtn').click(function () {
        $('.processor .name input').prop("checked", false);
        $('.processor .name .false-radio').removeClass('active');
        $(this).find('input').prop("checked", true);
        $(this).find('.false-radio').addClass('active');

        var radioId = $(this).find('input').attr('id');
        if ($('.processor .sub-name .items').length > 0) {
            $('.processor .sub-name .items').removeClass('active');
            $('.processor .sub-name .items.' + radioId).addClass('active');
        }
    });
    $('.processor .sub-name .items .mybtn').click(function () {
        $('.processor .sub-name input').prop("checked", false);
        $('.processor .sub-name .items .false-radio').removeClass('active');
        $(this).find('.false-radio').addClass('active');
        $(this).find('input').prop("checked", true);

        var value = $(this).find('label').html();
        $('.result .processor .value').html(value);
    });

    //ОЗУ
    $('.ram .radio-btn').click(function () {
        var ramImage = $('.result .ram figure');
        var clone = ramImage.parent().find('figure').html();

        $('.ram input').prop("checked", false);
        $('.ram .false-radio').removeClass('active');
        $(this).find('.false-radio').addClass('active');
        $(this).find('input').prop("checked", true);

        if (ramImage.length > 1) {
            ramImage.eq(1).remove();
            ramImage.eq(2).remove();
        }

        var id = $(this).find('input').attr('id');
        if (id == 'ram16') {
            $('.result .ram').append('<figure>' + clone + '</figure>');
        }
        if (id == 'ram32') {
            $('.result .ram').append('<figure>' + clone + '</figure><figure>' + clone + '</figure>');
        }
        var value = $(this).parent('.radio-btn').find('label').html();
        $('.result .ram .value').html(value);
    });

    //Аппаратный контроллер
    $('.control .radio-btn').click(function () {
        $('.control .false-radio').removeClass('active');
        $('.control input').prop("checked", false);
        $(this).find('.false-radio').addClass('active');
        $(this).find('input').prop("checked", true);
    });

    //Блок с ценой на главной
    $('.price-block .abonent > div').click(function () {
        $('.price-block .abonent .false-radio').removeClass('active');
        $('.price-block .abonent input').prop("checked", false);
        $(this).find('.false-radio').addClass('active');
        $(this).find('input').prop("checked", true);
    });

    //Чекбоксы
    $('.hdd .check-btn').click(function () {
        if ($(this).find('input').prop( "checked") == false) {
            $(this).find('input').prop("checked", true);
        } else {
            $(this).find('input').prop("checked", false);
        }

        if ($(this).find('input').prop("checked") == true) {
            $(this).find('.false-checkbox').addClass('active');
            $(this).parents('.check-block').find('select').prop('disabled', false);
        } else {
            $(this).find('.false-checkbox').removeClass('active');
            $(this).parents('.check-block').find('select').prop('disabled', true);
            $(this).parents('.check-block').find('select option').attr('selected', false);
            $(this).parents('.check-block').find('select option').eq(0).attr('selected', 'selected');
            $('.result').find('.hdd2').remove();
            $('.result .value').each(function () {
                if ($(this).html() == '') {
                    $(this).html('Не выбрано');
                }
            });
        }
    });

    //select
    $('.hdd select').change(function () {
        var hdd = $('.hdd select[name="diskNameHdd1"]');
        var ssd = $('.hdd select[name="diskNameHdd2"]');
        var typeHdd = hdd.find('option:selected').data('type');
        var typeSsd = ssd.find('option:selected').data('type');
        var valueHdd = hdd.find('option:selected').html();
        var valueSsd = ssd.find('option:selected').html();
        var resultHdd = $('.result .hdd .value');
        var resultSsd = $('.result .ssd .value');
        var hddImage = $('.result .hdd figure');
        var ssdImage = $('.result .ssd figure');
        var value, clone;
        resultHdd.html('Не выбрано');
        resultSsd.html('Не выбрано');

        if (hddImage.length > 1) {
            $('.result .hdd figure:last-of-type').remove();
        }
        if (ssdImage.length > 1) {
            $('.result .ssd figure:last-of-type').remove();
        }

        if (typeHdd == 'sata' && typeSsd == 'none') {
            resultHdd.html('<span class="hdd1">1 x ' + valueHdd + '</span>');
        }
        if (typeHdd == 'none' && typeSsd == 'sata') {
            resultHdd.html('<span class="hdd1">1 x ' + valueSsd + '</span>');
        }
        if (typeHdd == 'ssd' && typeSsd == 'none') {
            resultSsd.html('<span class="hdd2">1 x ' + valueHdd + '</span>');
        }
        if (typeHdd == 'none' && typeSsd == 'ssd') {
            resultSsd.html('<span class="hdd2">1 x ' + valueSsd + '</span>');
        }
        if (typeHdd == 'sata' && typeSsd == 'ssd') {
            resultHdd.html('<span class="hdd1">1 x ' + valueHdd + '</span>');
            resultSsd.html('<span class="hdd2">1 x ' + valueSsd + '</span>');
        }
        if (typeSsd == 'sata' && typeHdd == 'ssd') {
            resultHdd.html('<span class="hdd2">1 x ' + valueSsd + '</span>');
            resultSsd.html('<span class="hdd1">1 x ' + valueHdd + '</span>');
        }
        if (typeHdd == 'sata' && typeSsd == 'sata') {
            value = '<span class="hdd1">1 x ' + valueHdd + '</span>' + '<span class="hdd2">1 x ' + valueSsd + '</span>';
            resultHdd.html(value);
            clone = hddImage.eq(0).clone();
            $('.result .hdd').append(clone);
        }
        if (typeHdd == 'ssd' && typeSsd == 'ssd') {
            value = '<span class="hdd1">1 x ' + valueHdd + '</span>' + '<span class="hdd2">1 x ' + valueSsd + '</span>';
            resultSsd.html(value);
            clone = ssdImage.eq(0).clone();
            $('.result .ssd').append(clone);
        }
    });

    //Конфиги vps
    $( '#vps .slide .ddos-item .mybtn').click(function () {
        if ($( "#ddos" ).val() == 0) {
            $(this).animate({
                left: 34
            }, 100);
            $( "#ddos" ).val(1);
            $('#vps .ddos-block span').removeClass('active');
            $('#vps .ddos-block span:last-of-type').addClass('active');
        } else {
            $(this).animate({
                left: 0
            }, 100);
            $( "#ddos" ).val(0);
            $('#vps .ddos-block span').removeClass('active');
            $('#vps .ddos-block span:first-of-type').addClass('active');
        }
        sumSlide();
    });

    //Tabs
    $('#vps .tab div').click(function () {
        $('#vps .tab div').removeClass('active');
        $(this).addClass('active');
        if ($(this).hasClass('server')) {
            $('#vps .block .blocks > div').removeClass('active');
            $('#vps .block .blocks .package').addClass('active');
        }
        if ($(this).hasClass('config')) {
            $('#vps .block .blocks > div').removeClass('active');
            $('#vps .block .blocks .slide').addClass('active');
        }
    });

    //Конфиги выделенного сервера
    $('#other-servers .tab div').click(function () {
        var tab = $('#other-servers .tab div');
        tab.removeClass('active');
        $(this).addClass('active');

        if ($(this).hasClass('server')) {
            $('#other-servers .block .blocks > div').removeClass('active');
            $('#other-servers .block .blocks .server').addClass('active');
        }
        if ($(this).hasClass('config')) {
            $('#other-servers .block .blocks > div').removeClass('active');
            $('#other-servers .block .blocks .config').addClass('active');
        }
    });

    $('.wrapper').on('click', 'a', function (e) {
        if (e.target.nodeName == 'textarea'.toUpperCase()) {
            e.preventDefault();
        }
    });
    $('.wrapper').on('click', 'button', function (e) {
        if (e.target.nodeName == 'textarea'.toUpperCase()) {
            e.preventDefault();
        }
    });

    //Switch language
    $(document).click(function(e){
        var elem = $('.switch-lang');
        if (e.target != elem[0] && !elem.has(e.target).length) {
            elem.find('ul').hide();
        }
    });
    $('.switch-lang').click(function () {
        $(this).find('ul').toggle();
    });

    //Registration form
    //Enter form
    $('.signup input, .enter-form input, .order .account-form input, .order-enter .signup-form input, .account.edit input, .account.edit .textarea textarea').focus(function () {
        $(this).parents('.input').find('i').removeClass('active');
        $(this).prev().addClass('active');
        $(this).next().addClass('active');
    }).focusout(function () {
        $(this).parents('.input').find('i').removeClass('active');
    });
    $('.signup .input i:nth-of-type(2), .enter-form .input i:nth-of-type(2), .order .account-form .input i:nth-of-type(2), .order-enter .signup-form .input i:nth-of-type(2)').click(function () {
        if ($(this).hasClass('fa-eye-slash')) {
            $(this).removeClass('fa-eye-slash');
            $(this).addClass('fa-eye');
            $(this).parents('.input').find('input[type=password]').attr('type', 'text');
        } else {
            if ($(this).hasClass('fa-eye')) {
                $(this).removeClass('fa-eye');
                $(this).addClass('fa-eye-slash');
                $(this).parents('.input').find('input[type=text]').attr('type', 'password');
            }
        }
    });
    $('.signup .checkbox, .order-enter .signup-form .checkbox').click(function () {
        if ($(this).prop("checked") == true) {
            $(this).parent('.check').addClass('active');
            $('.button').prop('disabled', false);
        } else {
            $(this).parent('.check').removeClass('active');
            $('.button').prop('disabled', true);
        }
    });

    //Enter form submit
    $(document).click(function(e){
        if (e.target.id != 'enter-account') {
            var elem = $('.enter-form .form');
            if (e.target != elem[0] && !elem.has(e.target).length) {
                $('.enter-form').hide();
            }
        }
    });
    $('#enter-account').click(function () {
        $('.enter-form').show();
    });


    //order page
    //select
    $('.order .select-box, .account .action-wrap .select-box').click(function () {
        var selectBox = $('.select-box');
        $(this).find('ul').toggle();
        selectBox.not(this).find('ul').hide();
        if ($(this).find('.checked').hasClass('active')) {
            $(this).find('.checked').removeClass('active');
        } else {
            $(this).find('.checked').addClass('active');
        }
    });
    //choose
    $('.order .select-box li').click(function () {
        var textInput = $(this).find('span').eq(0).text();
        var idInput = $(this).data('id');
        $(this).parents('.select-box').find('.checked').text(textInput);
        $(this).parents('.select-box').find('input').val(idInput);
    });
    //choose option li
    $('.order .option .select-box li').click(function () {
        var option = $(this).parents('.option').data('option');
        var textInput = $(this).find('span').eq(0).text().substr(0, 30);
        $('.config .params span.' + option).text(textInput);
    });

    //choose option radio
    $('.order .option .radio-btn').click(function () {
        var option = $(this).parents('.option').data('option');
        var span = $(this).find('label').find('span').html();
        var textInput = $(this).find('label').text().replace(span, '').substr(0, 30);
        $('.config .params span.' + option).text(textInput);
    });

    //radio
    $('.order .radio-btn').click(function () {
        $(this).parents('.radio-block').parent().find('input[type=radio]').prop("checked", false);
        $(this).parents('.radio-block').parent().find('.false-radio').removeClass('active');
        $(this).find('input[type=radio]').prop("checked", true);
        $(this).find('.false-radio').addClass('active');
    });
    //decription
    $('.fa-question-circle-o').click(function () {
        $(this).next('.description').toggle();
    });
    
    
    //enter
    $('.order #order-signup').click(function () {
        $('.order.order-enter .account-form').removeClass('active');
        $('.order.order-enter .signup-form').addClass('active');
    });

    //pay-page
    $('.order .donation .var').click(function () {
        $('.order .donation .var').removeClass('active');
        $(this).addClass('active');
    });
    $('.order .donation .donate-perc, .order .donation .donate-perc .ui-slider-handle').focus(function () {
        $('.order .donation .var').removeClass('active');
        $(this).parents('.var').addClass('active');
    });


    //account
    //description
    $('*[data-hint]').mouseover(function () {
        var hint = $('.account > .description');
        var elemText = $(this).data('hint');
        var headerHeight = $('#header').innerHeight();
        var elemTop = $(this).offset().top - headerHeight;
        var elemLeft = $(this).offset().left;

        hint.text(elemText);
        var hintHeight = hint.innerHeight() + 15;
        var hintWidth = hint.width() - 2;
        hint.css('display', 'inline-block');

        hint.css({
            top: elemTop - hintHeight,
            left: elemLeft - hintWidth
        });
    }).mouseout(function () {
        $('.account > .description').hide();
    });

    //table
    $('.account.servers tr:not(.character)').click(function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).next('.character').find('td > div').stop().slideUp(500);
        } else {
            $('.account.servers tr').removeClass('active');
            $('.account.servers tr.character').find('td > div').stop().slideUp(500);
            $(this).addClass('active');
            $(this).next('.character').find('td > div').stop().slideDown(500);
        }
    });
    $('.account tr.character .slide-up').click(function () {
        $(this).parents('.character').find('td > div').stop().slideUp(500);
        $('.account.servers tr').removeClass('active');
    });
    $('.account table').colResizable({liveDrag:true});


    //confirm windows
    $('.account .action-wrap').click(function (e) {
        $(this).removeClass('active');
    }).children()
        .click(function(e){
            e.stopPropagation();
        });

    $('.account .actions').on('click' ,'.stop', function () {
        var serverId = $(this).parents('.character').data('id');

        $('.account .action-wrap.act-stop').data('id', serverId).addClass('active');
        $('.account .actions').on('click' ,'.play', function (e) {
            e.preventDefault();
        });
    });
    $('.account .actions').on('click' ,'.play', function () {
        var serverId = $(this).parents('.character').data('id');

        $('.account .action-wrap.act-play').data('id', serverId).addClass('active');
        $('.account .actions').on('click' ,'.stop', function (e) {
            e.preventDefault();
        });
    });

    $('.account .action-wrap.act-stop .buttons .yes').on('click', function () {
        var serverId = $(this).parents('.action-wrap').data('id');
        $('.account .action-wrap').removeClass('active');
        $('.account tr.character[data-id=' + serverId + ']').find('.act.stop').addClass('play').removeClass('stop');
    });

    $('.account .action-wrap.act-play .buttons .yes').on('click', function () {
        var serverId = $(this).parents('.action-wrap').data('id');
        $('.account .action-wrap').removeClass('active');
        $('.account tr.character[data-id=' + serverId + ']').find('.act.play').addClass('stop').removeClass('play');
    });
    $('.account .actions .act.reload').click(function () {
        $('.account .action-wrap.act-refresh').addClass('active');
    });
    $('.account .actions .act.reinstall').click(function () {
        $('.account .action-wrap.act-reinstall').addClass('active');
    });
    $('.account .action-wrap .select-box li').click(function () {
        var textInput = $(this).find('span').eq(0).text();
        var idInput = $(this).data('id');
        $(this).parents('.select-box').find('.checked').text(textInput);
        $(this).parents('.select-box').find('input').val(idInput);
    });
    $('.account .actions .act.delete').click(function () {
        $('.account .action-wrap.act-delete').addClass('active');
    });
});