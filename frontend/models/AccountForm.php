<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;
use yii\helpers\Url;


class AccountForm extends Model {

    public $email;
    public $password;
    private $_user;
    
    public function rules() {
        return [
            [['email', 'password'], 'required'],
            ['email', 'validateIsUserActivate', 'skipOnEmpty' => false, 'skipOnError' => false],
            ['email', 'email'],
            ['password', 'validatePassword']
        ];
    }

    protected function getUser() {
        if ($this->_user === null) {
            $this->_user = User::findByEmail($this->email);
        }

        return $this->_user;
    }

    public function login() {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), 0);
        } else {
            return false;
        }
    }

    public function validatePassword($attribute, $params) {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, Yii::t('app', 'pass_email_er'));
            }
        }
    }
    
    public function validateIsUserActivate($attribute, $params) {
        if (!$this->hasErrors()) {
            $user = User::findOne(['email' => $this->email]);
            if (!empty($user)) {
                $activate = User::findOne(['email' => $this->email, 'status' => 0]);
                if (!empty($activate)) {
                    $session = Yii::$app->session;
                    $session->set('reg', true);
                    $session->set('enter_form', true);
                    $session->set('repeat_id', $activate->id);
                    $link = '/signup/confirm';
                    $this->addError('not_active', Yii::t('app', 'confirm_email', ['link' => $link]));
                }
            }
        }
    }

    public function attributeLabels() {
        return [
            'email' => Yii::t('app', 'email'),
            'password' => Yii::t('app', 'password'),
        ];
    }

}