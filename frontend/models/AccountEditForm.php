<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

class AccountEditForm extends Model {

    public $name;
    public $lastname;
    public $phone;
    public $country;
    public $address;

    public function rules() {
        return [
            [['name', 'lastname'], 'required'],
            [['phone'], 'is8NumbersOnly'],
            [['country'], 'string', 'max' => 100],
            [['name'], 'string', 'min' => 2, 'max' => 255],
            [['lastname', 'address'], 'string', 'max' => 255],
            [['name', 'lastname', 'phone', 'country', 'address'], 'trim'],
        ];
    }

    public function is8NumbersOnly($attribute) {
        if (!preg_match('/^(\+[1-9][0-9]*(\([0-9]*\)|-[0-9]*-))?[0]?[1-9][0-9\- ]*$/', $this->$attribute)) {
            $this->addError($attribute, Yii::t('app', 'phone_format_er'));
        }
    }

    public function attributeLabels() {
        return [
            'name' => Yii::t('app', 'name'),
            'lastname' => Yii::t('app', 'lastname'),
            'country' => Yii::t('app', 'country'),
            'address' => Yii::t('app', 'address'),
        ];
    }

}