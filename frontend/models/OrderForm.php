<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

class OrderForm extends Model {

    public $server_id;
    public $os;
    public $panel;
    public $administration;
    public $traffic;
    public $ip;
    public $ip2;
    public $domen;
    public $period;

    public function rules() {
        return [
            [['domen', 'os', 'panel', 'administration', 'traffic', 'period'], 'string'],
            [['server_id', 'ip', 'ip2'], 'number']
        ];
    }

    public function attributeLabels() {
        return [
            'domen' => 'Домен',
        ];
    }

}