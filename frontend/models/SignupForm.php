<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;

class SignupForm extends Model {

    public $name;
    public $lastname;
    public $email;
    public $password;
    public $password_repeat;
    public $phone;
    public $country;
    public $address;
    public $agree;

    public function rules() {
        return [
            [['name', 'lastname', 'email', 'phone', 'country', 'address'], 'trim'],
            [['name', 'lastname', 'email', 'password', 'password_repeat'], 'required'],
            [['agree'], 'required', 'message' => ''],
            [['name'], 'string', 'min' => 2, 'max' => 255],
            [['password', 'password_repeat'], 'string', 'min' => 6, 'max' => 255],
            ['password', 'compare', 'compareAttribute' => 'password_repeat', 'message' => Yii::t('app', 'reg_pass_compare_er')],
            ['email', 'email'],
            [['email', 'lastname', 'address'], 'string', 'max' => 255],
            [['phone'], 'is8NumbersOnly'],
            [['country'], 'string', 'max' => 100],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => Yii::t('app', 'email_exist')],
        ];
    }

    public function attributeLabels(){
        return [
            'name' => Yii::t('app', 'name'),
            'lastname' => Yii::t('app', 'lastname'),
            'email' => Yii::t('app', 'email'),
            'password' => Yii::t('app', 'password'),
            'password_repeat' => Yii::t('app', 'password_repeat'),
            'phone' => Yii::t('app', 'phone'),
            'country' => Yii::t('app', 'country'),
            'address' => Yii::t('app', 'address'),
        ];
    }

    public function is8NumbersOnly($attribute) {
        if (!preg_match('/^(\+[1-9][0-9]*(\([0-9]*\)|-[0-9]*-))?[0]?[1-9][0-9\- ]*$/', $this->$attribute)) {
            $this->addError($attribute, Yii::t('app', 'phone_format_er'));
        }
    }

}
