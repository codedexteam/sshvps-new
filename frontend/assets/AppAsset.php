<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/fonts.css',
        'fonts/font-awesome/css/font-awesome.min.css',
        'fonts/material-design-iconic-font/css/material-design-iconic-font.min.css',
        'fonts/material-design-iconic-font/css/materialdesignicons.min.css',
        'lib/hibiki-slider-1.2/hibiki.slider.css',
        'lib/jquery-ui-1.12.0/jquery-ui.min.css',
        'css/style.css?r16',
    ];
    public $js = [
        'lib/accounting.min.js',
        'lib/jquery-ui-1.12.0/jquery-ui.min.js',
        'lib/hibiki-slider-1.2/hibiki.slider.js',
        'js/script.js?r1'
    ];
    public $depends = [
        'frontend\assets\OtherAsset',
    ];
}
