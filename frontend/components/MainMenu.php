<?php

namespace frontend\components;

use yii\base\Widget;
use common\models\MenuItem;
use common\models\MenuType;


class MainMenu extends Widget {

    public $menuId = MenuType::MAIN_MENU_ID;
    public $menuTemplate = 'main_menu'; 

    public function run() {
        $items = (new MenuItem())->getAllItems([
            MenuItem::tableName(). '.type_id' => $this->menuId,
            MenuItem::tableName(). '.active' => 1],
            true, [
            MenuItem::tableName() . '.sort' => SORT_ASC,
            MenuItem::tableName() . '.id' => SORT_DESC,
        ], true, true);

        return $this->render($this->menuTemplate, [
            'items' => $items
        ]);
    }

}