<?php

namespace frontend\components;

use yii\base\Widget;

class SliderTop extends Widget {

    public function run() {
        return $this->render('slider_top');
    }

}