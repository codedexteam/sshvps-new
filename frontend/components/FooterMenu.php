<?php

namespace frontend\components;

use common\models\MenuType;


class FooterMenu extends MainMenu {

    public $menuId = MenuType::FOOTER_ID;
    public $menuTemplate = 'footer_menu';

}