<?php

namespace frontend\components;

use yii\base\Widget;

class SliderOpinion extends Widget {

    public function run() {
        return $this->render('slider_opinion');
    }

}