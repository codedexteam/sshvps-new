<?php

namespace frontend\components;

use yii\base\Widget;

class SliderServer extends Widget {

    public function run() {
        return $this->render('slider_server');
    }

}