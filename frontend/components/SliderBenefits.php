<?php

namespace frontend\components;

use yii\base\Widget;

class SliderBenefits extends Widget {

    public function run() {
        return $this->render('slider_benefits');
    }

}