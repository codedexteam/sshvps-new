<?php

namespace frontend\components;

use Yii;
use yii\base\Widget;

class LangSwitch extends Widget {

    public function run() {
        $languages = [
            [
                'key' => 'ru',
                'title' => 'Русский'
            ],
            [
                'key' => 'en',
                'title' => 'English'
            ],
            [
                'key' => 'zh',
                'title' => 'Chinese'
            ]
        ];

        $controller = Yii::$app->controller->route != 'site/index' ? Yii::$app->controller->route : '/';

        return $this->render('lang_switch', [
            'languages' => $languages,
            'controller' => $controller
        ]);
    }

}