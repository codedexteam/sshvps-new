<?php if (!empty($items)) { ?>
    <?php foreach ($items as $item) { ?>
        <?php if (!empty($item->itemLang)) {?>
        <?php if ($item->parent_id == null) { ?>
            <div class="items">
                <div><?php echo $item->itemLang[0]['title']; ?></div>
                    <?php if (!empty($item->childItems)) { ?>
                        <ul><?php foreach ($item->childItems as $child) {?>
                                <li><a href="<?php echo $child->link; ?>"><?php echo $child->itemLang[0]['title']; ?></a></li>
                            <?php } ?></ul>
                    <?php } ?>
            </div>
        <?php } ?>
    <?php } ?>
    <?php } ?>
<?php } ?>