<?php
use yii\helpers\Url;
?>
<nav class="menu">
    <ul>
        <?php foreach ($items as $item) { ?>

            <?php if (!empty($item->itemLang)) {?>
                <?php if ($item->parent_id == null) { ?>
                    <li <?php echo $route == ('/' . Yii::$app->language . $item->link) ? 'class="active"' : ''; ?>><a href="<?php echo Url::toRoute([$item->link]); ?>"><i class="<?php echo $item->icon;?>"></i><?php echo $item->itemLang[0]['title']; ?></a>
                        <?php if (!empty($item->childItems)) { ?>
                            <ul><?php foreach ($item->childItems as $child) {?>
                                    <li <?php echo $route == ('/ru' . $child->link) ? 'class="active"' : ''; ?>><a href="<?php echo Url::toRoute([$child->link]); ?>"><?php echo $child->itemLang[0]['title']; ?></a></li>
                                <?php } ?></ul>
                        <?php } ?>
                    </li>
                <?php } ?>
            <?php } ?>
        <?php } ?>
    </ul>
</nav>