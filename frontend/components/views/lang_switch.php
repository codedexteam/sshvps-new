<?php

use yii\helpers\Url;

?>
<div class="switch-lang">
    <span class="<?php echo Yii::$app->language; ?> flag-lang"><?php echo Yii::$app->language; ?></span>
    <ul><?php foreach ($languages as $lang) { ?>
            <?php if (Yii::$app->language == $lang['key']) {?>
                <li>
                    <a class="<?php echo $lang['key']; ?> flag-lang" href="<?php echo Url::to([$controller, 'language' => $lang['key']]); ?>">
                        <div><?php echo $lang['title']; ?></div>
                    </a>
                </li>
            <?php continue;} ?>
        <?php } ?>
        <?php foreach ($languages as $lang) { ?>
            <?php if (Yii::$app->language != $lang['key']) {?>
                <li>
                    <a class="<?php echo $lang['key']; ?> flag-lang" href="<?php echo Url::to([$controller, 'language' => $lang['key']]); ?>">
                        <div><?php echo $lang['title']; ?></div>
                    </a>
                </li>
                <?php continue;} ?>
        <?php } ?>
    </ul>
</div>