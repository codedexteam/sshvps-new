<?php
use yii\helpers\Url;

if (!empty($items)) { ?>
<nav>
    <ul>
        <?php foreach ($items as $item) { ?>

            <?php if (!empty($item->itemLang)) {?>
                <?php if ($item->parent_id == null) { ?>
                    <li><a href="<?php echo Url::toRoute([$item->link]); ?>"><?php echo $item->itemLang[0]['title']; ?></a>
                        <?php if (!empty($item->childItems)) { ?>
                            <ul><?php foreach ($item->childItems as $child) {?>
                                <li><a href="<?php echo Url::toRoute([$child->link]); ?>"><?php echo $child->itemLang[0]['title']; ?></a></li>
                            <?php } ?></ul>
                        <?php } ?>
                    </li>
                <?php } ?>
            <?php } ?>
        <?php } ?>
    </ul>
</nav>
<?php } ?>