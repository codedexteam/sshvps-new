<?php

namespace frontend\components;

use common\models\MenuItem;
use common\models\MenuType;
use yii\base\Widget;
use Yii;


class AccountMenu extends Widget {

    public $menuId = MenuType::ACCOUNT_MENU_ID;
    public $menuTemplate = 'account_menu';

    public function run() {
        $items = (new MenuItem())->getAllItems([
            MenuItem::tableName(). '.type_id' => $this->menuId,
            MenuItem::tableName(). '.active' => 1],
            true, [
                MenuItem::tableName() . '.sort' => SORT_ASC,
                MenuItem::tableName() . '.id' => SORT_DESC,
            ], true, true);

        $route = Yii::$app->request->url;

        return $this->render('account_menu', [
            'items' => $items,
            'route' => $route,
        ]);
    }

}