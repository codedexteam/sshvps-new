<div class="order order-pay">
    <header>
        <div class="width">
            <h1 data-name="order-server-1"><?php echo $texts['order-server-1']['content']; ?></h1>
            <div class="tabs">
                <div class="params"><span>1</span><span data-name="order-server-2"><?php echo $texts['order-server-2']['content']; ?></span></div>
                <div class="enter"><span>2</span><span data-name="order-server-3"><?php echo $texts['order-server-3']['content']; ?></span></div>
                <div class="pay active"><span>3</span><span data-name="order-server-4"><?php echo $texts['order-server-4']['content']; ?></span></div>
            </div>
        </div>
    </header>
    <div class="order-block">
        <div class="width">
            <section class="pay-type">
                <header data-name="order-server-48"><?php echo $texts['order-server-48']['content']; ?></header>
                <div class="type-block">
                    <div class="visa">
                        <figure><img src="/images/system/visa-pay.png" alt="visa"></figure>
                        <span data-name="order-server-34"><?php echo $texts['order-server-34']['content']; ?></span>
                    </div>
                    <div class="yandex">
                        <figure><img src="/images/system/yandex-pay.png" alt="yandex"></figure>
                        <span data-name="order-server-35"><?php echo $texts['order-server-35']['content']; ?></span>
                    </div>
                    <div class="mastercard">
                        <figure><img src="/images/system/mastercard-pay.png" alt="mastercard"></figure>
                        <span data-name="order-server-36"><?php echo $texts['order-server-36']['content']; ?></span>
                    </div>
                    <div class="paypal">
                        <figure><img src="/images/system/paypal-pay.png" alt="paypal"></figure>
                        <span data-name="order-server-37"><?php echo $texts['order-server-37']['content']; ?></span>
                    </div>
                    <div class="bank">
                        <figure><img src="/images/system/bank-pay.png" alt="bill"></figure>
                        <span data-name="order-server-38"><?php echo $texts['order-server-38']['content']; ?></span>
                    </div>
                    <div class="webmoney">
                        <figure><img src="/images/system/WebMoney-pay.png" alt="WebMoney"></figure>
                        <span data-name="order-server-39"><?php echo $texts['order-server-39']['content']; ?></span>
                    </div>
                    <div class="bitcoin">
                        <figure><img src="/images/system/bitcoin-pay.png" alt="bitcoin"></figure>
                        <span data-name="order-server-40"><?php echo $texts['order-server-40']['content']; ?></span>
                    </div>
                </div>
            </section>
            <section class="sum">
                <header data-name="order-server-49"><?php echo $texts['order-server-49']['content']; ?></header>
                <div class="sum-block">
                    <div class="price">
                        <span>137.99$</span>
                        <span class="discount" data-name="order-server-41"><?php echo $texts['order-server-41']['content']; ?></span>
                    </div>
                    <div class="programm">
                        <div class="title" data-name="order-server-42"><?php echo $texts['order-server-42']['content']; ?></div>
                        <p data-name="order-server-43"><?php echo $texts['order-server-43']['content']; ?></p>
                        <div class="agree-block">
                            <div class="agree" data-name="order-server-44"><?php echo $texts['order-server-44']['content']; ?></div>
                            <div class="switch"></div>
                        </div>
                        <div class="donation">
                            <div class="var active">
                                <label>
                                    <span data-name="order-server-45"><?php echo $texts['order-server-45']['content']; ?></span>
                                    <input type="text" value="" name="" placeholder="0">
                                </label>
                            </div>
                            <div class="var perc">
                                <span class="value">5%</span>
                                <div class="donate-perc"></div>
                                <span>100%</span>
                            </div>
                            <div class="total">
                                <span data-name="order-server-46"><?php echo $texts['order-server-46']['content']; ?></span>
                                <span>146,00$</span>
                            </div>
                        </div>
                    </div>
                    <button class="button" data-name="order-server-47"><?php echo $texts['order-server-47']['content']; ?></button>
                </div>
            </section>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.agree-block .switch').slider({
            range: "min",
            value: 1,
            min: 1,
            max: 2,
            step: 1,
            slide: function(event, ui) {
                if (ui.value == 1) {
                    $('.agree-block .switch').removeClass('active');
                    $('.donation').removeClass('active');
                    $('.discount').hide().parent('.price').removeClass('active');
                } else {
                    $('.agree-block .switch').addClass('active');
                    $('.donation').addClass('active');
                    $('.discount').show().parent('.price').addClass('active');
                }
            }
        });
        $('.donation .donate-perc').slider({
            range: "min",
            value: 5,
            min: 5,
            max: 100,
            step: 1,
            slide: function(event, ui) {
                $('.donation .value').text(ui.value + '%');
            }
        });
    });
</script>