<?php
use yii\helpers\StringHelper;
use yii\helpers\Url;
use common\models\Option;
?>
<div class="order vps-order">
    <header>
        <div class="width">
            <h1 data-name="order-vps-1"><?php echo $texts['order-vps-1']['content']; ?></h1>
            <div class="tabs">
                <div class="active params"><span>1</span><span data-name="order-server-2"><?php echo $texts['order-server-2']['content']; ?></span></div>
                <div class="enter"><span>2</span><span data-name="order-server-3"><?php echo $texts['order-server-3']['content']; ?></span></div>
                <div class="pay"><span>3</span><span data-name="order-server-4"><?php echo $texts['order-server-4']['content']; ?></span></div>
            </div>
        </div>
    </header>
    <?php if (!empty($vps)) {?>
    <div class="order-block">
        <div class="width">
            <form action="<?php echo Url::toRoute('vps-order/pay'); ?>" id="order-vps" method="post">
                <input type="hidden" name="_csrf-frontend" value="<?php echo Yii::$app->request->getCsrfToken(); ?>">
                <section class="params-block">
                    <header>
                        <figure><img src="/images/system/<?php echo $vps->img; ?>" alt="медведь"></figure>
                        <span><?php echo $vps->langText[0]['title']; ?></span>
                        <span>31.59$<span class="min">/мес.</span></span>
                    </header>
                    <div class="options-block">
                        <div class="options-header">
                            <span><span class="bold"><?php echo $vps->langText[0]['cpu']; ?> </span>процессора, <span class="bold"><?php echo $vps->langText[0]['ram']; ?></span> оперативной памяти, <span class="bold"><?php echo $vps->langText[0]['hdd']; ?></span> диск</span>
                            <span><span class="bold">24.99$</span>/мес.</span>
                        </div>
                        <div class="options-cont">
                            <div class="option">
                                <div class="title">Шаблон ОС</div>
                                <div class="option-box">
                                    <div class="select-box os">
                                        <div class="checked">Centos-7-x86_64</div>
                                        <ul style="display: none;">
                                            <li data-id="1">
                                                <span>Centos-7-x86_64</span>
                                                <span>Бесплатно</span>
                                            </li>
                                            <li data-id="2">
                                                <span>Debian-7-x86_64</span>
                                                <span>Бесплатно</span>
                                            </li>
                                            <li data-id="3">
                                                <span>Debian-8-x86_64-minimal</span>
                                                <span>Бесплатно</span>
                                            </li>
                                            <li data-id="4">
                                                <span>Ubuntu-14.04-x86_64-minimal</span>
                                                <span>Бесплатно</span>
                                            </li>
                                            <li data-id="5">
                                                <span>Ubuntu-14.04-amd64</span>
                                                <span>Бесплатно</span>
                                            </li>
                                        </ul>
                                        <input type="hidden" value="" name="os">
                                    </div>
                                </div>
                                <div class="price">Бесплатно</div>
                            </div>
                            <?php if (!empty($options)) {?>
                            <?php $label = 1;?>
                                <?php foreach ($options as $option) {?>
                                    <?php if ($option->option_type == Option::OPTION_TYPE_LIST) { ?>
                                        <div class="option">
                                            <div class="title"><?php echo $option->langOption[0]['title']; ?></div>
                                            <div class="option-box">
                                                <div class="select-box os">
                                        <?php if (!empty($option->childItems)) {?>
                                            <?php $checked['title'] = $option->childItems[0]->langOption[0]['title']; ?>
                                            <?php $checked['id'] = $option->childItems[0]->id; ?>
                                            <?php foreach ($option->childItems as $child) {?>
                                                <?php if ($child->is_checked) {
                                                    $checked['title'] = $child->langOption[0]['title'];
                                                    $checked['id'] = $child->id;
                                                } ?>
                                            <?php } ?>
                                                    <div class="checked"><?php echo $checked['title']; ?></div>
                                                    <ul>
                                            <?php foreach ($option->childItems as $child) {?>
                                                        <li data-id="<?php echo $child->id; ?>">
                                                            <span><?php echo $child->langOption[0]['title']; ?></span>
                                                        </li>
                                            <?php } ?>
                                                    </ul>
                                                    <input type="hidden" value="<?php echo $checked['id']; ?>" name="<?php echo $option->name; ?>">
                                        <?php } ?>
                                                </div>
                                            </div>
                                            <div class="price">
                                                <?php if (!$option->price) {
                                                    echo 'Бесплатно';
                                                 } else { ?>
                                                    <span><?php echo number_format($option->price, 2); ?>$<span class="min">/мес.</span></span>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($option->option_type == Option::OPTION_TYPE_SLIDE) {?>
                                        <div class="option">
                                            <div class="title"><?php echo $option->langOption[0]['title']; ?></div>
                                            <div class="option-box">
                                                <div class="slide">
                                                    <input type="text" class="res"  value="<?php echo $option->min; ?>" name="<?php echo $option->name; ?>" readonly="readonly">
                                                    <div data-option="<?php echo $option->name; ?>"></div>
                                                </div>
                                            </div>
                                            <div class="price">+<?php echo number_format($option->price, 2); ?>$</div>
                                            <script type="text/javascript">
                                                $(document).ready(function() {
                                                    $('.option div[data-option="<?php echo $option->name; ?>"]').slider({
                                                        range: "min",
                                                        value: <?php echo $option->min; ?>,
                                                        min: <?php echo $option->min; ?>,
                                                        max: <?php echo $option->max; ?>,
                                                        step: 2,
                                                        slide: function(event, ui) {
                                                            $(this).parents('.option').find('.price').text('+' + accounting.formatNumber(ui.value * <?php echo $option->price; ?>, 2) + '$');
                                                            $(this).parents('.option').find('input').val(ui.value);
                                                        }
                                                    });
                                                });
                                            </script>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                            <div class="option">
                                <div class="title">Имя сервера</div>
                                <div class="option-box servername">
                                    <input type="text" placeholder="servername.ru" name="domen">
                                    <i class="fa fa-question-circle-o"></i>
                                    <div class="description" data-name="order-vps-7">Текст</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="config">
                    <header data-name="order-vps-8"><?php echo $texts['order-vps-8']['content']; ?></header>
                    <div class="conf-block">
                        <input type="hidden" name="server_id" value="<?php echo $vps->id; ?>">
                        <div class="price">137.99$</div>
                        <div class="radio-block">
                            <div class="radio-btn">
                                <span class="false-radio active">
                                    <input type="radio" name="period" value="month" id="month" checked>
                                </span>
                                <label for="month">за месяц</label>
                            </div>
                            <div class="radio-btn">
                                <span class="false-radio">
                                    <input type="radio" name="period" value="3-month" id="month3">
                                </span>
                                <label for="month3">за 3 месяца</label>
                            </div>
                            <div class="radio-btn">
                                <span class="false-radio">
                                    <input type="radio" name="period" value="half-year" id="half-year">
                                </span>
                                <label for="half-year">за полгода</label>
                            </div>
                            <div class="radio-btn">
                                <span class="false-radio">
                                    <input type="radio" name="period" value="year" id="year">
                                </span>
                                <label for="year">за год</label>
                            </div>
                        </div>
                        <button class="button" data-name="order-vps-9"><?php echo $texts['order-vps-9']['content']; ?></button>
                        <p><span data-name="order-server-32"><?php echo $texts['order-server-32']['content']; ?></span> <a href=""><span data-name="order-server-33"><?php echo $texts['order-server-33']['content']; ?></span></a></p>
                    </div>
                </section>
            </form>
        </div>
    </div>
    <?php }  else { ?>
        <p>Вы ничего не выбрали</p>
    <?php } ?>
</div>
<script type="text/javascript">
    //close select
    $(document).click(function(e){
        var className = e.target.className;
        if (className.indexOf('checked') == -1) {
            var elem =  $('.order .select-box ul');
            if (e.target != elem[0] && !elem.has(e.target).length) {
                elem.hide();
                $('.order .select-box .checked').removeClass('active');
            }
        }
        if (className.indexOf('fa-question-circle-o') == -1) {
            var elemDesc =  $('.order .servername .description');
            if (e.target != elemDesc[0] && !elemDesc.has(e.target).length) {
                elemDesc.hide();
            }
        }
    });
</script>