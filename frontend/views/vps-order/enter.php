<?php
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
?>
<div class="order order-enter">
    <header>
        <div class="width">
            <h1 data-name="order-server-1"><?php echo $texts['order-server-1']['content']; ?></h1>
            <div class="tabs">
                <div class="params"><span>1</span><span data-name="order-server-2"><?php echo $texts['order-server-2']['content']; ?></span></div>
                <div class="enter active"><span>2</span><span data-name="order-server-3"><?php echo $texts['order-server-3']['content']; ?></span></div>
                <div class="pay"><span>3</span><span data-name="order-server-4"><?php echo $texts['order-server-4']['content']; ?></span></div>
            </div>
        </div>
    </header>
    <div class="order-block">
        <div class="width">
            <div class="account-form active">
                <div class="title" data-name="enter-7"><?php echo $this->params['texts']['enter-7']['content']; ?></div>
                <?php $form = ActiveForm::begin([
                    'id' => 'order-enter',
                    'enableClientValidation' => false,
                ]); 
                $not_active = isset($account->errors['not_active']) ? '<p class="help-block help-block-error">' .  Yii::t('app', 'confirm_email', ['link' => '/vps-order/confirm']) . '</p>' : '';
                ?>
                <div class="content">
                    <div class="input">
                        <?php echo $form->field($account, 'email', [
                                'template' => '{error}' . $not_active . '<i class="fa fa-envelope"></i>{input}'
                            ]
                        )->input('email', [
                            'placeholder' => Yii::t('app', 'email'),
                            'id' => 'enter-order-email'
                        ]); ?>
                    </div>
                    <div class="input">
                        <?php echo $form->field($account, 'password', [
                                'template' => '{error}<i class="fa fa-key" aria-hidden="true"></i>{input}<i class="fa fa-eye-slash"></i>'
                            ]
                        )->input('password', [
                            'placeholder' => Yii::t('app', 'password'),
                            'id' => 'enter-order-password'
                        ]); ?>
                    </div>
                    <button class="button" data-name="enter-6"><?php echo $this->params['texts']['enter-6']['content']; ?></button>
                    <a href="<?php echo Url::to(['/request-password-reset']); ?>" data-name="enter-3"><?php echo $this->params['texts']['enter-3']['content']; ?></a>
                </div>
                <?php ActiveForm::end(); ?>
                <div class="sub">
                    <div class="content">
                        <span data-name="enter-4"><?php echo $this->params['texts']['enter-4']['content']; ?></span>
                        <a id="order-signup"><span data-name="enter-5"><?php echo $this->params['texts']['enter-5']['content']; ?></span></a>
                    </div>
                </div>
            </div>
            <div class="signup-form">
                <?php $form = ActiveForm::begin([
                    'id' => 'signup',
                    'enableAjaxValidation' => false,
                    'enableClientValidation' => true,
                ]); ?>
                <div class="form-block info">
                    <div class="title" data-name="signup-3" data-id=""><?php echo $texts['signup-3']['content']; ?></div>
                    <div class="inputs">
                        <?php echo $form->field($signup, 'name', [
                                'template' => '{error}<label>
                                <span class="name">' . Yii::t('app', 'name') . ':</span>
                                <span class="input">
                                    <i class="fa fa-user-circle-o"></i>
                                    {input}
                                </span>
                            </label>']
                        )->input('text'); ?>
                        <?php echo $form->field($signup, 'lastname', [
                                'template' => '{error}<label>
                                <span class="name">' . Yii::t('app', 'lastname') . ':</span>
                                <span class="input">
                                    <i class="fa fa-user-circle"></i>
                                    {input}
                                </span>
                            </label>']
                        )->input('text'); ?>
                        <?php echo $form->field($signup, 'email', [
                                'template' => '{error}<label>
                                <span class="name">' . Yii::t('app', 'email') . ':</span>
                                <span class="input">
                                    <i class="fa fa-envelope-square"></i>
                                    {input}
                                </span>
                            </label>']
                        )->input('email'); ?>
                        <?php echo $form->field($signup, 'password', [
                                'template' => '{error}<label>
                                <span class="name">' . Yii::t('app', 'password') . ':</span>
                                <span class="input">
                                    <i class="fa fa-lock"></i>
                                    {input}
                                    <i class="fa fa-eye-slash"></i>
                                </span>
                            </label>']
                        )->input('password'); ?>
                        <?php echo $form->field($signup, 'password_repeat', [
                                'template' => '{error}<label>
                                <span class="name">' . Yii::t('app', 'password_repeat') . ':</span>
                                <span class="input">
                                    <i class="fa fa-lock"></i>
                                    {input}
                                    <i class="fa fa-eye-slash"></i>
                                </span>
                            </label>']
                        )->input('password'); ?>
                        <div class="sub" data-name="signup-4" data-id=""><?php echo $texts['signup-4']['content']; ?></div>
                    </div>
                </div>
                <div class="form-block contact">
                    <div class="title" data-name="signup-5" data-id=""><?php echo $texts['signup-5']['content']; ?></div>
                    <div class="inputs">
                        <?php echo $form->field($signup, 'phone', [
                                'template' => '{error}<label>
                                <span class="name">' . Yii::t('app', 'phone') . ':</span>
                                <span class="input phone-mask">
                                    <i class="fa fa-phone"></i>
                                    {input}
                                </span>
                            </label>']
                        )->input('text'); ?>
                        <?php echo $form->field($signup, 'country', [
                                'template' => '{error}<label>
                                <span class="name" >' . Yii::t('app', 'country') . ':</span>
                                <span class="input">
                                    <i class="fa fa-globe"></i>
                                    {input}
                                </span>
                            </label>']
                        )->input('text'); ?>
                        <?php echo $form->field($signup, 'address', [
                                'template' => '{error}<label>
                                <span class="name">' . Yii::t('app', 'address') . ':</span>
                                <span class="input">
                                    <i class="fa fa-map-marker"></i>
                                    {input}
                                </span>
                            </label>']
                        )->input('text'); ?>
                        <?php echo $form->field($signup, 'agree', [
                            'template' => '{error}<label>
                                <div class="check">{input}</div>
                                <span class="agree" data-name="signup-6" data-id="">' . $texts['signup-6']['content'] . '</span>
                            </label>',
                        ])->input('checkbox', [
                            'class' => 'checkbox',
                        ])->label('Активность'); ?>
                        <?php echo Html::submitButton($texts['signup-7']['content'], [
                            'class' => 'button',
                            'data-name' => 'signup-7',
                            'data-id' => '',
                            'disabled' => true
                        ]) ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
//        $('#order-enter').on('beforeSubmit', function() {
//            var $this = $(this);
//            $this.find('.help-block-error').text('');
//
//            var email = $('#enter-order-email').val();
//            var password = $('#enter-order-password').val();
//
//            $.ajax({
//                url: '<?php //echo Url::to(['main/enter']); ?>//',
//                type: 'post',
//                dataType: 'json',
//                data: {
//                    email: email,
//                    password: password,
//                    _csrf: yii.getCsrfToken()
//                },
//                success: function (response) {
//                    if (response.valid.length != 0) {
//                        console.log(response.valid);
//                        if (response.valid['accountform-password']) {
//                            $this.find('.field-enter-order-email').find('.help-block-error').text(response.valid['accountform-password'][0]);
//                        }
//                        if (response.valid['accountform-not_active']) {
//                            $this.find('.field-enter-order-email').find('.help-block-error').html(response.valid['accountform-not_active'][0]);
//                        }
//                    }
//                },
//                error: function (response) {
//                }
//            });
//            return false;
//        });
    });
</script>