<?php
use frontend\components\AccountMenu;
use yii\helpers\Url;
?>
<div class="account">
    <div class="width">
        <?php echo AccountMenu::widget(); ?>
        <div class="content">
            <div class="title">
                <span data-name="account-1"><?php echo $texts['account-1']['content']; ?></span>
                <span data-name="account-2"><?php echo $texts['account-2']['content']; ?></span>
            </div>
            <div class="cont-menu">
                <ul>
                    <li><a href="">
                            <figure><img src="/images/system/notification.svg" alt=""></figure>
                            <span data-name="account-3"><?php echo $texts['account-3']['content']; ?></span>
                        </a>
                    </li>
                    <li><a href="">
                            <figure><img src="/images/system/24-hours.svg" alt=""></figure>
                            <span data-name="account-4"><?php echo $texts['account-4']['content']; ?></span>
                        </a>
                    </li>
                    <li class="option"><a href="">
                            <figure><img src="/images/system/shopping-cart.svg" alt=""></figure>
                            <span data-name="account-5"><?php echo $texts['account-5']['content']; ?></span>
                        </a>
                    </li>
                    <li><a href="">
                            <figure><img src="/images/system/placeholder.svg" alt=""></figure>
                            <span data-name="account-6"><?php echo $texts['account-6']['content']; ?></span>
                        </a></li>
                    <li class="inv"><a href="">
                            <figure><img src="/images/system/credit-card.svg" alt=""></figure>
                            <span data-name="account-7"><?php echo $texts['account-7']['content']; ?></span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="person">
                <?php if (!empty($user)) {?>
                <div class="title" data-name="account-8"><?php echo $texts['account-8']['content']; ?></div>
                <div class="info">
                    <div class="item">
                        <div class="name">
                            <i class="fa fa-user-circle-o"></i>
                            <span data-name="account-9"><?php echo $texts['account-9']['content']; ?></span>
                        </div>
                        <div class="value"><?php echo $user->username; ?></div>
                    </div>
                    <div class="item">
                        <div class="name">
                            <i class="fa fa-user-circle"></i>
                            <span data-name="account-10"><?php echo $texts['account-10']['content']; ?></span>
                        </div>
                        <div class="value"><?php echo $user->lastname; ?></div>
                    </div>
                    <div class="item">
                        <div class="name">
                            <i class="fa fa-calendar"></i>
                            <span data-name="account-11"><?php echo $texts['account-11']['content']; ?></span>
                        </div>
                        <div class="value"><?php echo date('d.m.Y', $user->created_at);?></div>
                    </div>
                    <div class="item">
                        <div class="name">
                            <i class="fa fa-envelope-square"></i>
                            <span data-name="account-12"><?php echo $texts['account-12']['content']; ?></span>
                        </div>
                        <div class="value"><?php echo $user->email; ?></div>
                    </div>
                    <div class="item">
                        <div class="name">
                            <i class="fa fa-phone"></i>
                            <span data-name="account-13"><?php echo $texts['account-13']['content']; ?></span>
                        </div>
                        <div class="value"><?php echo $user->phone; ?></div>
                    </div>
                    <div class="item">
                        <div class="name">
                            <i class="fa fa-globe"></i>
                            <span data-name="account-14"><?php echo $texts['account-14']['content']; ?></span>
                        </div>
                        <div class="value"><?php echo $user->country; ?></div>
                    </div>
                    <div class="item">
                        <div class="name">
                            <i class="fa fa-map-marker"></i>
                            <span data-name="account-15"><?php echo $texts['account-15']['content']; ?></span>
                        </div>
                        <div class="value"><?php echo $user->address; ?></div>
                    </div>
                    <div class="buttons">
                        <a class="button" href="<?php echo Url::to(['account/edit']); ?>" data-name="account-16"><?php echo $texts['account-16']['content']; ?></a>
                        <a href="<?php echo Url::to(['account/reset-password']); ?>" data-name="account-17"><?php echo $texts['account-17']['content']; ?></a>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>