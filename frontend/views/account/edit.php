<?php
use frontend\components\AccountMenu;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
?>
<div class="account edit">
    <div class="width">
        <?php echo AccountMenu::widget(); ?>
        <div class="content">
            <div class="person">
                <div class="title" data-name="account-18"><?php echo $texts['account-18']['content']; ?></div>
                <?php if (!empty($user)) {?>
                <div class="box-edit">
                    <?php $form = ActiveForm::begin([
                        'id' => 'account_edit',
                        'enableClientValidation' => false,
                    ]); ?>
                    <div class="item">
                        <div class="name"><?php echo Yii::t('app', 'email'); ?>:</div>
                        <div class="input">
                            <i class="fa fa-envelope-square"></i>
                            <div><?php echo $user->email; ?></div>
                            <p data-name="account-19"><?php echo $texts['account-19']['content']; ?></p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="name">
                            <span><?php echo Yii::t('app', 'name'); ?>:</span> <span class="red">*</span>
                        </div>
                        <div class="input">
                            <?php echo $form->field($model, 'name', [
                                    'template' => '{error}<i class="fa fa-user-circle-o"></i>{input}'
                                ]
                            )->input('text', [
                                'value' => $user->username
                            ]); ?>
                        </div>
                    </div>
                    <div class="item">
                        <div class="name">
                            <span><?php echo Yii::t('app', 'lastname'); ?>:</span> <span class="red">*</span>
                        </div>
                        <div class="input">
                            <?php echo $form->field($model, 'lastname', [
                                    'template' => '{error}<i class="fa fa-user-circle"></i>{input}'
                                ]
                            )->input('text', [
                                'value' => $user->lastname
                            ]); ?>
                        </div>
                    </div>
                    <div class="item">
                        <div class="name"><?php echo Yii::t('app', 'phone'); ?>:</div>
                        <div class="input">
                            <?php echo $form->field($model, 'phone', [
                                    'template' => '{error}<i class="fa fa-phone"></i>{input}'
                                ]
                            )->input('text', [
                                'value' => $user->phone
                            ]); ?>
                        </div>
                    </div>
                    <div class="item">
                        <div class="name"><?php echo Yii::t('app', 'country'); ?>:</div>
                        <div class="input">
                            <?php echo $form->field($model, 'country', [
                                    'template' => '{error}<i class="fa fa-globe"></i>{input}'
                                ]
                            )->input('text', [
                                'value' => $user->country
                            ]); ?>
                        </div>
                    </div>
                    <div class="item textarea">
                        <div class="name"><?php echo Yii::t('app', 'address'); ?>:</div>
                        <div class="input">
                            <i class="fa fa-map-marker"></i>
                            <?php $model->address = $user->address; ?>
                            <?php echo $form->field($model, 'address')->textarea()->label(false); ?>
                        </div>
                    </div>
                    <div class="sub"><span class="required">*</span> <span data-name="account-20"><?php echo $texts['account-20']['content']; ?></span></div>
                    <div class="buttons">
                        <?php echo Html::submitButton($texts['account-21']['content'], [
                            'class' => 'button',
                            'data-name' => 'account-21'
                        ]) ?>
                        <a class="button" href="<?php echo Url::to(['account/edit']); ?>" data-name="account-22"><?php echo $texts['account-22']['content']; ?></a>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>