<?php
use frontend\components\AccountMenu;
use yii\helpers\Url;
?>
<div class="account servers">
    <div class="description"></div>
    <div class="action-wrap act-stop" data-id="">
        <div class="action">
            <div class="icon"><i class="mdi mdi-stop"></i></div>
            <div class="title">Отключение питания</div>
            <p>Вы действительно хотите отключить питание сервера? Это приведет к (текст о возможных последствия действия)</p>
            <div class="buttons">
                <div class="button yes">Да</div>
                <div class="button cancel">Отмена</div>
            </div>
        </div>
    </div>
    <div class="action-wrap act-play" data-id="">
        <div class="action">
            <div class="icon"><i class="mdi mdi-play"></i></div>
            <div class="title">Включение питания</div>
            <p>Вы хотите включить питание сервера? Это приведет к (текст о возможных последствия действия)</p>
            <div class="buttons">
                <div class="button yes">Да</div>
                <div class="button cancel">Отмена</div>
            </div>
        </div>
    </div>
    <div class="action-wrap act-refresh" data-id="">
        <div class="action">
            <div class="icon"><i class="mdi mdi-refresh"></i></div>
            <div class="title">Перезагрузка сервера</div>
            <p>Вы действительно хотите перезагрузить сервер? Это приведет к (текст о возможных последствия действия)</p>
            <div class="buttons">
                <div class="button yes">Да</div>
                <div class="button cancel">Отмена</div>
            </div>
        </div>
    </div>
    <div class="action-wrap act-reinstall" data-id="">
        <div class="action">
            <div class="icon"><i class="mdi mdi-autorenew"></i></div>
            <div class="title">Переустановка ОС</div>
            <p>Выбирите операционную систему из списка (по умолчанию выбрана текущая ОС):</p>
            <?php if (!empty($os)) {?>
                <?php $first = true; ?>
                <div class="select-box">
                    <ul>
                        <?php foreach ($os as $val) { ?>
                            <?php foreach ($val->elem as $item) { ?>
                                <?php if ($first) {
                                    $firstId = (array)$item->id;
                                    $firstOsId = $firstId['$'];
                                    $firstName = (array)$item->name;
                                    $firstOSName = $firstName['$'];
                                    $first = false;
                                } ?>
                                <?php $id = (array)$item->id; ?>
                                <?php $name = (array)$item->name; ?>
                                <li data-id="<?php echo $id['$']; ?>">
                                    <span><?php echo $name['$']; ?></span>
                                    <span>Бесплатно</span>
                                </li>
                            <?php } ?>
                        <?php } ?>
                    </ul>
                    <div class="checked"><?php echo $firstOSName; ?></div>
                    <input type="hidden" value="<?php echo $firstOsId; ?>" name="os">
                </div>
            <?php } ?>
            <div class="buttons">
                <div class="button yes">Переустановить</div>
                <div class="button cancel">Отмена</div>
            </div>
        </div>
    </div>
    <div class="action-wrap act-delete" data-id="">
        <div class="action">
            <div class="icon"><i class="mdi mdi-delete-forever"></i></div>
            <div class="title">Удаление сервера</div>
            <p>Вы действительно хотите удалить и больше не использовать этот сервер? Внимание! Все данные с сервера будут удалены.</p>
            <div class="buttons">
                <div class="button yes">Да</div>
                <div class="button cancel">Отмена</div>
            </div>
        </div>
    </div>
    <div class="width">
        <?php echo AccountMenu::widget(); ?>
        <div class="content">
            <div class="title">
                <figure><img src="/images/system/server.svg" alt="server"></figure>
                <span>Выделенные серверы</span>
            </div>
            <div class="cont-server">
                <table class="table">
                    <tr class="head">
                        <th class="id"><div>ID</div></th>
                        <th class="domen"><div>Доменное имя</div></th>
                        <th class="conf"><div>Конфигурация</div></th>
                        <th class="state"><div>Состояние</div></th>
                        <th class="date"><div>Действует до</div></th>
                        <th class="cost"><div>Стоимость</div></th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>server.ru</td>
                        <td class="conf">
                            <span>Xeon E5-1286v3 / 4.1 ГГц 4 ядра</span>
                            <span>8 Гб RAM / 960 SSD - 3000 SATA</span>
                        </td>
                        <td class="state">
                            <i class="mdi mdi-power-plug" data-hint="Порт питания Power1:23-port – включен"></i>
                            <i class="mdi mdi-server-network" data-hint="Порт коммутатора – кабель подключен"></i>
                            <i class="mdi mdi-alert" data-hint="Последняя операция завершилась с ошибкой"></i>
                        </td>
                        <td class="date alert"><span data-hint="Срок оплаты истекает через 5 дней">19.09.2017</span></td>
                        <td class="price"><span>30$</span> / мес.</td>
                    </tr>
                    <tr class="character" data-id="1">
                        <td colspan="6">
                            <div>
                                <div class="slide-up">
                                    <i class="mdi mdi-chevron-double-up"></i>
                                    <span>Свернуть</span>
                                </div>
                                <div class="actions">
                                    <div class="act power stop">
                                        <div>
                                            <i class="mdi mdi-stop"></i>
                                            <i class="mdi mdi-play"></i>
                                        </div>
                                        <span>Питание вкл / выкл</span>
                                    </div>
                                    <div class="act reload">
                                        <div>
                                            <i class="mdi mdi-refresh"></i>
                                        </div>
                                        <span>Перезагрузить</span>
                                    </div>
                                    <div class="act reinstall">
                                        <div>
                                            <i class="mdi mdi-autorenew"></i>
                                        </div>
                                        <span>Переустановить ОС</span>
                                    </div>
                                    <div class="act delete">
                                        <div>
                                            <i class="mdi mdi-delete-forever"></i>
                                        </div>
                                        <span>Удалить сервер</span>
                                    </div>
                                </div>
                                <div class="info">
                                    <div class="state-info">
                                        <div class="title">Состояние:</div>
                                        <div class="state-item power">
                                            <i class="mdi mdi-power-plug"></i>
                                            <span>Порт питания Power1:23-port – <span>включен</span></span>
                                        </div>
                                        <div class="state-item">
                                            <i class="mdi mdi-server-network"></i>
                                            <span>Порт коммутатора 10.10.10.4 Switch 100:FastEthernet0/48 [default] – кабель <span>подключен</span></span>
                                        </div>
                                        <div class="state-item error">
                                            <i class="mdi mdi-alert"></i>
                                            <span>Последняя операция завершилась с <span>ошибкой</span>. Если ошибка повторяется, обратитесь к администратору.</span>
                                        </div>
                                        <div class="buttons">
                                            <a href="" class="button">
                                                <i class="fa fa-comments-o"></i>
                                                <span>Задать вопрос по услуге</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="period-info">
                                        <div class="title">Действует до: <span class="period-alert">19.09.2017</span></div>
                                        <div class="period">Срок оплаты истекает через <span>5 дней!</span></div>
                                        <div class="extend">
                                            <span>Продлить:</span>
                                            <div class="radio-block">
                                                <div class="radio-btn">
                                                <span class="false-radio active">
                                                    <input type="radio" name="period" value="month" id="month" checked="">
                                                </span>
                                                    <label for="month">на месяц <span>(39,35 $)</span></label>
                                                </div>
                                                <div class="radio-btn">
                                                <span class="false-radio">
                                                    <input type="radio" name="period" value="half-year" id="half-year">
                                                </span>
                                                    <label for="half-year">на полгода <span>(249,25 $)</span></label>
                                                </div>
                                                <div class="radio-btn">
                                                <span class="false-radio">
                                                    <input type="radio" name="period" value="3-month" id="month3">
                                                </span>
                                                    <label for="month3">на 3 месяца <span>(119,10 $)</span></label>
                                                </div>
                                                <div class="radio-btn">
                                                <span class="false-radio">
                                                    <input type="radio" name="period" value="year" id="year">
                                                </span>
                                                    <label for="year">на год <span>(249,25 $)</span></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="buttons">
                                            <a href="" class="button pay">Продлить</a>
                                            <a href="" class="button pay-history">история платежей</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="server-character">
                                    <div class="title">Характеристики сервера</div>
                                    <div class="conf-box">
                                        <div>
                                            <i class="zmdi zmdi-memory"></i>
                                            <span>Процессор:<span>Xeon E5-1286v3 / 4.1 ГГц 4 ядра</span></span>
                                        </div>
                                        <div>
                                            <i class="zmdi zmdi-storage"></i>
                                            <span>Диски:<span>960 SSD - 3000 SATA</span></span>
                                        </div>
                                        <div>
                                            <i class="zmdi zmdi-ruler"></i>
                                            <span>Оперативная память:<span>8 Гб</span></span>
                                        </div>
                                        <div>
                                            <i class="mdi mdi-image-filter-center-focus"></i>
                                            <span>RAID / IPMI:<span class="raid on"></span><span class="ipmi"></span></span>
                                        </div>
                                        <div>
                                            <i class="zmdi zmdi-select-all"></i>
                                            <span>Аппаратный контроллер:<span>Adaptec ASR-5405</span></span>
                                        </div>
                                    </div>
                                    <div class="charac-box">
                                        <div>
                                            <span>Имя хоста:</span><span>serv100.ds</span>
                                        </div>
                                        <div>
                                            <span>Шаблон ОС:</span><span>Windows Server 2012 R2 Clean</span>
                                        </div>
                                        <div>
                                            <span>Ip-адрес:</span><span>185.100.222.108</span>
                                        </div>
                                        <div>
                                            <span>Панель управления:</span><span>ISPmanager 5 Lite</span>
                                        </div>
                                        <div>
                                            <span>Нагрузка:</span><span>300 bit/s</span>
                                        </div>
                                        <div>
                                            <span>Администрирование:</span><span>расширенный уровень</span>
                                        </div>
                                        <div>
                                            <span>Трафик:</span><span>Порт 1 Гбит/с, неограниченный трафик по каналу 250 Мбит/с</span>
                                        </div>
                                        <div>
                                            <span>Дополнительные IPv4:</span><span>3 шт.</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="slide-up bottom">
                                    <i class="mdi mdi-chevron-double-up"></i>
                                    <span>Свернуть</span>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>domen.ru</td>
                        <td class="conf">
                            <span>AMD Phenom(tm) 9650 Quad-Core</span>
                            <span>4 Гб RAM / 250 Гб SATA</span>
                        </td>
                        <td class="state">
                            <i class="mdi mdi-power-plug" data-hint="Порт питания Power1:23-port – включен"></i>
                            <i class="mdi mdi-server-network-off" data-hint="Порт коммутатора – кабель отключен"></i>
                            <i class="mdi mdi-alert" data-hint="Последняя операция завершилась с ошибкой"></i>
                        </td>
                        <td class="date late"><span data-hint="Срок эксплуатации истёк">11.09.2017</span></td>
                        <td class="price"><span>15$</span> / мес.</td>
                    </tr>
                    <tr class="character" data-id="2">
                        <td colspan="6">
                            <div>
                                <div class="slide-up">
                                    <i class="mdi mdi-chevron-double-up"></i>
                                    <span>Свернуть</span>
                                </div>
                                <div class="actions">
                                    <div class="act power play">
                                        <div>
                                            <i class="mdi mdi-stop"></i>
                                            <i class="mdi mdi-play"></i>
                                        </div>
                                        <span>Питание вкл / выкл</span>
                                    </div>
                                    <div class="act reload">
                                        <div>
                                            <i class="mdi mdi-refresh"></i>
                                        </div>
                                        <span>Перезагрузить</span>
                                    </div>
                                    <div class="act reinstall">
                                        <div>
                                            <i class="mdi mdi-autorenew"></i>
                                        </div>
                                        <span>Переустановить ОС</span>
                                    </div>
                                    <div class="act delete">
                                        <div>
                                            <i class="mdi mdi-delete-forever"></i>
                                        </div>
                                        <span>Удалить сервер</span>
                                    </div>
                                </div>
                                <div class="info">
                                    <div class="state-info">
                                        <div class="title">Состояние:</div>
                                        <div class="state-item power">
                                            <i class="mdi mdi-power-plug"></i>
                                            <span>Порт питания Power1:23-port – <span>включен</span></span>
                                        </div>
                                        <div class="state-item">
                                            <i class="mdi mdi-server-network"></i>
                                            <span>Порт коммутатора 10.10.10.4 Switch 100:FastEthernet0/48 [default] – кабель <span>подключен</span></span>
                                        </div>
                                        <div class="state-item error">
                                            <i class="mdi mdi-alert"></i>
                                            <span>Последняя операция завершилась с <span>ошибкой</span>. Если ошибка повторяется, обратитесь к администратору.</span>
                                        </div>
                                        <div class="buttons">
                                            <a href="" class="button">
                                                <i class="fa fa-comments-o"></i>
                                                <span>Задать вопрос по услуге</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="period-info">
                                        <div class="title">Действует до: <span class="period-alert">19.09.2017</span></div>
                                        <div class="period">Срок оплаты истекает через <span>5 дней!</span></div>
                                        <div class="extend">
                                            <span>Продлить:</span>
                                            <div class="radio-block">
                                                <div class="radio-btn">
                                                <span class="false-radio active">
                                                    <input type="radio" name="period" value="month" id="month" checked="">
                                                </span>
                                                    <label for="month">на месяц <span>(39,35 $)</span></label>
                                                </div>
                                                <div class="radio-btn">
                                                <span class="false-radio">
                                                    <input type="radio" name="period" value="half-year" id="half-year">
                                                </span>
                                                    <label for="half-year">на полгода <span>(249,25 $)</span></label>
                                                </div>
                                                <div class="radio-btn">
                                                <span class="false-radio">
                                                    <input type="radio" name="period" value="3-month" id="month3">
                                                </span>
                                                    <label for="month3">на 3 месяца <span>(119,10 $)</span></label>
                                                </div>
                                                <div class="radio-btn">
                                                <span class="false-radio">
                                                    <input type="radio" name="period" value="year" id="year">
                                                </span>
                                                    <label for="year">на год <span>(249,25 $)</span></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="buttons">
                                            <a href="" class="button pay">Продлить</a>
                                            <a href="" class="button pay-history">история платежей</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="server-character">
                                    <div class="title">Характеристики сервера</div>
                                    <div class="conf-box">
                                        <div>
                                            <i class="zmdi zmdi-memory"></i>
                                            <span>Процессор:<span>Xeon E5-1286v3 / 4.1 ГГц 4 ядра</span></span>
                                        </div>
                                        <div>
                                            <i class="zmdi zmdi-storage"></i>
                                            <span>Диски:<span>960 SSD - 3000 SATA</span></span>
                                        </div>
                                        <div>
                                            <i class="zmdi zmdi-ruler"></i>
                                            <span>Оперативная память:<span>8 Гб</span></span>
                                        </div>
                                        <div>
                                            <i class="mdi mdi-image-filter-center-focus"></i>
                                            <span>RAID / IPMI:<span class="raid on"></span><span class="ipmi"></span></span>
                                        </div>
                                        <div>
                                            <i class="zmdi zmdi-select-all"></i>
                                            <span>Аппаратный контроллер:<span>Adaptec ASR-5405</span></span>
                                        </div>
                                    </div>
                                    <div class="charac-box">
                                        <div>
                                            <span>Имя хоста:</span><span>serv100.ds</span>
                                        </div>
                                        <div>
                                            <span>Шаблон ОС:</span><span>Windows Server 2012 R2 Clean</span>
                                        </div>
                                        <div>
                                            <span>Ip-адрес:</span><span>185.100.222.108</span>
                                        </div>
                                        <div>
                                            <span>Панель управления:</span><span>ISPmanager 5 Lite</span>
                                        </div>
                                        <div>
                                            <span>Нагрузка:</span><span>300 bit/s</span>
                                        </div>
                                        <div>
                                            <span>Администрирование:</span><span>расширенный уровень</span>
                                        </div>
                                        <div>
                                            <span>Трафик:</span><span>Порт 1 Гбит/с, неограниченный трафик по каналу 250 Мбит/с</span>
                                        </div>
                                        <div>
                                            <span>Дополнительные IPv4:</span><span>3 шт.</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="slide-up bottom">
                                    <i class="mdi mdi-chevron-double-up"></i>
                                    <span>Свернуть</span>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>namedomen.ru</td>
                        <td class="conf">
                            <span>Xeon 2xE3-1271v3 / 4.0 ГГц 8 ядер</span>
                            <span>16 Гб RAM / 480 SSD -  2000 SATA</span>
                        </td>
                        <td class="state">
                            <i class="mdi mdi-power-plug-off" data-hint="Порт питания – отключен"></i>
                            <i class="mdi mdi-server-network-off"  data-hint="Порт коммутатора – кабель отключен"></i>
                        </td>
                        <td class="date"><span>22.09.2017</span></td>
                        <td class="price"><span>22$</span> / мес.</td>
                    </tr>
                    <tr class="character" data-id="3">
                        <td colspan="6">
                            <div>
                                <div class="slide-up">
                                    <i class="mdi mdi-chevron-double-up"></i>
                                    <span>Свернуть</span>
                                </div>
                                <div class="actions">
                                    <div class="act power play">
                                        <div>
                                            <i class="mdi mdi-stop"></i>
                                            <i class="mdi mdi-play"></i>
                                        </div>
                                        <span>Питание вкл / выкл</span>
                                    </div>
                                    <div class="act reload">
                                        <div>
                                            <i class="mdi mdi-refresh"></i>
                                        </div>
                                        <span>Перезагрузить</span>
                                    </div>
                                    <div class="act reinstall">
                                        <div>
                                            <i class="mdi mdi-autorenew"></i>
                                        </div>
                                        <span>Переустановить ОС</span>
                                    </div>
                                    <div class="act delete">
                                        <div>
                                            <i class="mdi mdi-delete-forever"></i>
                                        </div>
                                        <span>Удалить сервер</span>
                                    </div>
                                </div>
                                <div class="info">
                                    <div class="state-info">
                                        <div class="title">Состояние:</div>
                                        <div class="state-item power">
                                            <i class="mdi mdi-power-plug"></i>
                                            <span>Порт питания Power1:23-port – <span>включен</span></span>
                                        </div>
                                        <div class="state-item">
                                            <i class="mdi mdi-server-network"></i>
                                            <span>Порт коммутатора 10.10.10.4 Switch 100:FastEthernet0/48 [default] – кабель <span>подключен</span></span>
                                        </div>
                                        <div class="state-item error">
                                            <i class="mdi mdi-alert"></i>
                                            <span>Последняя операция завершилась с <span>ошибкой</span>. Если ошибка повторяется, обратитесь к администратору.</span>
                                        </div>
                                        <div class="buttons">
                                            <a href="" class="button">
                                                <i class="fa fa-comments-o"></i>
                                                <span>Задать вопрос по услуге</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="period-info">
                                        <div class="title">Действует до: <span class="period-alert">19.09.2017</span></div>
                                        <div class="period">Срок оплаты истекает через <span>5 дней!</span></div>
                                        <div class="extend">
                                            <span>Продлить:</span>
                                            <div class="radio-block">
                                                <div class="radio-btn">
                                                <span class="false-radio active">
                                                    <input type="radio" name="period" value="month" id="month" checked="">
                                                </span>
                                                    <label for="month">на месяц <span>(39,35 $)</span></label>
                                                </div>
                                                <div class="radio-btn">
                                                <span class="false-radio">
                                                    <input type="radio" name="period" value="half-year" id="half-year">
                                                </span>
                                                    <label for="half-year">на полгода <span>(249,25 $)</span></label>
                                                </div>
                                                <div class="radio-btn">
                                                <span class="false-radio">
                                                    <input type="radio" name="period" value="3-month" id="month3">
                                                </span>
                                                    <label for="month3">на 3 месяца <span>(119,10 $)</span></label>
                                                </div>
                                                <div class="radio-btn">
                                                <span class="false-radio">
                                                    <input type="radio" name="period" value="year" id="year">
                                                </span>
                                                    <label for="year">на год <span>(249,25 $)</span></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="buttons">
                                            <a href="" class="button pay">Продлить</a>
                                            <a href="" class="button pay-history">история платежей</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="server-character">
                                    <div class="title">Характеристики сервера</div>
                                    <div class="conf-box">
                                        <div>
                                            <i class="zmdi zmdi-memory"></i>
                                            <span>Процессор:<span>Xeon E5-1286v3 / 4.1 ГГц 4 ядра</span></span>
                                        </div>
                                        <div>
                                            <i class="zmdi zmdi-storage"></i>
                                            <span>Диски:<span>960 SSD - 3000 SATA</span></span>
                                        </div>
                                        <div>
                                            <i class="zmdi zmdi-ruler"></i>
                                            <span>Оперативная память:<span>8 Гб</span></span>
                                        </div>
                                        <div>
                                            <i class="mdi mdi-image-filter-center-focus"></i>
                                            <span>RAID / IPMI:<span class="raid on"></span><span class="ipmi"></span></span>
                                        </div>
                                        <div>
                                            <i class="zmdi zmdi-select-all"></i>
                                            <span>Аппаратный контроллер:<span>Adaptec ASR-5405</span></span>
                                        </div>
                                    </div>
                                    <div class="charac-box">
                                        <div>
                                            <span>Имя хоста:</span><span>serv100.ds</span>
                                        </div>
                                        <div>
                                            <span>Шаблон ОС:</span><span>Windows Server 2012 R2 Clean</span>
                                        </div>
                                        <div>
                                            <span>Ip-адрес:</span><span>185.100.222.108</span>
                                        </div>
                                        <div>
                                            <span>Панель управления:</span><span>ISPmanager 5 Lite</span>
                                        </div>
                                        <div>
                                            <span>Нагрузка:</span><span>300 bit/s</span>
                                        </div>
                                        <div>
                                            <span>Администрирование:</span><span>расширенный уровень</span>
                                        </div>
                                        <div>
                                            <span>Трафик:</span><span>Порт 1 Гбит/с, неограниченный трафик по каналу 250 Мбит/с</span>
                                        </div>
                                        <div>
                                            <span>Дополнительные IPv4:</span><span>3 шт.</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="slide-up bottom">
                                    <i class="mdi mdi-chevron-double-up"></i>
                                    <span>Свернуть</span>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>