<?php

use yii\helpers\Html;
use frontend\assets\AppAsset;
use frontend\assets\AdminAsset;
use backend\components\AdminMenu;
use frontend\components\MainMenu;
use frontend\components\FooterMenu;
use frontend\components\LangSwitch;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

if (Yii::$app->user->can('AdminPanel')) {
    AdminAsset::register($this);
}
AppAsset::register($this);
$accountForm = $this->params['accountForm'];
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <!--    <meta name="viewport" content="width=device-width, initial-scale=1.0">-->
    <?php echo Html::csrfMetaTags(); ?>
    <?php $this->registerMetaTag([
        'name' => 'description',
        'content' => $this->description
    ]);
    $this->registerMetaTag([
        'name' => 'keywords',
        'content' => $this->keywords
    ]); ?>
    <title><?php echo Html::encode($this->title); ?></title>
    <?php $this->head() ?>
    <!--    <link href="/favicon.ico" rel="shortcut icon" type="image/x-icon">-->
</head>
<body>
<?php $this->beginBody(); ?>
<!--start wrapper-->
<div class="wrapper <?php echo Yii::$app->user->can('AdminPanel') ? 'admin' : ''; ?>">
    <?php if (Yii::$app->user->can('AdminPanel')) { ?>
        <nav class="admin navbar navbar-inverse" style="border-radius: 0;">
            <div class="navbar-header">
                <div class="pull-right exit">
                    <span><?php echo Yii::$app->user->identity->username && !empty(Yii::$app->user->identity->username) ? 'Здравствуйте, ' . Yii::$app->user->identity->username . '!' : ''; ?></span><br>
                    <a href="<?php echo Url::to(['/logout']); ?>">ВЫХОД</a>
                </div>
                <div class="btn edit btn-info pull-right edit-text">Редактировать</div>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="container">
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <?php echo AdminMenu::widget(); ?>
                    </ul>
                    <div class="pull-right exit">
                        <span><?php echo Yii::$app->user->identity->username && !empty(Yii::$app->user->identity->username) ? 'Здравствуйте, ' . Yii::$app->user->identity->username . '!' : ''; ?></span><br>
                        <a href="<?php echo Url::to(['/logout']); ?>">ВЫХОД</a>
                    </div>
                    <div class="btn edit btn-info pull-right edit-text">Редактировать</div>
                </div>
            </div>
        </nav>
    <?php } ?>
    <div class="enter-form">
        <div class="flex-block">
            <div class="form">
                <div class="title" data-name="enter-1"><?php echo $this->params['texts']['enter-1']['content']; ?></div>
                <?php $form = ActiveForm::begin([
                    'id' => 'account-enter',
                    'enableAjaxValidation' => false,
                    'enableClientValidation' => true,
                    'action' => 'main/enter'
                ]); ?>
                <div class="content">
                    <div class="input">
                        <?php echo $form->field($accountForm, 'email', [
                                'template' => '{error}<i class="fa fa-envelope"></i>{input}'
                            ]
                        )->input('email', [
                            'placeholder' => Yii::t('app', 'email'),
                            'id' => 'enter-email'
                        ]); ?>
                    </div>
                    <div class="input">
                        <?php echo $form->field($accountForm, 'password', [
                                'template' => '{error}<i class="fa fa-key" aria-hidden="true"></i>{input}<i class="fa fa-eye-slash"></i>'
                            ]
                        )->input('password', [
                            'placeholder' => Yii::t('app', 'password'),
                            'id' => 'enter-password'
                        ]); ?>
                    </div>
                    <button class="button" data-name="enter-2"><?php echo $this->params['texts']['enter-2']['content']; ?></button>
                    <a href="<?php echo Url::to(['/request-password-reset']); ?>" data-name="enter-3"><?php echo $this->params['texts']['enter-3']['content']; ?></a>
                </div>
                <?php ActiveForm::end(); ?>
                <div class="sub">
                    <div class="content">
                        <span data-name="enter-4"><?php echo $this->params['texts']['enter-4']['content']; ?></span>
                        <a href="<?php echo Url::to(['/signup']); ?>">
                            <span data-name="enter-5"><?php echo $this->params['texts']['enter-5']['content']; ?></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--start header-->
    <header id="header">
        <div class="width">
            <div class="top">
                <a href="<?php echo Url::to(['/']); ?>" class="logo"></a>
                <div class="description" data-name="header-1" data-id="<?php echo $this->params['texts']['header-1']['id']; ?>"><?php echo $this->params['texts']['header-1']['content']; ?></div>
                <div class="phone">
                    <span data-name="header-2" data-id="<?php echo $this->params['texts']['header-2']['id']; ?>"><?php echo $this->params['texts']['header-2']['content']; ?></span><br>
                    <span>0 800 435 12 14</span>
                </div>
                <div class="enter">
                    <?php echo LangSwitch::widget(); ?>
                    <!--                    <div class="enter-box">-->
                    <?php if (Yii::$app->user->can('account')) { ?>
                        <div class="box-account">
                            <div><i class="zmdi zmdi-account"></i><?php echo ucfirst(Yii::$app->user->identity->username); ?><i class="fa fa-sort-desc"></i>
                                <ul>
                                    <li class="lk"><a href="<?php echo Url::to(['/account']); ?>"><i class="fa fa-sign-in"></i>Личный кабинет</a></li>
                                    <li class="profile"><a href="<?php echo Url::to(['account/edit']); ?>"><i class="fa fa-cog"></i>Настройка профиля</a></li>
                                </ul>
                            </div>
                            <a href="<?php echo Url::to(['/logout']); ?>"><i class="fa fa-sign-out"></i>Выход</a>
                        </div>
                    <?php } else { ?>
                        <div class="enter-box">
                            <a href="<?php echo Url::to(['/signup']); ?>">Регистрация</a>
                            <a id="enter-account">Вход в личный кабинет</a>
                        </div>
                    <?php } ?>
                    <!--                    </div>-->
                </div>
            </div>
            <?php echo MainMenu::widget(); ?>
        </div>
    </header>
    <!--end header-->
    <!--start content-wrapper-->
    <main class="content-wrapper">
        <?php echo $content; ?>
    </main>
    <!--end content-wrapper-->
    <!--start footer-->
    <footer id="footer">
        <div class="top">
            <div class="width">
                <?php echo FooterMenu::widget(); ?>
            </div>
        </div>
        <div class="bottom">
            <div class="width">
                <div class="block soc">
                    <div data-name="footer-1" data-id="<?php echo $this->params['texts']['footer-1']['id']; ?>"><?php echo $this->params['texts']['footer-1']['content']; ?></div>
                    <div class="items">
                        <a href="" class="vk"></a>
                        <a href="" class="fb"></a>
                        <a href="" class="tw"></a>
                    </div>
                </div>
                <div class="block pay">
                    <div data-name="footer-2" data-id="<?php echo $this->params['texts']['footer-2']['id']; ?>"><?php echo $this->params['texts']['footer-2']['content']; ?></div>
                    <div class="items">
                        <div class="visa"></div>
                        <div class="pp"></div>
                        <div class="mc"></div>
                        <div class="webmoney"></div>
                        <div class="qiwi"></div>
                        <div class="alipay"></div>
                    </div>
                    <div class="all">
                        <a href="" data-name="footer-3" data-id="<?php echo $this->params['texts']['footer-3']['id']; ?>"><?php echo $this->params['texts']['footer-3']['content']; ?></a>
                        <span class="fa fa-angle-double-right"></span>
                    </div>
                </div>
                <div class="info">
                    <div class="contacts" data-name="footer-4" data-id="<?php echo $this->params['texts']['footer-4']['id']; ?>"><?php echo $this->params['texts']['footer-4']['content']; ?></div>
                    <a href="" class="developer"><img src="/images/system/codeDEX.svg" alt="Разработчик"></a>
                </div>
            </div>
        </div>
    </footer>
    <!--end footer-->
    <script type="text/javascript">
        $(document).ready(function () {
            $('.admin .edit').click(function () {
                editTexts($(this), '<?php echo Url::to(['main/edit']); ?>');
            });

            $('.enter-form #account-enter').on('beforeSubmit', function() {
                var $this = $(this);
                $this.find('.help-block-error').text('');

                var email = $('#enter-email').val();
                var password = $('#enter-password').val();

                $.ajax({
                    url: '<?php echo Url::to(['main/enter']); ?>',
                    type: 'post',
                    dataType: "json",
                    data: {
                        email: email,
                        password: password,
                        _csrf: yii.getCsrfToken()
                    },
                    success: function (response) {
                        if (response.valid.length != 0) {
                            if (response.valid['accountform-password']) {
                                $this.find('.field-enter-email').find('.help-block-error').text(response.valid['accountform-password'][0]);
                            }
                            if (response.valid['accountform-not_active']) {
                                $this.find('.field-enter-email').find('.help-block-error').html(response.valid['accountform-not_active'][0]);
                            }
                        }
                    },
                    error: function (response) {
                    }
                });
                return false;
            });
        });
    </script>
</div>
<!--end wrapper-->
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
