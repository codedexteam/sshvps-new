<?php

use frontend\components\SliderTop;
use frontend\components\SliderBenefits;
use frontend\components\SliderServer;
use frontend\components\SliderOpinion;
use yii\helpers\Url;

?>
<div id="main">
    <div class="slider-top">
        <div class="width">
            <?php echo SliderTop::widget(); ?>
        </div>
    </div>
    <div class="servers" id="vps">
        <div class="tabs">
            <div class="width">
                <div class="tab">
                    <div class="server active" data-name="main-1" data-id="<?php echo $texts['main-1']['id']; ?>"><?php echo $texts['main-1']['content']; ?></div>
                    <div class="config" data-name="main-2" data-id="<?php echo $texts['main-2']['id']; ?>"><?php echo $texts['main-2']['content']; ?></div>
                </div>
<!--                <a href="" class="button" data-name="main-3">--><?php //echo $texts['main-3']; ?><!--</a>-->
            </div>
        </div>
        <div class="block">
            <div class="width">
                <div class="blocks">
                    <div class="package active">
                        <h2 data-name="main-4" data-id="<?php echo $texts['main-2']['id']; ?>"><?php echo $texts['main-4']['content']; ?></h2>
                        <?php if (!empty($tariffs)) { ?>
                            <?php $traffText = true; ?>
                            <div class="items">
                                <?php foreach ($tariffs as $tariff) { ?>
                                    <div class="<?php echo $tariff->spec ? 'choice' : ''; ?>">
                                        <div class="img"><img src="/images/system/<?php echo $tariff->img; ?>" alt="<?php echo $tariff->langText[0]->title; ?>"></div>
                                        <div class="header">
                                            <div class="title"><?php echo $tariff->langText[0]->title; ?></div>
                                            <div class="price"><?php echo $tariff->price; ?></div>
                                        </div>
                                        <div class="body">
                                            <div class="config">
                                                <div>
                                                    <strong><?php echo $tariff->langText[0]->cpu; ?></strong>
                                                    <span <?php echo $traffText ? 'data-name="main-5" data-id="' . $texts['main-5']['id'] . '"' : ''; ?> ><?php echo $texts['main-5']['content']; ?></span>
                                                </div>
                                                <div>
                                                    <strong><?php echo $tariff->langText[0]->ram; ?></strong>
                                                    <span <?php echo $traffText ? 'data-name="main-6" data-id="' . $texts['main-6']['id'] . '"' : ''; ?>><?php echo $texts['main-6']['content']; ?></span>
                                                </div>
                                                <div>
                                                    <strong><?php echo $tariff->langText[0]->hdd; ?></strong>
                                                    <span <?php echo $traffText ? 'data-name="main-7" data-id="' . $texts['main-7']['id'] . '"' : ''; ?>><?php echo $texts['main-7']['content']; ?></span>
                                                </div>
                                                <div>
                                                    <strong><?php echo $tariff->langText[0]->traff; ?></strong>
                                                    <span <?php echo $traffText ? 'data-name="main-8" data-id="' . $texts['main-8']['id'] . '"' : ''; ?>><?php echo $texts['main-8']['content']; ?></span>
                                                </div>
                                                <div>
                                                    <strong><?php echo $tariff->langText[0]->virtual; ?></strong>
                                                </div>
                                                <?php if (!empty($tariff->vpsOs)) { ?>
                                                <div class="os">
                                                    <?php foreach ($tariff->vpsOs as $osItem) { ?>
                                                        <?php foreach ($osItem->os as $item) { ?>
                                                            <div class="fa fa-<?php echo mb_strtolower($item['title']); ?>"></div>
                                                            <?php } ?>
                                                    <?php } ?>
                                                </div>
                                                <?php } ?>
                                            </div>
                                            <div class="footer">
                                                <a href="<?php echo Url::to(['/vps-order', 'id' => $tariff->id]); ?>" class="button" data-name="main-9"><?php echo $texts['main-9']['content']; ?></a>
                                            </div>
                                        </div>
                                    </div>
                                    <?php $traffText = false; ?>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="slide">
                        <h2 data-name="main-10" data-id="<?php echo $texts['main-10']['id']; ?>"><?php echo $texts['main-10']['content']; ?></h2>
                        <div class="body">
                            <div class="slides">
                                <div class="config-name">
                                    <div class="item" data-name="main-11" data-id="<?php echo $texts['main-11']['id']; ?>"><?php echo $texts['main-11']['content']; ?></div>
                                    <div class="item" data-name="main-12" data-id="<?php echo $texts['main-12']['id']; ?>"><?php echo $texts['main-12']['content']; ?></div>
                                    <div class="item" data-name="main-13" data-id="<?php echo $texts['main-13']['id']; ?>"><?php echo $texts['main-13']['content']; ?></div>
                                    <div class="item" data-name="main-14" data-id="<?php echo $texts['main-14']['id']; ?>"><?php echo $texts['main-14']['content']; ?></div>
                                </div>
                                <div class="config-slide">
                                    <div class="core-item"></div>
                                    <div class="ram-item"></div>
                                    <div class="hard-item"></div>
                                    <div class="ddos-block">
                                        <span class="active" data-name="main-15" data-id="<?php echo $texts['main-15']['id']; ?>"><?php echo $texts['main-15']['content']; ?></span>
                                        <div class="ddos-item">
                                            <div class="mybtn"></div>
                                        </div>
                                        <span data-name="main-16" data-id="<?php echo $texts['main-16']['id']; ?>"><?php echo $texts['main-16']['content']; ?></span>
                                    </div>
                                </div>
                                <div class="config-result">
                                    <div><input type="text" id="core" readonly value="1 ядро по 2 ГГц"></div>
                                    <div><input type="text" id="ram" readonly value="2 Гб"></div>
                                    <div><input type="text" id="hard" readonly value="20 Гб"></div>
                                    <input type="hidden" id="ddos" value="0">
                                </div>
                            </div>
                            <div class="price">
                                <div class="sum" data-name="main-17" data-id="<?php echo $texts['main-17']['id']; ?>"><?php echo $texts['main-17']['content']; ?></div>
                                <div class="links">
                                    <a href="" class="button" data-name="main-18" data-id="<?php echo $texts['main-18']['id']; ?>"><?php echo $texts['main-18']['content']; ?></a>
                                    <a href="" data-name="main-19" data-id="<?php echo $texts['main-19']['id']; ?>"><?php echo $texts['main-19']['content']; ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="servers" id="other-servers">
        <div class="tabs">
            <div class="width">
                <div class="tab">
                    <div class="server active" data-name="main-20" data-id="<?php echo $texts['main-20']['id']; ?>"><?php echo $texts['main-20']['content']; ?></div>
                    <div class="config" data-name="main-21" data-id="<?php echo $texts['main-21']['id']; ?>"><?php echo $texts['main-21']['content']; ?></div>
                </div>
<!--                <a href="" class="button" data-name="main-22">--><?php //echo $texts['main-22']; ?><!--</a>-->
            </div>
        </div>
        <div class="block">
            <div class="width">
                <div class="blocks">
                    <div class="server active">
                        <h2 data-name="main-23" data-id="<?php echo $texts['main-23']['id']; ?>"><?php echo $texts['main-23']['content']; ?></h2>
                        <table>
                            <caption data-name="main-24" data-id="<?php echo $texts['main-24']['id']; ?>"><?php echo $texts['main-24']['content']; ?></caption>
                            <tbody>
                                <tr class="head">
                                    <th></th>
                                    <th data-name="main-25" data-id="<?php echo $texts['main-25']['id']; ?>"><?php echo $texts['main-25']['content']; ?></th>
                                    <th data-name="main-26" data-id="<?php echo $texts['main-26']['id']; ?>"><?php echo $texts['main-26']['content']; ?></th>
                                    <th data-name="main-27" data-id="<?php echo $texts['main-27']['id']; ?>"><?php $texts['main-27']['content']; ?></th>
                                    <th data-name="main-28" data-id="<?php echo $texts['main-28']['id']; ?>"><?php echo $texts['main-28']['content']; ?></th>
                                    <th data-name="main-29" data-id="<?php echo $texts['main-29']['id']; ?>"><?php echo $texts['main-29']['content']; ?></th>
                                    <th data-name="main-30" data-id="<?php echo $texts['main-30']['id']; ?>"><?php echo $texts['main-30']['content']; ?></th>
                                    <th data-name="main-31" data-id="<?php echo $texts['main-31']['id']; ?>"><?php echo $texts['main-31']['content']; ?></th>
                                    <th></th>
                                </tr>
                                <?php if (!empty($servers)) { ?>
                                    <?php foreach ($servers as $server) { ?>
                                        <tr>
                                            <td class="flag"><?php if ($server->location == 'RU') { ?>
                                                    <img src="/images/system/ru.png" alt="russian flag">
                                            <?php } ?>
                                                <?php if ($server->location == 'EN') { ?>
                                                    <img src="/images/system/en.jpg" alt="english flag">
                                                <?php } ?>
                                                <?php if ($server->location == 'DH') { ?>
                                                    <img src="/images/system/dh.png" alt="german flag">
                                                <?php } ?></td>
                                            <td class="processor"><?php echo $server->langText[0]['cpuname']; ?></td>
                                            <td class="ram"><?php echo $server->langText[0]['ram']; ?></td>
                                            <td class="disc"><?php echo $server->langText[0]['hdd']; ?></td>
                                            <td class="raid"><div class="circle <?php echo $server['raid'] ? 'green' : 'grey'; ?>"></div></td>
                                            <td class="irmi"><div class="circle <?php echo $server['ipmi'] ? 'green' : 'grey'; ?>"></div></td>
                                            <td class="traffic"><?php echo $server->langText[0]['localspeed']; ?></td>
                                            <td class="price"><?php echo $server['price']; ?> $</td>
                                            <td class="buy">
                                                <a href="<?php echo Url::to(['/server-order', 'id' => $server->id]); ?>" class="button" data-name="main-32" data-id="<?php echo $texts['main-31']['id']; ?>"><?php echo $texts['main-32']['content']; ?></a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="config">
                        <h2 data-name="main-33" data-name="main-32" data-id="<?php echo $texts['main-33']['id']; ?>"><?php echo $texts['main-33']['content']; ?></h2>
                        <div class="body">
                            <div class="options">
                                <div class="item processor">
                                    <div class="name">
                                        <div class="title" data-name="main-34" data-id="<?php echo $texts['main-34']['id']; ?>" data-id="<?php echo $texts['main-34']['id']; ?>"><?php echo $texts['main-34']['content']; ?></div>
                                        <div class="sub-title" data-name="main-35" data-id="<?php echo $texts['main-35']['id']; ?>"><?php echo $texts['main-35']['content']; ?></div>
                                        <div class="radio-btn">
                                            <div class="mybtn">
                                                <span class="false-radio active"><input type="radio" name="proc" value="Сервер Xeon E3" id="xeonE3" checked></span>
                                                <label for="xeonE3" data-name="main-36" data-id="<?php echo $texts['main-36']['id']; ?>"><?php echo $texts['main-36']['content']; ?></label>
                                            </div>
                                            <div class="mybtn">
                                                <span class="false-radio"><input type="radio" name="proc" value="Сервер Xeon E5" id="xeonE5"></span>
                                                <label for="xeonE5" data-name="main-37" data-id="<?php echo $texts['main-37']['id']; ?>"><?php echo $texts['main-37']['content']; ?></label>
                                            </div>
                                            <div class="mybtn">
                                                <span class="false-radio"><input type="radio" name="proc" value="Сервер Xeon 2xE5" id="xeon2x"></span>
                                                <label for="xeon2x" data-name="main-38" data-id="<?php echo $texts['main-38']['id']; ?>"><?php echo $texts['main-38']['content']; ?></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="sub-name">
                                        <div class="sub-title" data-name="main-39" data-id="<?php echo $texts['main-39']['id']; ?>"><?php echo $texts['main-39']['content']; ?></div>
                                        <div class="items radio-btn xeonE3 active">
                                            <div class="mybtn">
                                                <span class="false-radio"><input type="radio" name="procFreq" value="E3-1231v3" id="freq-1"></span>
                                                <label for="freq-1">
                                                    <span data-name="main-40" data-id="<?php echo $texts['main-40']['id']; ?>"><?php echo $texts['main-40']['content']; ?></span>
                                                    <span data-name="main-41" data-id="<?php echo $texts['main-41']['id']; ?>"><?php echo $texts['main-41']['content']; ?></span>
                                                </label>
                                            </div>
                                            <div class="mybtn">
                                                <span class="false-radio"><input type="radio" name="procFreq" value="E3-1241v3" id="freq-2"></span>
                                                <label for="freq-2">
                                                    <span data-name="main-42" data-id="<?php echo $texts['main-42']['id']; ?>"><?php echo $texts['main-42']['content']; ?></span>
                                                    <span data-name="main-43" data-id="<?php echo $texts['main-43']['id']; ?>"><?php echo $texts['main-43']['content']; ?></span>
                                                </label>
                                            </div>
                                            <div class="mybtn">
                                                <span class="false-radio"><input type="radio" name="procFreq" value="E3-1271v3" id="freq-3"></span>
                                                <label for="freq-3">
                                                    <span data-name="main-44" data-id="<?php echo $texts['main-44']['id']; ?>"><?php echo $texts['main-44']['content']; ?></span>
                                                    <span data-name="main-45" data-id="<?php echo $texts['main-45']['id']; ?>"><?php echo $texts['main-45']['content']; ?></span>
                                                </label>
                                            </div>
                                            <div class="mybtn">
                                                <span class="false-radio"><input type="radio" name="procFreq" value="E3-1280v3" id="freq-4"></span>
                                                <label for="freq-4">
                                                    <span data-name="main-46" data-id="<?php echo $texts['main-46']['id']; ?>"><?php echo $texts['main-46']['content']; ?></span>
                                                    <span data-name="main-47" data-id="<?php echo $texts['main-47']['id']; ?>"><?php echo $texts['main-47']['content']; ?></span>
                                                </label>
                                            </div>
                                            <div class="mybtn">
                                                <span class="false-radio"><input type="radio" name="procFreq" value="E3-1286v3" id="freq-5"></span>
                                                <label for="freq-5">
                                                    <span data-name="main-48" data-id="<?php echo $texts['main-48']['id']; ?>"><?php echo $texts['main-48']['content']; ?></span>
                                                    <span data-name="main-49" data-id="<?php echo $texts['main-49']['id']; ?>"><?php echo $texts['main-49']['content']; ?></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="items radio-btn xeonE5">
                                            <div class="mybtn">
                                                <span class="false-radio"><input type="radio" name="procFreq" value="E3-1231v3" id="xeonE5-1"></span>
                                                <label for="xeonE5-1">
                                                    <span data-name="main-50" data-id="<?php echo $texts['main-50']['id']; ?>"><?php echo $texts['main-50']['content']; ?></span>
                                                    <span data-name="main-51" data-id="<?php echo $texts['main-51']['id']; ?>"><?php echo $texts['main-51']['content']; ?></span>
                                                </label>
                                            </div>
                                            <div class="mybtn">
                                                <span class="false-radio"><input type="radio" name="procFreq" value="E3-1241v3" id="xeonE5-2"></span>
                                                <label for="xeonE5-2">
                                                    <span data-name="main-52" data-id="<?php echo $texts['main-52']['id']; ?>"><?php echo $texts['main-52']['content']; ?></span>
                                                    <span data-name="main-53" data-id="<?php echo $texts['main-53']['id']; ?>"><?php echo $texts['main-53']['content']; ?></span>
                                                </label>
                                            </div>
                                            <div class="mybtn">
                                                <span class="false-radio"><input type="radio" name="procFreq" value="E3-1271v3" id="xeonE5-3"></span>
                                                <label for="xeonE5-3">
                                                    <span data-name="main-54" data-id="<?php echo $texts['main-54']['id']; ?>"><?php echo $texts['main-54']['content']; ?></span>
                                                    <span data-name="main-55" data-id="<?php echo $texts['main-55']['id']; ?>"><?php echo $texts['main-55']['content']; ?></span>
                                                </label>
                                            </div>
                                            <div class="mybtn">
                                                <span class="false-radio"><input type="radio" name="procFreq" value="E3-1280v3" id="xeonE5-4"></span>
                                                <label for="xeonE5-4">
                                                    <span data-name="main-56" data-id="<?php echo $texts['main-56']['id']; ?>"><?php echo $texts['main-56']['content']; ?></span>
                                                    <span data-name="main-57" data-id="<?php echo $texts['main-57']['id']; ?>"><?php echo $texts['main-57']['content']; ?></span>
                                                </label>
                                            </div>
                                            <div class="mybtn">
                                                <span class="false-radio"><input type="radio" name="procFreq" value="E3-1286v3" id="freq-5"></span>
                                                <label for="freq-5">
                                                    <span data-name="main-58" data-id="<?php echo $texts['main-58']['id']; ?>"><?php echo $texts['main-58']['content']; ?></span>
                                                    <span data-name="main-59" data-id="<?php echo $texts['main-59']['id']; ?>"><?php echo $texts['main-59']['content']; ?></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="items radio-btn xeon2x">
                                            <div class="mybtn">
                                                <span class="false-radio"><input type="radio" name="procFreq" value="E3-1231v3" id="xeon2x-1"></span>
                                                <label for="xeon2x-1">
                                                    <span data-name="main-60" data-id="<?php echo $texts['main-60']['id']; ?>"><?php echo $texts['main-60']['content']; ?></span>
                                                    <span data-name="main-61" data-id="<?php echo $texts['main-61']['id']; ?>"><?php echo $texts['main-61']['content']; ?></span>
                                                </label>
                                            </div>
                                            <div class="mybtn">
                                                <span class="false-radio"><input type="radio" name="procFreq" value="E3-1241v3" id="xeon2x-2"></span>
                                                <label for="xeon2x-2">
                                                    <span data-name="main-62" data-id="<?php echo $texts['main-62']['id']; ?>"><?php echo $texts['main-62']['content']; ?></span>
                                                    <span data-name="main-63" data-id="<?php echo $texts['main-63']['id']; ?>"><?php echo $texts['main-63']['content']; ?></span>
                                                </label>
                                            </div>
                                            <div class="mybtn">
                                                <span class="false-radio"><input type="radio" name="procFreq" value="E3-1271v3" id="xeon2x-3"></span>
                                                <label for="xeon2x-3">
                                                    <span data-name="main-64" data-id="<?php echo $texts['main-64']['id']; ?>"><?php echo $texts['main-64']['content']; ?></span>
                                                    <span data-name="main-65" data-id="<?php echo $texts['main-65']['id']; ?>"><?php echo $texts['main-65']['content']; ?></span>
                                                </label>
                                            </div>
                                            <div class="mybtn">
                                                <span class="false-radio"><input type="radio" name="procFreq" value="E3-1280v3" id="xeon2x-4"></span>
                                                <label for="xeon2x-4">
                                                    <span data-name="main-66" data-id="<?php echo $texts['main-66']['id']; ?>"><?php echo $texts['main-66']['content']; ?></span>
                                                    <span data-name="main-67" data-id="<?php echo $texts['main-67']['id']; ?>"><?php echo $texts['main-67']['content']; ?></span>
                                                </label>
                                            </div>
                                            <div class="mybtn">
                                                <span class="false-radio"><input type="radio" name="procFreq" value="E3-1286v3" id="xeon2x-5"></span>
                                                <label for="xeon2x-5">
                                                    <span data-name="main-68" data-id="<?php echo $texts['main-68']['id']; ?>"><?php echo $texts['main-68']['content']; ?></span>
                                                    <span data-name="main-69" data-id="<?php echo $texts['main-69']['id']; ?>"><?php echo $texts['main-69']['content']; ?></span>
                                                </label>
                                            </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="item hdd">
                                    <div class="title" data-name="main-70" data-id="<?php echo $texts['main-70']['id']; ?>"><?php echo $texts['main-70']['content']; ?></div>
                                    <div class="items">
                                        <div class="check-block">
                                            <div class="check-btn">
                                                <span class="false-checkbox active"><input type="checkbox" name="hdd1" checked id="hdd1" disabled></span>
                                                <label for="hdd1" data-name="main-71" data-id="<?php echo $texts['main-71']['id']; ?>"><?php echo $texts['main-71']['content']; ?></label>
                                            </div>
                                            <select name="diskNameHdd1">
                                                <option data-type="none" value="none" selected>Выберите диск</option>
                                                <option data-type="sata" value="SATA 1000 Гб">SATA 1000 Гб</option>
                                                <option data-type="sata" value="SATA 2000 Гб">SATA 2000 Гб</option>
                                                <option data-type="sata" value="SATA 3000 Гб">SATA 3000 Гб</option>
                                                <option data-type="sata" value="SATA 4000 Гб">SATA 4000 Гб</option>
                                                <option data-type="ssd" value="SSD 240 Гб">SSD 240 Гб</option>
                                                <option data-type="ssd" value="SSD 480 Гб">SSD 480 Гб</option>
                                                <option data-type="ssd" value="SSD 960 Гб">SSD 960 Гб</option>
                                            </select>
                                        </div>
                                        <div class="check-block">
                                            <div class="check-btn">
                                                <span class="false-checkbox"><input type="checkbox" name="hdd2" id="hdd2"></span>
                                                <label for="hdd2" data-name="main-72" data-id="<?php echo $texts['main-72']['id']; ?>"><?php echo $texts['main-72']['content']; ?></label>
                                            </div>
                                            <select name="diskNameHdd2" disabled>
                                                <option data-type="none" value="none" selected>Выберите диск</option>
                                                <option data-type="sata" value="SATA 1000 Гб">SATA 1000 Гб</option>
                                                <option data-type="sata" value="SATA 2000 Гб">SATA 2000 Гб</option>
                                                <option data-type="sata" value="SATA 3000 Гб">SATA 3000 Гб</option>
                                                <option data-type="sata" value="SATA 4000 Гб">SATA 4000 Гб</option>
                                                <option data-type="ssd" value="SSD 240 Гб">SSD 240 Гб</option>
                                                <option data-type="ssd" value="SSD 480 Гб">SSD 480 Гб</option>
                                                <option data-type="ssd" value="SSD 960 Гб">SSD 960 Гб</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="item ram">
                                    <div class="title" data-name="main-73" data-id="<?php echo $texts['main-73']['id']; ?>"><?php echo $texts['main-73']['content']; ?></div>
                                    <div class="items radio-block">
                                        <div class="radio-btn">
                                            <span class="false-radio active"><input type="radio" name="ram" value="8 Гб" id="ram8" checked></span>
                                            <label for="ram8">8 Гб</label>
                                        </div>
                                        <div class="radio-btn">
                                            <span class="false-radio"><input type="radio" name="ram" value="16 Гб" id="ram16"></span>
                                            <label for="ram16">16 Гб</label>
                                        </div>
                                        <div class="radio-btn">
                                            <span class="false-radio"><input type="radio" name="ram" value="32 Гб" id="ram32"></span>
                                            <label for="ram32">32 Гб</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="item control">
                                    <div class="title"><span class="ico zmdi zmdi-select-all"></span>Аппаратный контроллер</div>
                                    <div class="items radio-block">
                                        <div class="radio-btn">
                                            <span class="false-radio active"><input type="radio" name="control" value="Без контроллера" id="none-contr" checked></span>
                                            <label for="none-contr">Без контроллера</label>
                                        </div>
                                        <div class="radio-btn">
                                            <span class="false-radio"><input type="radio" name="control" value="Adaptec ASR-5405" id="adaptec"></span>
                                            <label for="adaptec">Adaptec ASR-5405</label>
                                        </div>
                                        <div class="radio-btn">
                                            <span class="false-radio"><input type="radio" name="ram" value="LSI NMR8100-4i" id="lsi"></span>
                                            <label for="lsi">LSI NMR8100-4i</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="result">
                                <figure class="main-pic"><img src="/images/system/nastroika-servera.png" alt="процессор"></figure>
                                <div class="title">Конфигурация сервера:</div>
                                <div class="res">
                                    <div class="item processor">
                                        <span class="name">Процессор</span>
                                        <span class="value">Не выбрано</span>
                                        <figure><img src="/images/system/CPU.png" alt="processor"></figure>
                                    </div>
                                    <div class="item ram">
                                        <span class="name">Оперативная память</span>
                                        <span class="value">8 Гб</span>
                                        <figure><img src="/images/system/ram.png" alt="ram"></figure>
                                    </div>
                                    <div class="item hdd">
                                        <span class="name">Жесткий диск HDD</span>
                                        <span class="value">Не выбрано</span>
                                        <figure><img src="/images/system/hdd.png" alt="hdd"></figure>
                                    </div>
                                    <div class="item ssd">
                                        <span class="name">Жесткий диск SSD</span>
                                        <span class="value">Не выбрано</span>
                                        <figure><img src="/images/system/ssd.png" alt="ssd"></figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="price-block">
                            <div class="price">
                                <span>Цена</span>
                                <span class="tarif">12$</span>
                                <span>За 1 месяц</span>
                            </div>
                            <div class="abonent">
                                <div>
                                    <span class="false-radio"><input type="radio" value="За 1 год" name="abonYear" id="oneYear"></span>
                                    <label for="oneYear">За 1 год<span>8$ в месяц</span></label>
                                </div>
                                <div>
                                    <span class="false-radio"><input type="radio" value="За полгода" name="abonYear" id="6month"></span>
                                    <label for="6month">За полгода<span>9$ в месяц</span></label>
                                </div>
                                <div>
                                    <span class="false-radio"><input type="radio" value="За 3 месяца" name="abonYear" id="3monyh"></span>
                                    <label for="3monyh">За 3 месяца<span>10$ в месяц</span></label>
                                </div>
                                <div>
                                    <span class="false-radio active"><input type="radio" value="За 1 месяц" name="abonYear" id="oneMonth"></span>
                                    <label for="oneMonth">За 1 месяц<span>12$ в месяц</span></label>
                                </div>
                                <span>При заказе сервера от 15000 ₽, администрирование в подарок!</span>
                            </div>
                            <a href="" class="button">Заказать сервер</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="benefits">
        <div class="width">
            <div>
                <span>99%</span><br>
                <span>аптайм</span>
            </div>
            <div>
                <span>401</span><br>
                <span>клиент</span>
            </div>
            <div>
                <span>8</span><br>
                <span>лет на рынке</span>
            </div>
            <div>
                <span>10%</span><br>
                <span>скидка на всё</span>
            </div>
        </div>
    </div>
    <div class="more-benefits">
<!--        <div class="width">-->
            <h3>Наши преимущества</h3>
            <?php echo SliderBenefits::widget(); ?>
<!--        </div>-->
    </div>
    <div class="server-block">
        <?php echo SliderServer::widget(); ?>
    </div>
    <div class="opinion-block">
        <?php echo SliderOpinion::widget(); ?>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.slider-top .slider').HbKSlider({
            sliderSize: 1,
            autoPlay: true,
            overStop: true,
            navigationArrows: false,
            navigationRadioButtons: false,
            sliderSpeed: 5500,
            animation: 'fade'
        });
        $('.more-benefits .slider').HbKSlider({
            sliderSize: 3,
            autoPlay: false,
            overStop: true,
            imageSize: 1120,
            navigationArrows: true,
            navigationRadioButtons: true,
            sliderSpeed: 8500,
            animationSpeed: 400,
            animation: 'carousel'
        });
        $('.server-block .slider').HbKSlider({
            sliderSize: 1,
            autoPlay: false,
            overStop: true,
            navigationArrows: true,
            navigationRadioButtons: false,
            sliderSpeed: 8500,
            animation: 'carousel'
        });
        $('.opinion-block .slider').HbKSlider({
            sliderSize: 3,
            autoPlay: false,
            overStop: true,
            navigationArrows: true,
            navigationRadioButtons: false,
            sliderSpeed: 8500,
            animationSpeed: 600,
            animation: 'carousel'
        });

        $('#vps .slide .core-item').slider({
            range: "min",
            value: 1,
            min: 1,
            max: 8,
            slide: function( event, ui ) {
                if (ui.value == 1) {
                    $( "#core" ).val(ui.value + ' ядро по 2 ГГц');
                }
                else if (ui.value == 2 || ui.value == 3 || ui.value == 4) {
                    $( "#core" ).val(ui.value + ' ядра по 2 ГГц');
                } else {
                    $( "#core" ).val(ui.value + ' ядер по 2 ГГц');
                }
                sumSlide();
            }
        });
        $('#vps .slide .ram-item').slider({
            range: "min",
            value: 2,
            min: 2,
            max: 32,
            step: 2,
            slide: function( event, ui ) {
                $( "#ram" ).val(ui.value + ' Гб');
                sumSlide();
            }
        });
        $('#vps .slide .hard-item').slider({
            range: "min",
            value: 20,
            min: 20,
            max: 200,
            step: 10,
            slide: function( event, ui ) {
                $( "#hard" ).val(ui.value + ' Гб');
                sumSlide();
            }
        });
    });
</script>
