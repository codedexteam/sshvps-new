<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-request-password-reset">
    <h1 data-name="reset-pass-1"><?php echo $texts['reset-pass-1']['content']; ?></h1>
    <?php if (Yii::$app->session->has('success')) { ?>
        <p><?php echo Yii::$app->session['success']; ?></p>
        <?php Yii::$app->session->remove('success'); ?>
    <?php }
    elseif (Yii::$app->session->has('error')) { ?>
        <p><?php echo Yii::$app->session['error']; ?></p>
        <?php Yii::$app->session->remove('error'); ?>
        <?php } else { ?>

    <p data-name="reset-pass-2"><?php echo $texts['reset-pass-2']['content']; ?></p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

                <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

                <div class="form-group">
                    <?= Html::submitButton($texts['reset-pass-3']['content'], [
                        'class' => 'btn btn-primary',
                        'data-name' => 'reset-pass-3'
                    ]) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
    <?php } ?>
</div>
