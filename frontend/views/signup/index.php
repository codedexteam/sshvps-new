<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div id="reg-form" class="signup">
    <header>
        <div class="width">
            <h1 data-name="signup-1"><?php echo $texts['signup-1']['content']; ?></h1>
            <p>
                <span data-name="signup-2"><?php echo $texts['signup-2']['content']; ?></span>
                <a href="<?php echo Url::to(['/account']); ?>"><span data-name="signup-15"><?php echo $texts['signup-15']['content']; ?></span></a>
            </p>
        </div>
    </header>
    <div class="reg-block">
        <div class="width">
            <div class="form">
                <?php $form = ActiveForm::begin([
                    'id' => 'signup',
                    'enableClientValidation' => false,
                ]); ?>
                <div class="form-block">
                    <div class="title" data-name="signup-3" data-id=""><?php echo $texts['signup-3']['content']; ?></div>
                    <div class="inputs">
                        <?php echo $form->field($model, 'name', [
                                'template' => '{error}<label>
                                <span class="name">' . Yii::t('app', 'name') . ':</span>
                                <span class="input">
                                    <i class="fa fa-user-circle-o"></i>
                                    {input}
                                </span>
                            </label>']
                        )->input('text'); ?>
                        <?php echo $form->field($model, 'lastname', [
                                'template' => '{error}<label>
                                <span class="name">' . Yii::t('app', 'lastname') . ':</span>
                                <span class="input">
                                    <i class="fa fa-user-circle"></i>
                                    {input}
                                </span>
                            </label>']
                        )->input('text'); ?>
                        <?php echo $form->field($model, 'email', [
                                'template' => '{error}<label>
                                <span class="name">' . Yii::t('app', 'email') . ':</span>
                                <span class="input">
                                    <i class="fa fa-envelope-square"></i>
                                    {input}
                                </span>
                            </label>']
                        )->input('email'); ?>
                        <?php echo $form->field($model, 'password', [
                                'template' => '{error}<label>
                                <span class="name">' . Yii::t('app', 'password') . ':</span>
                                <span class="input">
                                    <i class="fa fa-lock"></i>
                                    {input}
                                    <i class="fa fa-eye-slash"></i>
                                </span>
                            </label>']
                        )->input('password'); ?>
                        <?php echo $form->field($model, 'password_repeat', [
                                'template' => '{error}<label>
                                <span class="name">' . Yii::t('app', 'password_repeat') . ':</span>
                                <span class="input">
                                    <i class="fa fa-lock"></i>
                                    {input}
                                    <i class="fa fa-eye-slash"></i>
                                </span>
                            </label>']
                        )->input('password'); ?>
                        <div class="sub" data-name="signup-4" data-id=""><?php echo $texts['signup-4']['content']; ?></div>
                    </div>
                </div>
                <div class="form-block">
                    <div class="title" data-name="signup-5" data-id=""><?php echo $texts['signup-5']['content']; ?></div>
                    <div class="inputs">
                        <?php echo $form->field($model, 'phone', [
                                'template' => '{error}<label>
                                <span class="name">' . Yii::t('app', 'phone') . ':</span>
                                <span class="input phone-mask">
                                    <i class="fa fa-phone"></i>
                                    {input}
                                </span>
                            </label>']
                        )->input('text'); ?>
                        <?php echo $form->field($model, 'country', [
                                'template' => '{error}<label>
                                <span class="name" >' . Yii::t('app', 'country') . ':</span>
                                <span class="input">
                                    <i class="fa fa-globe"></i>
                                    {input}
                                </span>
                            </label>']
                        )->input('text'); ?>
                        <?php echo $form->field($model, 'address', [
                                'template' => '{error}<label>
                                <span class="name">' . Yii::t('app', 'address') . ':</span>
                                <span class="input">
                                    <i class="fa fa-map-marker"></i>
                                    {input}
                                </span>
                            </label>']
                        )->input('text'); ?>
                        <?php echo $form->field($model, 'agree', [
                            'template' => '{error}<label>
                                <div class="check">{input}</div>
                                <span class="agree" data-name="signup-6" data-id="">' . $texts['signup-6']['content'] . '</span>
                            </label>',
                        ])->input('checkbox', [
                            'class' => 'checkbox',
                        ])->label('Активность'); ?>
                        <?php echo Html::submitButton($texts['signup-7']['content'], [
                            'class' => 'button',
                            'data-name' => 'signup-7',
                            'data-id' => '',
                            'disabled' => true
                        ]) ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>