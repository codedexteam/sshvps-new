<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div id="confirm" class="signup">
    <header>
        <div class="width">
            <h1 data-name="signup-1"><?php echo $texts['signup-1']['content']; ?></h1>
            <p>
                <span data-name="signup-2"><?php echo $texts['signup-2']['content']; ?></span>
                <a href="<?php echo Url::to(['/account']); ?>"><span data-name="signup-15"><?php echo $texts['signup-15']['content']; ?></span></a>
            </p>
        </div>
    </header>
    <div class="reg-block">
        <div class="width">
            <div class="title" data-name="signup-8" data-id=""><?php echo $texts['signup-8']['content']; ?></div>
            <p data-name="signup-9" data-id=""><?php echo $texts['signup-9']['content']; ?></p>
            <p data-name="signup-10" data-id=""><?php echo $texts['signup-10']['content']; ?></p>
            <?php if (!empty($repeat)) { ?>
                <span class="repeat"><?php echo $repeat; ?></span>
            <?php } ?>
            <?php $form = ActiveForm::begin([
                'id' => 'repeat',
                'enableClientValidation' => false,
            ]); ?>
            <?php echo Html::submitButton($texts['signup-11']['content'], [
                'class' => 'button',
                'data-name' => 'signup-11',
                'data-id' => '',
            ]) ?>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>