<?php
use yii\helpers\Url;
?>
<div id="success" class="signup">
    <header>
        <div class="width">
            <h1 data-name="signup-1"><?php echo $texts['signup-1']['content']; ?></h1>
            <p>
                <span data-name="signup-2"><?php echo $texts['signup-2']['content']; ?></span>
                <a href="<?php echo Url::to(['/account']); ?>"><span data-name="signup-15"><?php echo $texts['signup-15']['content']; ?></span></a>
            </p>
        </div>
    </header>
    <div class="reg-block">
        <div class="width">
            <?php if (empty($error)) {?>
                <div class="title" data-name="signup-12" data-id=""><?php echo $texts['signup-12']['content']; ?></div>
                <a href="/" class="button" data-name="signup-13" data-id=""><?php echo $texts['signup-13']['content']; ?></a>
            <?php } else { ?>
                <?php foreach ($error as $er) { ?>
                    <p class="error"><?php echo $er; ?></p>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
</div>