<?php
use yii\helpers\StringHelper;
use yii\helpers\Url;
use common\models\Option;
?>
<div class="order server-order">
    <header>
        <div class="width">
            <h1 data-name="order-server-1"><?php echo $texts['order-server-1']['content']; ?></h1>
            <div class="tabs">
                <div class="active params"><span>1</span><span data-name="order-server-2"><?php echo $texts['order-server-2']['content']; ?></span></div>
                <div class="enter"><span>2</span><span data-name="order-server-3"><?php echo $texts['order-server-3']['content']; ?></span></div>
                <div class="pay"><span>3</span><span data-name="order-server-4"><?php echo $texts['order-server-4']['content']; ?></span></div>
            </div>
        </div>
    </header>
    <?php if (!empty($server)) {?>
        <div class="order-block">
            <div class="width">
                <form action="<?php echo Url::toRoute('server-order/pay'); ?>" id="order-server" method="post">
                    <input type="hidden" name="_csrf-frontend" value="<?php echo Yii::$app->request->getCsrfToken(); ?>">
                    <section class="params-block">
                        <header data-name="order-server-5"><?php echo $texts['order-server-5']['content']; ?></header>
                        <div class="options-block">
                            <?php if (!empty($os)) {?>
                                <?php $first = true; ?>
                                <div class="option" data-option="os">
                                    <div class="select">
                                        <span class="title"><i class="fa fa-windows"></i><span>Операционная система</span></span>
                                        <div class="select-box">
                                            <ul>
                                                <?php foreach ($os as $val) { ?>
                                                    <?php foreach ($val->elem as $item) { ?>
                                                        <?php if ($first) {
                                                            $firstId = (array)$item->id;
                                                            $firstOsId = $firstId['$'];
                                                            $firstName = (array)$item->name;
                                                            $firstOSName = $firstName['$'];
                                                            $first = false;
                                                        } ?>
                                                        <?php $id = (array)$item->id; ?>
                                                        <?php $name = (array)$item->name; ?>
                                                        <li data-id="<?php echo $id['$']; ?>">
                                                            <span><?php echo $name['$']; ?></span>
                                                            <span>Бесплатно</span>
                                                        </li>
                                                    <?php } ?>
                                                <?php } ?>
                                            </ul>
                                            <div class="checked"><?php echo $firstOSName; ?></div>
                                            <input type="hidden" value="<?php echo $firstOsId; ?>" name="os">
                                        </div>
                                    </div>
                                    <p>Установим операционную систему из списка автоматически. Для установки другой операционной системы обратитесь в службу поддержки или к онлайн-помощнику.</p>
                                </div>
                            <?php } ?>
                            <div class="option" data-option="panel-manager">
                                <div class="select">
                                    <span class="title"><i class="fa fa-sliders"></i><span>Панель управления</span></span>
                                    <div class="select-box">
                                        <ul>
                                            <li data-id="1"><span>ISPmanager Lite1</span></li>
                                            <li data-id="2"><span>ISPmanager Lite2</span></li>
                                            <li data-id="3"><span>ISPmanager Lite3</span></li>
                                            <li data-id="4"><span>ISPmanager Lite4</span></li>
                                        </ul>
                                        <div class="checked">ISPmanager Lite1</div>
                                        <input type="hidden" value="1" name="panel">
                                    </div>
                                </div>
                                <p>Используйте максимум инструментов для управления вашими серверами. Оцените коммерческое программное обеспечение одного из ведущих мировых производителей в этой области — компании ISPsystem.</p>
                            </div>
                            <?php if (!empty($options)) {?>
                                <?php $label = 1;?>
                                <?php foreach ($options as $option) {?>
                                    <?php if ($option->option_type == Option::OPTION_TYPE_RADIO) { ?>
                                <div class="option option-radio" data-option="<?php echo $option->name; ?>">
                                    <div class="select">
                                        <span class="title"><i class="fa <?php echo $option->icon; ?>" aria-hidden="true"></i><?php echo $option->langOption[0]['title']; ?></span>
                                        <div class="radio-block">
                                            <?php if (!empty($option->childItems)) {?>
                                                <?php foreach ($option->childItems as $child) {?>
                                                    <div class="radio-btn">
                                                        <span class="false-radio <?php echo $child->is_checked ? 'active' : ''; ?>">
                                                            <input type="radio" name="<?php echo $option->name; ?>" value="<?php echo $child->id; ?>" id="radio<?php echo $label; ?>" data-title="<?php echo $child->langOption[0]['title']; ?>" <?php echo $child->is_checked ? 'checked' : ''; ?>>
                                                        </span>
                                                        <label for="radio<?php echo $label; ?>"><?php echo $child->langOption[0]['title']; ?><?php echo $child->price ? ' <span>(+' . number_format($child->price, 2) . ' $/мес.)</span>' : ''; ?></label>
                                                    </div>
                                                    <?php $label++; ?>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <p><?php echo $option->langOption[0]['description']; ?></p>
                                </div>
                                    <?php } ?>
                                    <?php if ($option->option_type == Option::OPTION_TYPE_LIST) { ?>
                                    <div class="option" data-option="<?php echo $option->name; ?>">
                                        <div class="select">
                                            <span class="title"><i class="fa <?php echo $option->icon; ?>"></i><span data-name="order-server-8"><?php echo $option->langOption[0]['title']; ?></span></span>
                                            <div class="select-box">
                                                <?php if (!empty($option->childItems)) {?>
                                                    <?php $checked['title'] = $option->childItems[0]->langOption[0]['title']; ?>
                                                    <?php $checked['id'] = $option->childItems[0]->id; ?>
                                                <ul>
                                                    <?php foreach ($option->childItems as $child) {?>
                                                        <?php if ($child->is_checked) {
                                                            $checked['title'] = $child->langOption[0]['title'];
                                                            $checked['id'] = $child->id;
                                                         } ?>
                                                        <li data-id="<?php echo $child->id; ?>"><span><?php echo $child->langOption[0]['title']; ?></span></li>
                                                    <?php } ?>
                                                </ul>
                                                <div class="checked"><?php echo $checked['title']; ?></div>
                                                <input type="hidden" value="<?php echo $checked['id']; ?>" name="<?php echo $option->name; ?>">
                                                <?php } ?>
                                            </div>
                                        </div>
<!--                                        <p data-name="order-server-9">--><?php //echo $texts['order-server-9']['content']; ?><!--</p>-->
                                        <p><?php echo $option->langOption[0]['description']; ?></p>
                                    </div>
                                    <?php } ?>
                                    <?php if ($option->option_type == Option::OPTION_TYPE_SLIDE) { ?>
                                        <div class="option option-slide" data-option="<?php echo $option->name; ?>">
                                            <div class="select">
                                                <span class="title"><i class="fa <?php echo $option->icon; ?>"></i><?php echo $option->langOption[0]['title']; ?></span>
                                                <div class="slide">
                                                    <input type="text" class="res"  value="<?php echo $option->min; ?>" name="<?php echo $option->name; ?>" readonly="readonly">
                                                    <div data-option="<?php echo $option->name; ?>"></div>
                                                </div>
                                            </div>
                                            <p><?php echo $option->langOption[0]['description']; ?></p>
                                        </div>
                                        <script type="text/javascript">
                                            $(document).ready(function() {
                                                $('.option div[data-option="<?php echo $option->name; ?>"]').slider({
                                                    range: "min",
                                                    value: <?php echo $option->min; ?>,
                                                    min: <?php echo $option->min; ?>,
                                                    max: <?php echo $option->max; ?>,
                                                    step: 2,
                                                    slide: function(event, ui) {
                                                        $('.option[data-option="<?php echo $option->name; ?>"] input').val(ui.value);
                                                        $('.config .params span.<?php echo $option->name; ?>').text(ui.value + ' IP');
                                                    }
                                                });
                                            });
                                        </script>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                            <div class="option domen">
                                <div class="select">
                                    <span data-name="order-server-17"><?php echo $texts['order-server-17']['content']; ?></span>
                                    <div class="input">
                                        <input type="text" value="" name="domen">
                                        <i class="fa fa-question-circle-o"></i>
                                        <div class="description" data-name="order-server-18"><?php echo $texts['order-server-18']['content']; ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="config">
                        <header data-name="order-server-19"><?php echo $texts['order-server-19']['content']; ?></header>
                        <div class="conf-block">
                            <div class="block">
                                <div class="title" data-name="order-server-20"><?php echo $texts['order-server-20']['content']; ?></div>
                                <div class="params">
                                    <input type="hidden" name="server_id" value="<?php echo $server->id; ?>">
                                    <div class="item">
                                        <span data-name="order-server-21"><?php echo $texts['order-server-21']['content']; ?></span>
                                        <span><?php echo $server->langText[0]['cpuname']; ?></span>
                                    </div>
                                    <div class="item">
                                        <span data-name="order-server-22"><?php echo $texts['order-server-22']['content']; ?></span>
                                        <span><?php echo $server->langText[0]['ram']; ?></span>
                                    </div>
                                    <div class="item">
                                        <span data-name="order-server-23"><?php echo $texts['order-server-23']['content']; ?></span>
                                        <span><?php echo $server->langText[0]['hdd']; ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="block">
                                <div class="title" data-name="order-server-25"><?php echo $texts['order-server-25']['content']; ?></div>
                                <div class="params">
                                    <div class="item">
                                        <span>Операционная система</span>
                                        <span class="os"><?php echo strip_tags(StringHelper::truncate($firstOSName, 30, '')); ?></span>
                                    </div>
                                    <div class="item">
                                        <span>Панель управления</span>
                                        <span class="panel-manager">SPmanager Lite</span>
                                    </div>
                                    <?php if (!empty($options)) {?>
                                        <?php foreach ($options as $val) {?>
                                            <?php if ($val->option_type == Option::OPTION_TYPE_RADIO || $val->option_type == Option::OPTION_TYPE_LIST) { ?>
                                                <div class="item">
                                                    <span><?php echo $val->langOption[0]['title']; ?></span>
                                                    <?php if (!empty($val->childItems)) {?>
                                                        <?php foreach ($val->childItems as $item) {?>
                                                            <?php if ($item->is_checked) {?>
                                                                <span class="<?php echo $val->name; ?>"><?php echo StringHelper::truncate($item->langOption[0]['title'], 30, ''); ?></span>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </div>
                                            <?php } ?>
                                            <?php if ($val->option_type == Option::OPTION_TYPE_SLIDE) { ?>
                                                <div class="item">
                                                    <span><?php echo $val->langOption[0]['title']; ?></span>
                                                    <span class="<?php echo $val->name; ?>"><?php echo $val->min; ?> IP</span>
                                                </div>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="price-block">
                                <div class="price">254.25$</div>
                                <div class="radio-block">
                                    <div class="radio-btn">
                                        <span class="false-radio active">
                                            <input type="radio" name="period" value="month" id="month" checked>
                                        </span>
                                        <label for="month">за месяц <span>(39,35 $/мес.)</span></label>
                                    </div>
                                    <div class="radio-btn">
                                        <span class="false-radio">
                                            <input type="radio" name="period" value="3-month" id="month3">
                                        </span>
                                        <label for="month3">за 3 месяца <span>(119,10 $/мес.)</span></label>
                                    </div>
                                    <div class="radio-btn">
                                        <span class="false-radio">
                                            <input type="radio" name="period" value="half-year" id="half-year">
                                        </span>
                                        <label for="half-year">за полгода <span>(249,25 $/мес.)</span></label>
                                    </div>
                                    <div class="radio-btn">
                                        <span class="false-radio">
                                            <input type="radio" name="period" value="year" id="year">
                                        </span>
                                        <label for="year">за год <span>(249,25 $/мес.)</span></label>
                                    </div>
                                    <button class="button" type="submit" data-name="order-server-31"><?php echo $texts['order-server-31']['content']; ?></button>
                                </div>
                                <figure><img src="/images/system/panda.png" alt="panda"></figure>
                                <p><span data-name="order-server-32"><?php echo $texts['order-server-32']['content']; ?></span> <a href=""><span data-name="order-server-33"><?php echo $texts['order-server-33']['content']; ?></span></a></p>
                            </div>
                        </div>
                    </section>
                </form>
            </div>
        </div>
    <?php } else { ?>
        <p>Вы ничего не выбрали</p>
    <?php } ?>
</div>
<script type="text/javascript">
    //close select
    $(document).click(function(e){
        var className = e.target.className;
        if (className.indexOf('checked') == -1) {
            var elem =  $('.order .select-box ul');
            if (e.target != elem[0] && !elem.has(e.target).length) {
                elem.hide();
                $('.order .select-box .checked').removeClass('active');
            }
        }
        if (className.indexOf('fa-question-circle-o') == -1) {
            var elemDesc =  $('.order .option .description');
            if (e.target != elemDesc[0] && !elemDesc.has(e.target).length) {
                elemDesc.hide();
            }
        }
    });
</script>