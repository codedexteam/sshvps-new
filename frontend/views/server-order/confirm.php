<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
?>
<div class="order order-enter">
    <header>
        <div class="width">
            <h1 data-name="order-server-1"><?php echo $texts['order-server-1']['content']; ?></h1>
            <div class="tabs">
                <div class="params"><span>1</span><span data-name="order-server-2"><?php echo $texts['order-server-2']['content']; ?></span></div>
                <div class="enter active"><span>2</span><span data-name="order-server-3"><?php echo $texts['order-server-3']['content']; ?></span></div>
                <div class="pay"><span>3</span><span data-name="order-server-4"><?php echo $texts['order-server-4']['content']; ?></span></div>
            </div>
        </div>
    </header>
</div>
<div id="confirm" class="signup">
       <div class="reg-block">
        <div class="width">
            <div class="title" data-name="signup-8" data-id=""><?php echo $texts['signup-8']['content']; ?></div>
            <p data-name="signup-9" data-id=""><?php echo $texts['signup-9']['content']; ?></p>
            <p data-name="signup-10" data-id=""><?php echo $texts['signup-10']['content']; ?></p>
            <?php if (!empty($repeat)) { ?>
                <span class="repeat"><?php echo $repeat; ?></span>
            <?php } ?>
            <?php $form = ActiveForm::begin([
                'id' => 'repeat',
                'enableClientValidation' => false,
            ]); ?>
            <?php echo Html::submitButton($texts['signup-11']['content'], [
                'class' => 'button',
                'data-name' => 'signup-11',
                'data-id' => '',
            ]) ?>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>