<?php
use yii\helpers\Url;
?>
<div class="order order-enter">
    <header>
        <div class="width">
            <h1 data-name="order-server-1"><?php echo $texts['order-server-1']['content']; ?></h1>
            <div class="tabs">
                <div class="params"><span>1</span><span data-name="order-server-2"><?php echo $texts['order-server-2']['content']; ?></span></div>
                <div class="enter active"><span>2</span><span data-name="order-server-3"><?php echo $texts['order-server-3']['content']; ?></span></div>
                <div class="pay"><span>3</span><span data-name="order-server-4"><?php echo $texts['order-server-4']['content']; ?></span></div>
            </div>
        </div>
    </header>
</div>
<div id="success" class="signup">
    <div class="reg-block">
        <div class="width">
            <?php if (empty($error)) {?>
                <div class="title" data-name="signup-12" data-id=""><?php echo $texts['signup-12']['content']; ?></div>
                <a href="<?php echo Url::to(['server-order/pay']); ?>" class="button" data-name="signup-13" data-id=""><?php echo $texts['signup-14']['content']; ?></a>
            <?php } else { ?>
                <?php foreach ($error as $er) { ?>
                    <p class="error"><?php echo $er; ?></p>
                    <a href="<?php echo Url::to(['server-order/pay']); ?>" class="button" data-name="signup-13" data-id=""><?php echo $texts['signup-14']['content']; ?></a>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
</div>