<?php

namespace frontend\controllers;

use Yii;
use common\models\Server;
use common\components\BillingAPI;
use frontend\models\OrderForm;
use yii\helpers\Url;
use frontend\models\AccountForm;
use frontend\models\SignupForm;
use common\models\LangTexts;
use common\models\Texts;
use yii\helpers\Html;
use common\models\User;
use common\models\Option;


class ServerOrderController extends MainController {
    
    public function actionIndex() {
        $session = Yii::$app->session;
        $id = Yii::$app->request->getQueryParam('id') ? Yii::$app->request->getQueryParam('id') : null;
        $texts = LangTexts::getTexstByLang(Texts::ORDER_SERV_NAME);
        $options = Option::getAllOptions(['and', ['options.server_type' => Option::TYPE_SERVER, 'options.active' => 1], ['is not', 'options.name', null]], true,
            ['options.sort' => SORT_ASC, 'options.id' => SORT_DESC], true, true);
        
        $form = new OrderForm();
        $server = [];
        $os = [];

        if ($id) {
            $session->set('order_form', ['server_id' => $id]);
        }
        if ($id || $session['order_form']['server_id']) {
            $server_id = $id ? $id : $session['order_form']['server_id'];
            $server = (new Server)->getOneServer([
                'servers.is_deleted' => 0,
                'servers.active' => 1,
                'servers.id' => $server_id
            ], true, true);
            $os = (new BillingAPI())->getServerOsList();
        }

        return $this->render('index', [
            'server' => $server,
            'os' => $os,
            'model' => $form,
            'texts' => $texts,
            'options' => $options
        ]);
    }
    
    public function actionPay() {
        $texts = LangTexts::getTexstByLang(Texts::ORDER_SERV_NAME);
        $session = Yii::$app->session;
        $form = new OrderForm();

        if ($post = Yii::$app->request->post()) {

            $form->server_id = Html::encode($post['server_id']);
            $form->os = Html::encode($post['os']);
            $form->panel = Html::encode($post['panel']);
            $form->administration = Html::encode($post['administration']);
            $form->traffic = Html::encode($post['traffic']);
            $form->ip = Html::encode($post['ip']);
            $form->domen = Html::encode($post['domen']);
            $form->period = Html::encode($post['period']);

            if ($form->validate()) {
                $session->set('order_form', [
                    'os' => $form->os,
                    'panel' => $form->panel,
                    'administration' => $form->administration,
                    'traffic' => $form->traffic,
                    'ip' => $form->ip,
                    'domen' => $form->domen,
                    'period' => $form->period,
                ]);
            }
        }

        if (!$session['order_form']) {
            Yii::$app->getResponse()->redirect(Url::toRoute(['/server-order']));
        }

        if (Yii::$app->user->isGuest || empty(User::findIdentity(Yii::$app->user->id))) {
            Yii::$app->getResponse()->redirect(Url::toRoute(['server-order/enter']));
        }

        return $this->render('pay', [
            'texts' => $texts
        ]);
    }

    public function actionSuccess() {
        $activation = Yii::$app->request->getQueryParam('activation') ? Yii::$app->request->getQueryParam('activation') : null;
        $errorArr = [];

        if ($activation) {
            $isKey = User::findOne(['activation' => $activation]);
            if ($isKey) {
                $user = User::findOne(['activation' => $activation, 'status' => 0]);
                if (!empty($user)) {
                    $user->status = 10;
                    $user->update();

                    $session = Yii::$app->session;
                    if ($session->has('reg')) {
                        $session->remove('reg');
                        $session->remove('repeat_id');
                    }
                } else {
                    $errorArr['user'] = Yii::t('app', 'account_exist');
                }
            } else {
                $errorArr['key'] = Yii::t('app', 'link_activation_er');
            }
        } else {
            Yii::$app->getResponse()->redirect(Url::toRoute(['/signup']));
        }
        $texts = LangTexts::getTexstByLang([Texts::SIGNUP_NAME, Texts::ORDER_SERV_NAME]);
        return $this->render('success', [
            'error' => $errorArr,
            'texts' => $texts
        ]);
    }

    public function actionConfirm() {
        $session = Yii::$app->session;
        if ($session->has('reg')) {
            $repeat = '';

            if (Yii::$app->request->post() || $session->has('enter_form')) {
                $id = $session->has('repeat_id') ? $session['repeat_id'] : null;
                if ($session->has('enter_form')) {
                    $session->remove('enter_form');
                }

                if ($id) {
                    $user = User::findOne(['id' => $id]);

                    if (!empty($user)) {
                        $activation = md5($user->email . time());
                        $user->activation = $activation;
                        $user->update();

                        $repeat = Yii::t('app', 'repeat_send_mail');
                        $url = substr(Url::home(true), 0, -1) . Url::toRoute(['server-order/success', 'activation' => $activation]);

                        Yii::$app->mailer->compose()
                            ->setTo($user->email)
                            ->setSubject(Yii::t('app', 'confirm_reg'))
                            ->setHtmlBody(Yii::t('app', 'link_activation') . ' <a href="' . $url . '">' . $url . '</a>')
                            ->send();
                    }
                }
            }
            $texts = LangTexts::getTexstByLang([Texts::SIGNUP_NAME, Texts::ORDER_SERV_NAME]);
            return $this->render('confirm', [
                'repeat' => $repeat,
                'texts' => $texts
            ]);
        } else {
            Yii::$app->getResponse()->redirect(Url::toRoute(['/signup']));
        }
    }

    public function actionEnter() {
        $accountForm = new AccountForm();
        $signupForm = new SignupForm();
        $texts = LangTexts::getTexstByLang([Texts::SIGNUP_NAME, Texts::ORDER_SERV_NAME]);

        if ($accountForm->load(Yii::$app->request->post()) && $accountForm->login()) {
                Yii::$app->getResponse()->redirect(Url::toRoute(['server-order/pay']));
        }

        if ($signupForm->load(Yii::$app->request->post()) && $signupForm->validate()) {
            $name = Html::encode($signupForm->name);
            $lastname = Html::encode($signupForm->lastname);
            $email = Html::encode($signupForm->email);
            $phone = Html::encode($signupForm->phone);
            $country = Html::encode($signupForm->country);
            $address = Html::encode($signupForm->address);
            $activation = md5($email . time());

            $user = new User();
            $user->username = $name;
            $user->lastname = $lastname;
            $user->email = $email;
            $user->phone = $phone ? $phone : null;
            $user->country = $country ? $country : null;
            $user->address = $address ? $address : null;
            $user->activation = $activation;
            $user->setPassword($signupForm->password);
            $user->generateAuthKey();
            $user->status = $user::STATUS_DELETED;
            $res = $user->save() ? $user : null;


            if ($res) {
                $id = Yii::$app->db->lastInsertID;

                $userRole = Yii::$app->authManager->getRole('user');
                Yii::$app->authManager->assign($userRole, $id);
                $permit = Yii::$app->authManager->getPermission('account');
                Yii::$app->authManager->assign($permit, $id);

                $url = substr(Url::home(true), 0, -1) . Url::toRoute(['server-order/success', 'activation' => $activation]);

                Yii::$app->mailer->compose()
                    ->setTo($email)
                    ->setSubject(Yii::t('app', 'confirm_reg'))
                    ->setHtmlBody(Yii::t('app', 'link_activation') . ' <a href="' . $url . '">' . $url . '</a>')
                    ->send();

                $session = Yii::$app->session;
                $session->set('reg', true);
                $session->set('repeat_id', $id);
                    Yii::$app->getResponse()->redirect(Url::toRoute(['server-order/confirm']));
            }
        }

        return $this->render('enter', [
            'account' => $accountForm,
            'signup' => $signupForm,
            'texts' => $texts
        ]);
    }

}