<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use yii\helpers\Url;
use yii\web\Response;
use common\models\Texts;
use common\models\LangTexts;
use common\models\Server;
use common\models\VpsTariff;


class SiteController extends MainController {
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

  
    public function actionIndex() {
        $texts = LangTexts::getTexstByLang(Texts::MAIN_PAGE_NAME);
        $servers = (new Server)->getAllServers(['servers.is_deleted' => 0, 'servers.active' => 1], true, ['servers.id' => SORT_ASC], true);
        $tariffs = (new VpsTariff())->getAllTariffs([
            VpsTariff::tableName() . '.active' => 1,
            VpsTariff::tableName() . '.is_deleted' => 0
        ], true, [VpsTariff::tableName() . '.id' => SORT_ASC], true);
        
        return $this->render('index', [
            'texts' => $texts,
            'servers' => $servers,
            'tariffs' => $tariffs
        ]);
    }
    
    public function actionLogin() {
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout() {
        Yii::$app->user->logout();
        $session = Yii::$app->session;
        $session->open();
        $session->remove('username');

        return $this->goHome();
    }
    

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset() {
        $texts = LangTexts::getTexstByLang(Texts::RESET_PASS_NAME);
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'reset_email_success'));
            } else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'reset_email_error'));
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
            'texts' => $texts
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token) {
        $texts = LangTexts::getTexstByLang(Texts::RESET_PASS_NAME);
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'reset_email_save'));
            Yii::$app->getResponse()->redirect(Url::toRoute(['/request-password-reset']));
        }

        return $this->render('resetPassword', [
            'model' => $model,
            'texts' => $texts
        ]);
    }

}
