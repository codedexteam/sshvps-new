<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use common\models\Texts;
use common\models\LangTexts;
use frontend\models\AccountForm;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use common\models\User;
use common\models\MetaTag;

class MainController extends Controller {
    
    public function init() {
        parent::init();
        $this->view->params['texts'] = LangTexts::getTexstByLang([Texts::FOOTER_NAME, Texts::HEADER_NAME, Texts::ENTER_NAME]);
        $this->view->params['accountForm'] = new AccountForm;
    }

    public function beforeAction($action) {
        if (parent::beforeAction($action)) {
            $controller = Yii::$app->controller->id;
            $page_action = Yii::$app->controller->action->id;
            $page_key = strtoupper($controller . '_' . $page_action);
            $meta = MetaTag::getOneTag(['page_key' => $page_key], true, true);

            if (!empty($meta)) {
                $this->view->description = $meta->langTags[0]->description;
                $this->view->keywords = $meta->langTags[0]->keywords;
                $this->view->title = $meta->langTags[0]->title;
            }

            return true;
        }
        return false;
    }
    
    public function actionEdit() {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $values = Yii::$app->request->post('values') ? Yii::$app->request->post('values') : null;

            if (!empty($values)) {
                $textsNames = [];
                $newNames = [];
                $textValues = [];

                foreach ($values as $name) {
                    $textsNames[] = $name['name'];
                    $newNames[]['name'] = $name['name'];
                }
//                (new Texts())->insertData(Texts::tableName(), $newNames, true);

                $nameTextString = implode('", "', $textsNames);

                $texts = Texts::getByCondition('name IN ("' . $nameTextString . '")');

                $i = 0;
                foreach ($values as $val) {
                    foreach ($texts as $text) {
                        if ($val['name'] == $text['name']) {
                            $textValues[$i]['text_id'] = $text['id'] ;
                            $textValues[$i]['content'] = trim($val['content']);
                            $textValues[$i]['lang'] = Yii::$app->language;
                        }
                        $i++;
                    }
                }
                $langTexts = new LangTexts();
//                $langTexts->insertData(LangTexts::tableName(), $textValues, true);
                return $result = $langTexts->updateData(LangTexts::tableName(), 'content', $textValues, 'text_id', 'lang = \'' . Yii::$app->language . '\'');
            }
        }
    }


    public function actionEnter() {
        $form = new AccountForm();
        if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $form->email = Yii::$app->request->post('email');
            $form->password = Yii::$app->request->post('password');

            if ($form->login()) {
                Yii::$app->getResponse()->redirect(Url::toRoute(['/account']));
            } 

            return [
                'valid' => ActiveForm::validate($form)
            ];
        }
    }

}