<?php

namespace frontend\controllers;

use Yii;
use frontend\models\SignupForm;
use yii\helpers\Url;
use yii\helpers\Html;
use common\models\User;
use common\models\LangTexts;
use common\models\Texts;


class SignupController extends MainController {

    public function actionIndex() {
        $form = new SignupForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $name = Html::encode($form->name);
            $lastname = Html::encode($form->lastname);
            $email = Html::encode($form->email);
            $phone = Html::encode($form->phone);
            $country = Html::encode($form->country);
            $address = Html::encode($form->address);
            $activation = md5($email . time());

            $user = new User();
            $user->username = $name;
            $user->lastname = $lastname;
            $user->email = $email;
            $user->phone = $phone ? $phone : null;
            $user->country = $country ? $country : null;
            $user->address = $address ? $address : null;
            $user->activation = $activation;
            $user->setPassword($form->password);
            $user->generateAuthKey();
            $user->status = $user::STATUS_DELETED;
            $res = $user->save() ? $user : null;


            if ($res) {
                $id = Yii::$app->db->lastInsertID;

                $userRole = Yii::$app->authManager->getRole('user');
                Yii::$app->authManager->assign($userRole, $id);
                $permit = Yii::$app->authManager->getPermission('account');
                Yii::$app->authManager->assign($permit, $id);

                $url = substr(Url::home(true), 0, -1) . Url::toRoute(['signup/success', 'activation' => $activation]);

                Yii::$app->mailer->compose()
                    ->setTo($email)
                    ->setSubject(Yii::t('app', 'confirm_reg'))
                    ->setHtmlBody(Yii::t('app', 'link_activation') . ' <a href="' . $url . '">' . $url . '</a>')
                    ->send();

                $session = Yii::$app->session;
                $session->set('reg', true);
                $session->set('repeat_id', $id);
                Yii::$app->getResponse()->redirect(Url::toRoute(['signup/confirm']));
            }
        }

        $texts = LangTexts::getTexstByLang(Texts::SIGNUP_NAME);

        return $this->render('index', [
            'model' => $form,
            'texts' => $texts
        ]);
    }

    public function actionConfirm() {
        $session = Yii::$app->session;
        if ($session->has('reg')) {
            $repeat = '';
            
            if (Yii::$app->request->post() || $session->has('enter_form')) {
                $id = $session->has('repeat_id') ? $session['repeat_id'] : null;
                if ($session->has('enter_form')) {
                    $session->remove('enter_form');
                }

                if ($id) {
                    $user = User::findOne(['id' => $id]);

                    if (!empty($user)) {
                        $activation = md5($user->email . time());
                        $user->activation = $activation;
                        $user->update();

                        $repeat = Yii::t('app', 'repeat_send_mail');
                        $url = substr(Url::home(true), 0, -1) . Url::toRoute(['signup/success', 'activation' => $activation]);

                        Yii::$app->mailer->compose()
                            ->setTo($user->email)
                            ->setSubject(Yii::t('app', 'confirm_reg'))
                            ->setHtmlBody(Yii::t('app', 'link_activation') . ' <a href="' . $url . '">' . $url . '</a>')
                            ->send();
                    }
                }
            }
            $texts = LangTexts::getTexstByLang(Texts::SIGNUP_NAME);
            return $this->render('confirm', [
                'repeat' => $repeat,
                'texts' => $texts
            ]);
        } else {
            Yii::$app->getResponse()->redirect(Url::toRoute(['/signup']));
        }
    }

    public function actionSuccess() {
        $activation = Yii::$app->request->getQueryParam('activation') ? Yii::$app->request->getQueryParam('activation') : null;
        $errorArr = [];

        if ($activation) {
            $isKey = User::findOne(['activation' => $activation]);
            if ($isKey) {
                $user = User::findOne(['activation' => $activation, 'status' => 0]);
                if (!empty($user)) {
                    $user->status = 10;
                    $user->update();

                    $session = Yii::$app->session;
                    if ($session->has('reg')) {
                        $session->remove('reg');
                        $session->remove('repeat_id');
                    }
                } else {
                    $errorArr['user'] = Yii::t('app', 'account_exist');
                }
            } else {
                $errorArr['key'] = Yii::t('app', 'link_activation_er');
            }
        } else {
            Yii::$app->getResponse()->redirect(Url::toRoute(['/signup']));
        }
        $texts = LangTexts::getTexstByLang(Texts::SIGNUP_NAME);
        return $this->render('success', [
            'error' => $errorArr,
            'texts' => $texts
        ]);
    }

}