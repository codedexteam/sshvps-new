<?php

namespace frontend\controllers;

use Yii;
use frontend\models\AccountForm;
use yii\helpers\Url;
use yii\filters\AccessControl;
use common\models\User;
use frontend\models\AccountEditForm;
use yii\helpers\Html;
use common\models\LangTexts;
use common\models\Texts;
use common\components\BillingAPI;


class AccountController extends MainController {

    public function beforeAction($action) {
        if (!Yii::$app->user->can('account')) {
            return Yii::$app->getResponse()->redirect(Url::toRoute(['/signup']));
        }

        if (!parent::beforeAction($action)) {
            return false;
        }
        return true;
    }
    
    public function actionIndex() {
        $user = User::findIdentity(Yii::$app->user->id);
        $texts = LangTexts::getTexstByLang(Texts::ACCOUNT_NAME);
        
        return $this->render('index', [
            'user' => $user,
            'texts' => $texts
        ]);
    }
    
    public function actionEdit() {
        $form = new AccountEditForm();
        $user = User::findIdentity(Yii::$app->user->id);
        $texts = LangTexts::getTexstByLang(Texts::ACCOUNT_NAME);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $name = Html::encode($form->name);
            $lastname = Html::encode($form->lastname);
            $phone = Html::encode($form->phone);
            $country = Html::encode($form->country);
            $address = Html::encode($form->address);

            $user->username = $name;
            $user->lastname = $lastname;
            $user->phone = $phone;
            $user->country = $country;
            $user->address = $address;
            $user->save();

            Yii::$app->getResponse()->redirect(Url::toRoute(['account/edit']));
        }

        return $this->render('edit', [
            'model' => $form,
            'user' => $user,
            'texts' => $texts
        ]);
    }

    public function actionResetPassword() {
        return $this->render('index');
    }

    public function actionServers() {
        $this->view->registerJsFile('/lib/colresizable/colResizable-1.6.min.js');
        $os = (new BillingAPI())->getServerOsList();
        
        return $this->render('servers', [
            'os' => $os
        ]);
    }

    public function actionVps() {
        return $this->render('index');
    }

    public function actionOptions() {
        return $this->render('index');
    }

    public function actionNotifications() {
        return $this->render('index');
    }

    public function actionTickets() {
        return $this->render('index');
    }

    public function actionQuestion() {
        return $this->render('index');
    }

    public function actionInvoices() {
        return $this->render('index');
    }

}