<?php

namespace frontend\controllers;

use common\models\LangTexts;
use common\models\Texts;
use common\models\VpsTariff;
use Yii;
use frontend\models\OrderForm;
use yii\helpers\Url;
use common\models\User;
use frontend\models\AccountForm;
use frontend\models\SignupForm;
use yii\helpers\Html;
use common\models\Option;


class VpsOrderController extends MainController {
    
    public function actionIndex() {
        $session = Yii::$app->session;
        $id = Yii::$app->request->getQueryParam('id') ? Yii::$app->request->getQueryParam('id') : null;
        $texts = LangTexts::getTexstByLang([Texts::ORDER_SERV_NAME, Texts::ORDER_VPS_NAME]);
        $options = Option::getAllOptions(['and', ['options.server_type' => Option::TYPE_VPS, 'options.active' => 1], ['is not', 'options.name', null]], true,
            ['options.sort' => SORT_ASC, 'options.id' => SORT_DESC], true, true);
        $vps = [];

        if ($id) {
            $session->set('order_vps_form', ['server_id' => $id]);
        }

        if ($id || $session['order_vps_form']['server_id']) {
            $server_id = $id ? $id : $session['order_vps_form']['server_id'];
            $vps = (new VpsTariff)->getOneVps([
                VpsTariff::tableName() . '.is_deleted' => 0,
                VpsTariff::tableName() . '.active' => 1,
                VpsTariff::tableName() . '.id' => $server_id
            ], true, true);
        }

        return $this->render('index', [
            'texts' => $texts,
            'vps' => $vps,
            'options' => $options
        ]);
    }

    public function actionPay() {
        $texts = LangTexts::getTexstByLang(Texts::ORDER_SERV_NAME);
        $session = Yii::$app->session;
        $form = new OrderForm();

        if ($post = Yii::$app->request->post()) {

            $form->server_id = $post['server_id'];
            $form->os = Html::encode($post['os']);
            $form->panel = Html::encode($post['panel']);
            $form->ip = Html::encode($post['ip']);
            $form->ip2 = Html::encode($post['ip2']);
            $form->domen = Html::encode($post['domen']);
            $form->period = Html::encode($post['period']);

            if ($form->validate()) {
                $session->set('order_vps_form', [
                    'os' => $form->os,
                    'panel' => $form->panel,
                    'ip' => $form->ip,
                    'ip2' => $form->ip2,
                    'domen' => $form->domen,
                    'period' => $form->period,
                ]);
            }
        }

        if (!$session['order_vps_form']) {
            Yii::$app->getResponse()->redirect(Url::toRoute(['/vps-order']));
        }

        if (Yii::$app->user->isGuest || empty(User::findIdentity(Yii::$app->user->id))) {
            Yii::$app->getResponse()->redirect(Url::toRoute(['vps-order/enter']));
        }

        return $this->render('pay', [
            'texts' => $texts
        ]);
    }

    public function actionSuccess() {
        $activation = Yii::$app->request->getQueryParam('activation') ? Yii::$app->request->getQueryParam('activation') : null;
        $errorArr = [];

        if ($activation) {
            $isKey = User::findOne(['activation' => $activation]);
            if ($isKey) {
                $user = User::findOne(['activation' => $activation, 'status' => 0]);
                if (!empty($user)) {
                    $user->status = 10;
                    $user->update();

                    $session = Yii::$app->session;
                    if ($session->has('reg')) {
                        $session->remove('reg');
                        $session->remove('repeat_id');
                    }
                } else {
                    $errorArr['user'] = Yii::t('app', 'account_exist');
                }
            } else {
                $errorArr['key'] = Yii::t('app', 'link_activation_er');
            }
        } else {
            Yii::$app->getResponse()->redirect(Url::toRoute(['/signup']));
        }
        $texts = LangTexts::getTexstByLang([Texts::SIGNUP_NAME, Texts::ORDER_SERV_NAME]);
        return $this->render('success', [
            'error' => $errorArr,
            'texts' => $texts
        ]);
    }

    public function actionConfirm() {
        $session = Yii::$app->session;
        if ($session->has('reg')) {
            $repeat = '';

            if (Yii::$app->request->post() || $session->has('enter_form')) {
                $id = $session->has('repeat_id') ? $session['repeat_id'] : null;
                if ($session->has('enter_form')) {
                    $session->remove('enter_form');
                }

                if ($id) {
                    $user = User::findOne(['id' => $id]);

                    if (!empty($user)) {
                        $activation = md5($user->email . time());
                        $user->activation = $activation;
                        $user->update();

                        $repeat = Yii::t('app', 'repeat_send_mail');
                        $url = substr(Url::home(true), 0, -1) . Url::toRoute(['vps-order/success', 'activation' => $activation]);

                        Yii::$app->mailer->compose()
                            ->setTo($user->email)
                            ->setSubject(Yii::t('app', 'confirm_reg'))
                            ->setHtmlBody(Yii::t('app', 'link_activation') . ' <a href="' . $url . '">' . $url . '</a>')
                            ->send();
                    }
                }
            }
            $texts = LangTexts::getTexstByLang([Texts::SIGNUP_NAME, Texts::ORDER_SERV_NAME]);
            return $this->render('confirm', [
                'repeat' => $repeat,
                'texts' => $texts
            ]);
        } else {
            Yii::$app->getResponse()->redirect(Url::toRoute(['/signup']));
        }
    }

    public function actionEnter() {
        $accountForm = new AccountForm();
        $signupForm = new SignupForm();
        $texts = LangTexts::getTexstByLang([Texts::SIGNUP_NAME, Texts::ORDER_SERV_NAME]);

        if ($accountForm->load(Yii::$app->request->post()) && $accountForm->login()) {
            Yii::$app->getResponse()->redirect(Url::toRoute(['vps-order/pay']));
        }

        if ($signupForm->load(Yii::$app->request->post()) && $signupForm->validate()) {
            $name = Html::encode($signupForm->name);
            $lastname = Html::encode($signupForm->lastname);
            $email = Html::encode($signupForm->email);
            $phone = Html::encode($signupForm->phone);
            $country = Html::encode($signupForm->country);
            $address = Html::encode($signupForm->address);
            $activation = md5($email . time());

            $user = new User();
            $user->username = $name;
            $user->lastname = $lastname;
            $user->email = $email;
            $user->phone = $phone ? $phone : null;
            $user->country = $country ? $country : null;
            $user->address = $address ? $address : null;
            $user->activation = $activation;
            $user->setPassword($signupForm->password);
            $user->generateAuthKey();
            $user->status = $user::STATUS_DELETED;
            $res = $user->save() ? $user : null;


            if ($res) {
                $id = Yii::$app->db->lastInsertID;

                $userRole = Yii::$app->authManager->getRole('user');
                Yii::$app->authManager->assign($userRole, $id);
                $permit = Yii::$app->authManager->getPermission('account');
                Yii::$app->authManager->assign($permit, $id);

                $url = substr(Url::home(true), 0, -1) . Url::toRoute(['vps-order/success', 'activation' => $activation]);

                Yii::$app->mailer->compose()
                    ->setTo($email)
                    ->setSubject(Yii::t('app', 'confirm_reg'))
                    ->setHtmlBody(Yii::t('app', 'link_activation') . ' <a href="' . $url . '">' . $url . '</a>')
                    ->send();

                $session = Yii::$app->session;
                $session->set('reg', true);
                $session->set('repeat_id', $id);
                Yii::$app->getResponse()->redirect(Url::toRoute(['vps-order/confirm']));
            }
        }

        return $this->render('enter', [
            'account' => $accountForm,
            'signup' => $signupForm,
            'texts' => $texts
        ]);
    }

}