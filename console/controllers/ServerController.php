<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use common\components\BillingAPI;
use common\models\BillingServer;

class ServerController extends Controller {

    public function actionUpdate() {
        $apiServers = (new BillingAPI())->getServerList();
        $server = new BillingServer();

        $success = false;

        if (!empty($apiServers) && isset($apiServers->doc->elem)) {
            $insertData = [];

            foreach ($apiServers->doc->elem as $val) {
                $id = (array)$val->id;

                $oneServer = (new BillingAPI())->getOneServer($id['$']);

                $name = isset($val->name) ? (array)$val->name : null;
                $cpuname = isset($oneServer->doc->cpuname) ? (array)$oneServer->doc->cpuname : null;
                $ram = isset($oneServer->doc->ram) ? (array)$oneServer->doc->ram : null;
                $hdd = isset($oneServer->doc->hddinfo) ? (array)$oneServer->doc->hddinfo : null;
                $raid = isset($oneServer->doc->raid) ? (array)$oneServer->doc->raid : null;
                $ipmi = isset($oneServer->doc->ipmi) ? (array)$oneServer->doc->ipmi : null;
                $price = isset($oneServer->doc->price) ? (array)$oneServer->doc->price : null;
                $rack = isset($val->rack) ? (array)$val->rack : null;
                $config = isset($val->type) ? (array)$val->type : null;
                $chassis_templ = isset($val->chassis_templ) ? (array)$val->chassis_templ : null;
                $hostname = isset($val->hostname) ? (array)$val->hostname : null;
                $owner = isset($val->owner) ? (array)$val->owner : null;
                $mac = isset($val->mac) ? (array)$val->mac : null;
                $poweron = isset($val->poweron) ? (array)$val->poweron : null;
                $ip = isset($val->ip) ? (array)$val->ip : null;
                $localspeed = isset($val->localspeed) ? (array)$val->localspeed : null;
                $os = isset($val->os) ? (array)$val->os : null;

                $insertData[$id['$']]['billing_id'] = $id['$'];
                $insertData[$id['$']]['name'] = isset($name['$']) ? $name['$'] : null;
                $insertData[$id['$']]['cpuname'] = isset($cpuname['$']) ? $cpuname['$'] : null;
                $insertData[$id['$']]['ram'] = isset($ram['$']) ? $ram['$'] : null;
                $insertData[$id['$']]['hdd'] = isset($hdd['$']) ? $hdd['$'] : null;
                $insertData[$id['$']]['raid'] = isset($raid['$']) ? ($raid['$'] != 'off') ? 1 : 0 : 0;
                $insertData[$id['$']]['ipmi'] = isset($ipmi['$']) ? ($ipmi['$'] != 'off') ? 1 : 0 : 0;
                $insertData[$id['$']]['price'] = isset($price['$']) ? $price['$'] : null;
                $insertData[$id['$']]['rack'] = isset($rack['$']) ? $rack['$'] : null;
                $insertData[$id['$']]['config'] = isset($config['$']) ? $config['$'] : null;
                $insertData[$id['$']]['chassis_templ'] = isset($chassis_templ['$']) ? $chassis_templ['$'] : null;
                $insertData[$id['$']]['hostname'] = isset($hostname['$']) ? $hostname['$'] : null;
                $insertData[$id['$']]['owner'] = isset($owner['$']) ? $owner['$'] : null;
                $insertData[$id['$']]['mac'] = isset($mac['$']) ? $mac['$'] : null;
                $insertData[$id['$']]['poweron'] = isset($poweron['$']) ? ($poweron['$'] != 'off') ? 1 : 0 : 0;
                $insertData[$id['$']]['ip'] = isset($ip['$']) ? $ip['$'] : null;
                $insertData[$id['$']]['localspeed'] = isset($localspeed['$']) ? $localspeed['$'] : null;
                $insertData[$id['$']]['os'] = isset($os['$']) ? $os['$'] : null;
            }
            $success = $server->insertData(BillingServer::tableName(), $insertData, true);

            $localServers = $server->getAll();

            if (!empty($localServers)) {
                foreach ($localServers as $local) {
                    if (array_key_exists($local['billing_id'], $insertData)) {
                        $eqServer = $insertData[$local['billing_id']];
                        $changedValues = [];

                        if (trim($local['name']) != trim($eqServer['name'])) {
                            $changedValues['name'] =  $eqServer['name'];
                        }
                        if (trim($local['cpuname']) != trim($eqServer['cpuname'])) {
                            $changedValues['cpuname'] =  $eqServer['cpuname'];
                        }
                        if (trim($local['ram']) != trim($eqServer['ram'])) {
                            $changedValues['ram'] =  $eqServer['ram'];
                        }
                        if (trim($local['hdd']) != trim($eqServer['hdd'])) {
                            $changedValues['hdd'] =  $eqServer['hdd'];
                        }
                        if (trim($local['raid']) != trim($eqServer['raid'])) {
                            $changedValues['raid'] =  $eqServer['raid'];
                        }
                        if (trim($local['ipmi']) != trim($eqServer['ipmi'])) {
                            $changedValues['ipmi'] =  $eqServer['ipmi'];
                        }
                        if (trim($local['price']) != trim($eqServer['price'])) {
                            $changedValues['price'] =  $eqServer['price'];
                        }
                        if (trim($local['rack']) != trim($eqServer['rack'])) {
                            $changedValues['rack'] =  $eqServer['rack'];
                        }
                        if (trim($local['config']) != trim($eqServer['config'])) {
                            $changedValues['config'] =  $eqServer['config'];
                        }
                        if (trim($local['chassis_templ']) != trim($eqServer['chassis_templ'])) {
                            $changedValues['chassis_templ'] =  $eqServer['chassis_templ'];
                        }
                        if (trim($local['hostname']) != trim($eqServer['hostname'])) {
                            $changedValues['hostname'] =  $eqServer['hostname'];
                        }
                        if (trim($local['owner']) != trim($eqServer['owner'])) {
                            $changedValues['owner'] =  $eqServer['owner'];
                        }
                        if (trim($local['mac']) != trim($eqServer['mac'])) {
                            $changedValues['mac'] =  $eqServer['mac'];
                        }
                        if (trim($local['poweron']) != trim($eqServer['poweron'])) {
                            $changedValues['poweron'] =  $eqServer['poweron'];
                        }
                        if (trim($local['ip']) != trim($eqServer['ip'])) {
                            $changedValues['ip'] =  $eqServer['ip'];
                        }
                        if (trim($local['localspeed']) != trim($eqServer['localspeed'])) {
                            $changedValues['localspeed'] =  $eqServer['localspeed'];
                        }
                        if (trim($local['os']) != trim($eqServer['os'])) {
                            $changedValues['os'] =  $eqServer['os'];
                        }

                        if (!empty($changedValues)) {
                            Yii::$app->db->createCommand()->
                            update(BillingServer::tableName(), $changedValues, 'id = ' . $local['id'])
                                ->execute();
                        }
                    } else {
                        Yii::$app->db->createCommand()->
                        update(BillingServer::tableName(), ['is_deleted' => 1], 'id = ' . $local['id'])
                            ->execute();
                    }
                }
            }
        }
        if ($success) {
            echo 'Finished';
            return true;
        }
    }

}