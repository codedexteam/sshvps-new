<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
/**
 * Инициализатор RBAC выполняется в консоли php yii rbac/init
 */
class RbacController extends Controller {

    public function actionInit() {
        $auth = Yii::$app->authManager;

        $auth->removeAll(); //На всякий случай удаляем старые данные из БД...

        // Создадим роли админа и редактора новостей
        $admin = $auth->createRole('admin');
        $guest = $auth->createRole('guest');
        $user = $auth->createRole('user');

        // запишем их в БД
        $auth->add($admin);
        $auth->add($guest);
        $auth->add($user);

        // Создаем разрешения. Например, просмотр админки viewAdminPage и редактирование новости updateNews
        $viewAdminPage = $auth->createPermission('AdminPanel');
        $viewAdminPage->description = 'Просмотр админки';

        $viewAccount = $auth->createPermission('account');
        $viewAccount->description = 'Личный кабинет';

        // Запишем эти разрешения в БД
        $auth->add($viewAdminPage);
        $auth->add($viewAccount);

        // Теперь добавим наследования. Для роли editor мы добавим разрешение updateNews,
        // а для админа добавим наследование от роли editor и еще добавим собственное разрешение viewAdminPage

        // Еще админ имеет собственное разрешение - «Просмотр админки»
        $auth->addChild($admin, $viewAdminPage);
        $auth->addChild($user, $viewAccount);

        // Назначаем роль admin пользователю с ID 1
//        $auth->assign($admin, 1);
    }

}